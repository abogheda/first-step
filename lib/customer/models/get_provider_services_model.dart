import 'package:base_flutter/customer/models/sections_model.dart';
import 'package:base_flutter/customer/models/service_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'get_provider_services_model.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class GetProviderServicesModel {
  @JsonKey(name: 'serviceCategories')
  List<SectionsModel>? serviceCategories;
  @JsonKey(name: 'services')
  List<ServiceModel>? services;

  GetProviderServicesModel({this.serviceCategories, this.services});

   factory GetProviderServicesModel.fromJson(Map<String, dynamic> json) => _$GetProviderServicesModelFromJson(json);

   Map<String, dynamic> toJson() => _$GetProviderServicesModelToJson(this);
}


