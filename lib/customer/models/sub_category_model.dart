import 'package:json_annotation/json_annotation.dart'; 

part 'sub_category_model.g.dart'; 

@JsonSerializable( ignoreUnannotated: false)
class SubCategoryModel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'image')
  String? image;
  @JsonKey(name: 'parent_id')
  int parentId;
  @JsonKey(name: 'parent_name')
  String? parentName;

  SubCategoryModel({required this.id, this.name, this.image,required this.parentId, this.parentName});

   factory SubCategoryModel.fromJson(Map<String, dynamic> json) => _$SubCategoryModelFromJson(json);

   Map<String, dynamic> toJson() => _$SubCategoryModelToJson(this);
}

