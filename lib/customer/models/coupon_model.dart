import 'package:json_annotation/json_annotation.dart'; 

part 'coupon_model.g.dart'; 

@JsonSerializable( ignoreUnannotated: false)
class CouponModel {
  @JsonKey(name: 'type')
  String? type;
  @JsonKey(name: 'discount')
  num? discount;
  @JsonKey(name: 'id')
  int id;

  CouponModel({this.type, this.discount,required this.id});

   factory CouponModel.fromJson(Map<String, dynamic> json) => _$CouponModelFromJson(json);

   Map<String, dynamic> toJson() => _$CouponModelToJson(this);
}

