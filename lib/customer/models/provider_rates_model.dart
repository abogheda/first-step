import 'package:json_annotation/json_annotation.dart'; 

part 'provider_rates_model.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class ProviderRatesModel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'value')
  num? value;
  @JsonKey(name: 'comment')
  String? comment;
  @JsonKey(name: 'user_name')
  String? userName;
  @JsonKey(name: 'user_image')
  String? userImage;
  @JsonKey(name: 'created_at')
  String? createdAt;

  ProviderRatesModel({required this.id, this.value, this.comment, this.userName, this.userImage, this.createdAt});

   factory ProviderRatesModel.fromJson(Map<String, dynamic> json) => _$ProviderRatesModelFromJson(json);

   Map<String, dynamic> toJson() => _$ProviderRatesModelToJson(this);
}

