import 'package:json_annotation/json_annotation.dart';

import 'notify_sub_body.dart';

part 'notify_body_model.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class NotifyBodyModel {
  @JsonKey(name: 'id')
  String? id;
  @JsonKey(name: 'type')
  String? type;
  @JsonKey(name: 'title')
  String? title;
  @JsonKey(name: 'body')
  String? body;
  @JsonKey(name: 'data')
  NotifySubBody? data;
  @JsonKey(name: 'created_at')
  String? createdAt;

  NotifyBodyModel({this.id, this.type, this.title, this.body, this.data, this.createdAt});

   factory NotifyBodyModel.fromJson(Map<String, dynamic> json) => _$NotifyBodyModelFromJson(json);

   Map<String, dynamic> toJson() => _$NotifyBodyModelToJson(this);
}


