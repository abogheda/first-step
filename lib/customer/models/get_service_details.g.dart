// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_service_details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetServiceDetails _$GetServiceDetailsFromJson(Map<String, dynamic> json) =>
    GetServiceDetails(
      service: json['service'] == null
          ? null
          : ServiceModel.fromJson(json['service'] as Map<String, dynamic>),
      serviceFiles: (json['service_files'] as List<dynamic>?)
          ?.map((e) => ServiceFileModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      comments: (json['comments'] as List<dynamic>?)
          ?.map((e) => CommentModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GetServiceDetailsToJson(GetServiceDetails instance) =>
    <String, dynamic>{
      'service': instance.service,
      'service_files': instance.serviceFiles,
      'comments': instance.comments,
    };
