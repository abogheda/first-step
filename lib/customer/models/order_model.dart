import 'package:json_annotation/json_annotation.dart'; 

part 'order_model.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class OrderModel {
  @JsonKey(name: 'invoice_id')
  int invoiceId;
  @JsonKey(name: 'order_id')
  int orderId;
  @JsonKey(name: 'order_num')
  String? orderNum;
  @JsonKey(name: 'invoice_num')
  String? invoiceNum;
  @JsonKey(name: 'service_name')
  String? serviceName;
  @JsonKey(name: 'provider_name')
  String? providerName;
  @JsonKey(name: 'provider_commerical_number')
  String? providerCommercialNumber;
  @JsonKey(name: 'provider_image')
  String? providerImage;
  @JsonKey(name: 'price_before_coupon')
  num? priceBeforeCoupon;
  @JsonKey(name: 'total_price')
  num? totalPrice;
  @JsonKey(name: 'have_coupon')
  bool? haveCoupon;
  @JsonKey(name: 'coupon_value')
  num? couponValue;
  @JsonKey(name: 'pay_type')
  int? payType;
  @JsonKey(name: 'pay_type_text')
  String? payTypeText;
  @JsonKey(name: 'status')
  int? status;
  @JsonKey(name: 'status_text')
  String? statusText;
  @JsonKey(name: 'pay_status')
  int? payStatus;
  @JsonKey(name: 'pay_status_text')
  String? payStatusText;
  @JsonKey(name: 'service_rated')
  bool? serviceRated;
  @JsonKey(name: 'provider_rated')
  bool? providerRated;
  @JsonKey(name: 'room_id')
  int? roomId;
  @JsonKey(name: 'date')
  String? date;
  @JsonKey(name: 'created_at')
  String? createdAt;

  OrderModel({required this.invoiceId,required this.orderId, this.orderNum, this.invoiceNum, this.serviceName, this.providerName, this.providerCommercialNumber, this.providerImage, this.priceBeforeCoupon, this.totalPrice, this.haveCoupon, this.couponValue, this.payType, this.payTypeText, this.status, this.statusText, this.payStatus, this.payStatusText, this.serviceRated, this.providerRated, this.roomId, this.date, this.createdAt});

   factory OrderModel.fromJson(Map<String, dynamic> json) => _$OrderModelFromJson(json);

   Map<String, dynamic> toJson() => _$OrderModelToJson(this);
}

