// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'provider_timing_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProviderTimingModel _$ProviderTimingModelFromJson(Map<String, dynamic> json) =>
    ProviderTimingModel(
      id: json['id'] as int,
      day: json['day'] as String?,
      from: json['from'] as String?,
      to: json['to'] as String?,
    );

Map<String, dynamic> _$ProviderTimingModelToJson(
        ProviderTimingModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'day': instance.day,
      'from': instance.from,
      'to': instance.to,
    };
