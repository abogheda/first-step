import 'package:base_flutter/customer/models/notify_body_model.dart';
import 'package:base_flutter/customer/models/pagination_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'notifications_model.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class NotificationsModel {
  @JsonKey(name: 'pagination')
  PaginationModel? pagination;
  @JsonKey(name: 'data')
  List<NotifyBodyModel>? notifyBody;

  NotificationsModel({this.pagination, this.notifyBody});

   factory NotificationsModel.fromJson(Map<String, dynamic> json) => _$NotificationsModelFromJson(json);

   Map<String, dynamic> toJson() => _$NotificationsModelToJson(this);
}



