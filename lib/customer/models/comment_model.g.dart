// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comment_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommentModel _$CommentModelFromJson(Map<String, dynamic> json) => CommentModel(
      id: json['id'] as int,
      value: json['value'] as num?,
      comment: json['comment'] as String?,
      userName: json['user_name'] as String?,
      userImage: json['user_image'] as String?,
      createdAt: json['created_at'] as String?,
    );

Map<String, dynamic> _$CommentModelToJson(CommentModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'value': instance.value,
      'comment': instance.comment,
      'user_name': instance.userName,
      'user_image': instance.userImage,
      'created_at': instance.createdAt,
    };
