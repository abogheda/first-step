// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ServiceModel _$ServiceModelFromJson(Map<String, dynamic> json) => ServiceModel(
      id: json['id'] as int,
      title: json['title'] as String?,
      description: json['description'] as String?,
      price: json['price'] as num?,
      image: json['image'] as String?,
      isFavorite: json['is_favorite'] as bool?,
      avgRate: json['avg_rate'] as num?,
      categoryId: json['category_id'] as int?,
      categoryName: json['category_name'] as String?,
      subCategoryId: json['sub_category_id'] as int?,
      subCategoryName: json['sub_category_name'] as String?,
      serviceCategoryId: json['service_category_id'] as int?,
      serviceCategoryName: json['service_category_name'] as String?,
      providerId: json['provider_id'] as int?,
      providerName: json['provider_name'] as String?,
    );

Map<String, dynamic> _$ServiceModelToJson(ServiceModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'price': instance.price,
      'image': instance.image,
      'avg_rate': instance.avgRate,
      'category_id': instance.categoryId,
      'category_name': instance.categoryName,
      'sub_category_id': instance.subCategoryId,
      'sub_category_name': instance.subCategoryName,
      'service_category_id': instance.serviceCategoryId,
      'service_category_name': instance.serviceCategoryName,
      'provider_id': instance.providerId,
      'provider_name': instance.providerName,
      'is_favorite': instance.isFavorite,
    };
