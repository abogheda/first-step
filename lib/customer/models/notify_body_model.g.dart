// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notify_body_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotifyBodyModel _$NotifyBodyModelFromJson(Map<String, dynamic> json) =>
    NotifyBodyModel(
      id: json['id'] as String?,
      type: json['type'] as String?,
      title: json['title'] as String?,
      body: json['body'] as String?,
      data: json['data'] == null
          ? null
          : NotifySubBody.fromJson(json['data'] as Map<String, dynamic>),
      createdAt: json['created_at'] as String?,
    );

Map<String, dynamic> _$NotifyBodyModelToJson(NotifyBodyModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'title': instance.title,
      'body': instance.body,
      'data': instance.data,
      'created_at': instance.createdAt,
    };
