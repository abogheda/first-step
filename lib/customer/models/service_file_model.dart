import 'package:json_annotation/json_annotation.dart'; 

part 'service_file_model.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class ServiceFileModel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'file')
  String? file;

  ServiceFileModel({required this.id, this.file});

   factory ServiceFileModel.fromJson(Map<String, dynamic> json) => _$ServiceFileModelFromJson(json);

   Map<String, dynamic> toJson() => _$ServiceFileModelToJson(this);
}

