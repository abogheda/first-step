import 'package:json_annotation/json_annotation.dart'; 

part 'category_model.g.dart'; 

@JsonSerializable( ignoreUnannotated: false)
class CategoryModel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'image')
  String? image;

  CategoryModel({required this.id, this.name, this.image});

   factory CategoryModel.fromJson(Map<String, dynamic> json) => _$CategoryModelFromJson(json);

   Map<String, dynamic> toJson() => _$CategoryModelToJson(this);
}

