import 'package:json_annotation/json_annotation.dart'; 

part 'get_room_model.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class GetRoomModel {
  @JsonKey(name: 'id')
  int? id;
  @JsonKey(name: 'members')
  List<Member>? members;
  @JsonKey(name: 'last_message_body')
  String? lastMessageBody;
  @JsonKey(name: 'last_message_created_dt')
  String? lastMessageCreatedDt;

  GetRoomModel({this.id, this.members, this.lastMessageBody, this.lastMessageCreatedDt});

   factory GetRoomModel.fromJson(Map<String, dynamic> json) => _$GetRoomModelFromJson(json);

   Map<String, dynamic> toJson() => _$GetRoomModelToJson(this);
}

@JsonSerializable(ignoreUnannotated: false)
class Member {
  @JsonKey(name: 'id')
  int? id;
  @JsonKey(name: 'type')
  String? type;
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'image')
  String? image;

  Member({this.id, this.type, this.name, this.image});

   factory Member.fromJson(Map<String, dynamic> json) => _$MemberFromJson(json);

   Map<String, dynamic> toJson() => _$MemberToJson(this);
}

