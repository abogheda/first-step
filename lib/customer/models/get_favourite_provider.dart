import 'package:json_annotation/json_annotation.dart'; 

part 'get_favourite_provider.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class GetFavouriteProvider {
  @JsonKey(name: 'id')
  int? id;
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'email')
  String? email;
  @JsonKey(name: 'country_code')
  String? countryCode;
  @JsonKey(name: 'phone')
  String? phone;
  @JsonKey(name: 'full_phone')
  String? fullPhone;
  @JsonKey(name: 'image')
  String? image;
  @JsonKey(name: 'lat')
  String? lat;
  @JsonKey(name: 'lng')
  String? lng;
  @JsonKey(name: 'map_desc')
  String? mapDesc;
  @JsonKey(name: 'type')
  String? type;
  @JsonKey(name: 'lang')
  String? lang;
  @JsonKey(name: 'avg_rate')
  String? avgRate;
  @JsonKey(name: 'is_notify')
  bool? isNotify;
  @JsonKey(name: 'is_favorite')
  bool? isFavorite;
  @JsonKey(name: 'token')
  String? token;

  GetFavouriteProvider({this.id, this.name, this.email, this.countryCode, this.phone, this.fullPhone, this.image, this.lat, this.lng, this.mapDesc, this.type, this.lang, this.avgRate, this.isNotify, this.isFavorite, this.token});

   factory GetFavouriteProvider.fromJson(Map<String, dynamic> json) => _$GetFavouriteProviderFromJson(json);

   Map<String, dynamic> toJson() => _$GetFavouriteProviderToJson(this);
}

