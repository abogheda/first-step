import 'package:json_annotation/json_annotation.dart'; 

part 'cart_item_model.g.dart'; 

@JsonSerializable( ignoreUnannotated: false)
class CartItemModel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'service_name')
  String? serviceName;
  @JsonKey(name: 'service_price')
  num? servicePrice;

  CartItemModel({required this.id, this.serviceName, this.servicePrice});

   factory CartItemModel.fromJson(Map<String, dynamic> json) => _$CartItemModelFromJson(json);

   Map<String, dynamic> toJson() => _$CartItemModelToJson(this);
}

