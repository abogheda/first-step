// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_created_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderCreatedModel _$OrderCreatedModelFromJson(Map<String, dynamic> json) =>
    OrderCreatedModel(
      id: json['id'] as int?,
      orderNum: json['order_num'] as String?,
      userName: json['user_name'] as String?,
      orderPrice: json['order_price'] as num?,
      priceBeforeCoupon: json['price_before_coupon'] as num?,
      haveCoupon: json['have_coupon'] as bool?,
      couponValue: json['coupon_value'] as int?,
      vatValue: json['vat_value'] as num? ?? 0,
      adminCommissionValue: json['admin_commission_value'] as num?,
      totalPrice: json['total_price'] as num?,
      payType: json['pay_type'] as int?,
      payTypeText: json['pay_type_text'] as String?,
      status: json['status'] as int?,
      statusText: json['status_text'] as String?,
      payStatus: json['pay_status'] as int?,
      payStatusText: json['pay_status_text'] as String?,
      createdAt: json['created_at'] as String?,
    );

Map<String, dynamic> _$OrderCreatedModelToJson(OrderCreatedModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'order_num': instance.orderNum,
      'user_name': instance.userName,
      'order_price': instance.orderPrice,
      'price_before_coupon': instance.priceBeforeCoupon,
      'have_coupon': instance.haveCoupon,
      'coupon_value': instance.couponValue,
      'vat_value': instance.vatValue,
      'admin_commission_value': instance.adminCommissionValue,
      'total_price': instance.totalPrice,
      'pay_type': instance.payType,
      'pay_type_text': instance.payTypeText,
      'status': instance.status,
      'status_text': instance.statusText,
      'pay_status': instance.payStatus,
      'pay_status_text': instance.payStatusText,
      'created_at': instance.createdAt,
    };
