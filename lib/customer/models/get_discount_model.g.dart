// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_discount_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetDiscountModel _$GetDiscountModelFromJson(Map<String, dynamic> json) =>
    GetDiscountModel(
      discAmount: json['disc_amount'] as num?,
      coupon: json['coupon'] == null
          ? null
          : CouponModel.fromJson(json['coupon'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$GetDiscountModelToJson(GetDiscountModel instance) =>
    <String, dynamic>{
      'disc_amount': instance.discAmount,
      'coupon': instance.coupon,
    };
