import 'package:base_flutter/customer/models/offer_model.dart';
import 'package:base_flutter/customer/models/sections_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'get_offers_model.g.dart'; 

@JsonSerializable( ignoreUnannotated: false)
class GetOffersModel {
  @JsonKey(name: 'sections')
  List<SectionsModel>? sectionsModel;
  @JsonKey(name: 'offers')
  List<OfferModel>? offers;

  GetOffersModel({this.sectionsModel, this.offers});

   factory GetOffersModel.fromJson(Map<String, dynamic> json) => _$GetOffersModelFromJson(json);

   Map<String, dynamic> toJson() => _$GetOffersModelToJson(this);
}


