import 'package:base_flutter/customer/models/comment_model.dart';
import 'package:base_flutter/customer/models/service_file_model.dart';
import 'package:base_flutter/customer/models/service_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'get_service_details.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class GetServiceDetails {
  @JsonKey(name: 'service')
  ServiceModel? service;
  @JsonKey(name: 'service_files')
  List<ServiceFileModel>? serviceFiles;
  @JsonKey(name: 'comments')
  List<CommentModel>? comments;

  GetServiceDetails({this.service, this.serviceFiles, this.comments});

   factory GetServiceDetails.fromJson(Map<String, dynamic> json) => _$GetServiceDetailsFromJson(json);

   Map<String, dynamic> toJson() => _$GetServiceDetailsToJson(this);
}


