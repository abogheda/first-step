import 'package:json_annotation/json_annotation.dart';

part 'offer_model.g.dart';

@JsonSerializable( ignoreUnannotated: false)
class OfferModel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'title')
  String? title;
  @JsonKey(name: 'description')
  String? description;
  @JsonKey(name: 'price')
  num? price;
  @JsonKey(name: 'price_before')
  num? priceBefore;
  @JsonKey(name: 'price_after')
  num? priceAfter;
  @JsonKey(name: 'image')
  String? image;
  @JsonKey(name: 'avg_rate')
  num? avgRate;
  @JsonKey(name: 'category_id')
  int categoryId;
  @JsonKey(name: 'category_name')
  String? categoryName;
  @JsonKey(name: 'sub_category_id')
  int subCategoryId;
  @JsonKey(name: 'sub_category_name')
  String? subCategoryName;
  @JsonKey(name: 'offer_category_id')
  int offerCategoryId;
  @JsonKey(name: 'offer_category_name')
  String? offerCategoryName;
  @JsonKey(name: 'provider_id')
  int providerId;
  @JsonKey(name: 'provider_name')
  String? providerName;
  @JsonKey(name: 'is_favorite')
  bool? isFavorite;


  OfferModel(
      {required this.id,
      this.title,
      this.description,
      this.price,
      this.priceAfter,
      this.image,
      this.avgRate,
      this.isFavorite,
      required this.categoryId,
      this.categoryName,
      required this.subCategoryId,
      this.subCategoryName,
      required this.offerCategoryId,
      this.offerCategoryName,
      required this.providerId,
      this.providerName});

  factory OfferModel.fromJson(Map<String, dynamic> json) =>
      _$OfferModelFromJson(json);

  Map<String, dynamic> toJson() => _$OfferModelToJson(this);
}
