// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service_file_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ServiceFileModel _$ServiceFileModelFromJson(Map<String, dynamic> json) =>
    ServiceFileModel(
      id: json['id'] as int,
      file: json['file'] as String?,
    );

Map<String, dynamic> _$ServiceFileModelToJson(ServiceFileModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'file': instance.file,
    };
