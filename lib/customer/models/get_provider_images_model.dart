import 'package:base_flutter/customer/models/service_file_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'get_provider_images_model.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class GetProviderImagesModel {
  @JsonKey(name: 'provider_files')
  List<ServiceFileModel>? providerFiles;

  GetProviderImagesModel({this.providerFiles});

   factory GetProviderImagesModel.fromJson(Map<String, dynamic> json) => _$GetProviderImagesModelFromJson(json);

   Map<String, dynamic> toJson() => _$GetProviderImagesModelToJson(this);
}



