// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_provider_images_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetProviderImagesModel _$GetProviderImagesModelFromJson(
        Map<String, dynamic> json) =>
    GetProviderImagesModel(
      providerFiles: (json['provider_files'] as List<dynamic>?)
          ?.map((e) => ServiceFileModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GetProviderImagesModelToJson(
        GetProviderImagesModel instance) =>
    <String, dynamic>{
      'provider_files': instance.providerFiles,
    };
