import 'package:json_annotation/json_annotation.dart'; 

part 'get_favourite_services_model.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class GetFavouriteServicesModel {
  @JsonKey(name: 'id')
  int? id;
  @JsonKey(name: 'title')
  String? title;
  @JsonKey(name: 'description')
  String? description;
  @JsonKey(name: 'price')
  int? price;
  @JsonKey(name: 'image')
  String? image;
  @JsonKey(name: 'avg_rate')
  num? avgRate;
  @JsonKey(name: 'category_id')
  int? categoryId;
  @JsonKey(name: 'category_name')
  String? categoryName;
  @JsonKey(name: 'sub_category_id')
  int? subCategoryId;
  @JsonKey(name: 'sub_category_name')
  String? subCategoryName;
  @JsonKey(name: 'service_category_id')
  int? serviceCategoryId;
  @JsonKey(name: 'service_category_name')
  String? serviceCategoryName;
  @JsonKey(name: 'provider_id')
  int? providerId;
  @JsonKey(name: 'provider_name')
  String? providerName;
  @JsonKey(name: 'is_favorite')
  bool? isFavorite;

  GetFavouriteServicesModel({this.id, this.title, this.description, this.price, this.image, this.avgRate, this.categoryId, this.categoryName, this.subCategoryId, this.subCategoryName, this.serviceCategoryId, this.serviceCategoryName, this.providerId, this.providerName, this.isFavorite});

   factory GetFavouriteServicesModel.fromJson(Map<String, dynamic> json) => _$GetFavouriteServicesModelFromJson(json);

   Map<String, dynamic> toJson() => _$GetFavouriteServicesModelToJson(this);
}

