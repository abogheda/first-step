import 'package:json_annotation/json_annotation.dart'; 

part 'order_created_model.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class OrderCreatedModel {
  @JsonKey(name: 'id')
  int? id;
  @JsonKey(name: 'order_num')
  String? orderNum;
  @JsonKey(name: 'user_name')
  String? userName;
  @JsonKey(name: 'order_price')
  num? orderPrice;
  @JsonKey(name: 'price_before_coupon')
  num? priceBeforeCoupon;
  @JsonKey(name: 'have_coupon')
  bool? haveCoupon;
  @JsonKey(name: 'coupon_value')
  int? couponValue;
  @JsonKey(name: 'vat_value')
  num vatValue;
  @JsonKey(name: 'admin_commission_value')
  num? adminCommissionValue;
  @JsonKey(name: 'total_price')
  num? totalPrice;
  @JsonKey(name: 'pay_type')
  int? payType;
  @JsonKey(name: 'pay_type_text')
  String? payTypeText;
  @JsonKey(name: 'status')
  int? status;
  @JsonKey(name: 'status_text')
  String? statusText;
  @JsonKey(name: 'pay_status')
  int? payStatus;
  @JsonKey(name: 'pay_status_text')
  String? payStatusText;
  @JsonKey(name: 'created_at')
  String? createdAt;

  OrderCreatedModel({this.id, this.orderNum, this.userName, this.orderPrice, this.priceBeforeCoupon, this.haveCoupon, this.couponValue, this.vatValue = 0, this.adminCommissionValue, this.totalPrice, this.payType, this.payTypeText, this.status, this.statusText, this.payStatus, this.payStatusText, this.createdAt});

   factory OrderCreatedModel.fromJson(Map<String, dynamic> json) => _$OrderCreatedModelFromJson(json);

   Map<String, dynamic> toJson() => _$OrderCreatedModelToJson(this);
}

