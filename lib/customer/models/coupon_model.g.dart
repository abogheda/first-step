// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coupon_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CouponModel _$CouponModelFromJson(Map<String, dynamic> json) => CouponModel(
      type: json['type'] as String?,
      discount: json['discount'] as num?,
      id: json['id'] as int,
    );

Map<String, dynamic> _$CouponModelToJson(CouponModel instance) =>
    <String, dynamic>{
      'type': instance.type,
      'discount': instance.discount,
      'id': instance.id,
    };
