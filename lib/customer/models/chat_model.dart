import 'package:json_annotation/json_annotation.dart'; 

part 'chat_model.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class ChatModel {
  @JsonKey(name: 'room')
  Room? room;
  @JsonKey(name: 'members')
  List<Member>? members;
  @JsonKey(name: 'messages')
  Messages? messages;

  ChatModel({this.room, this.members, this.messages});

   factory ChatModel.fromJson(Map<String, dynamic> json) => _$ChatModelFromJson(json);

   Map<String, dynamic> toJson() => _$ChatModelToJson(this);
}

@JsonSerializable(ignoreUnannotated: false)
class Room {
  @JsonKey(name: 'id')
  int? id;

  Room({this.id});

   factory Room.fromJson(Map<String, dynamic> json) => _$RoomFromJson(json);

   Map<String, dynamic> toJson() => _$RoomToJson(this);
}

@JsonSerializable(ignoreUnannotated: false)
class Member {
  @JsonKey(name: 'id')
  int? id;
  @JsonKey(name: 'type')
  String? type;
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'image')
  String? image;

  Member({this.id, this.type, this.name, this.image});

   factory Member.fromJson(Map<String, dynamic> json) => _$MemberFromJson(json);

   Map<String, dynamic> toJson() => _$MemberToJson(this);
}

@JsonSerializable(ignoreUnannotated: false)
class Messages {
  @JsonKey(name: 'pagination')
  Pagination? pagination;
  @JsonKey(name: 'data')
  List<ChatMessage>? chatMessage;

  Messages({this.pagination, this.chatMessage});

   factory Messages.fromJson(Map<String, dynamic> json) => _$MessagesFromJson(json);

   Map<String, dynamic> toJson() => _$MessagesToJson(this);
}

@JsonSerializable(ignoreUnannotated: false)
class Pagination {
  @JsonKey(name: 'total_items')
  int? totalItems;
  @JsonKey(name: 'count_items')
  int? countItems;
  @JsonKey(name: 'per_page')
  int? perPage;
  @JsonKey(name: 'total_pages')
  int? totalPages;
  @JsonKey(name: 'current_page')
  int? currentPage;
  @JsonKey(name: 'next_page_url')
  String? nextPageUrl;
  @JsonKey(name: 'perv_page_url')
  String? pervPageUrl;

  Pagination({this.totalItems, this.countItems, this.perPage, this.totalPages, this.currentPage, this.nextPageUrl, this.pervPageUrl});

   factory Pagination.fromJson(Map<String, dynamic> json) => _$PaginationFromJson(json);

   Map<String, dynamic> toJson() => _$PaginationToJson(this);
}

@JsonSerializable(ignoreUnannotated: false)
class ChatMessage {
  @JsonKey(name: 'id')
  int? id;
  @JsonKey(name: 'is_sender')
  int? isSender;
  @JsonKey(name: 'body')
  String? body;
  @JsonKey(name: 'type')
  String? type;
  @JsonKey(name: 'duration')
  int? duration;
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'created_dt')
  String? createdDt;

  ChatMessage({this.id, this.isSender, this.body, this.type, this.duration, this.name, this.createdDt});

   factory ChatMessage.fromJson(Map<String, dynamic> json) => _$ChatMessageFromJson(json);

   Map<String, dynamic> toJson() => _$ChatMessageToJson(this);
}

