// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'provider_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProviderModel _$ProviderModelFromJson(Map<String, dynamic> json) =>
    ProviderModel(
      id: json['id'] as int,
      name: json['name'] as String?,
      email: json['email'] as String?,
      countryCode: json['country_code'] as String?,
      phone: json['phone'] as String?,
      fullPhone: json['full_phone'] as String?,
      image: json['image'] as String?,
      lat: json['lat'] as String?,
      lng: json['lng'] as String?,
      mapDesc: json['map_desc'] as String?,
      type: json['type'] as String?,
      lang: json['lang'] as String?,
      avgRate: json['avg_rate'] as String?,
      isNotify: json['is_notify'] as bool?,
      isFavorite: json['is_favorite'] as bool?,
      token: json['token'] as String?,
    );

Map<String, dynamic> _$ProviderModelToJson(ProviderModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'email': instance.email,
      'country_code': instance.countryCode,
      'phone': instance.phone,
      'full_phone': instance.fullPhone,
      'image': instance.image,
      'lat': instance.lat,
      'lng': instance.lng,
      'map_desc': instance.mapDesc,
      'type': instance.type,
      'lang': instance.lang,
      'avg_rate': instance.avgRate,
      'is_notify': instance.isNotify,
      'is_favorite': instance.isFavorite,
      'token': instance.token,
    };
