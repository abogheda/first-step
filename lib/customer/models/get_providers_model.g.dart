// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_providers_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetProvidersModel _$GetProvidersModelFromJson(Map<String, dynamic> json) =>
    GetProvidersModel(
      sliders: (json['sliders'] as List<dynamic>?)
          ?.map((e) => SliderModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      providers: (json['providers'] as List<dynamic>?)
          ?.map((e) => ProviderModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GetProvidersModelToJson(GetProvidersModel instance) =>
    <String, dynamic>{
      'sliders': instance.sliders,
      'providers': instance.providers,
    };
