// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_provider_rates_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetProviderRatesModel _$GetProviderRatesModelFromJson(
        Map<String, dynamic> json) =>
    GetProviderRatesModel(
      provider: json['provider'] == null
          ? null
          : ProviderModel.fromJson(json['provider'] as Map<String, dynamic>),
      providerRates: (json['provider_rates'] as List<dynamic>?)
          ?.map((e) => ProviderRatesModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GetProviderRatesModelToJson(
        GetProviderRatesModel instance) =>
    <String, dynamic>{
      'provider': instance.provider,
      'provider_rates': instance.providerRates,
    };
