// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_room_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetRoomModel _$GetRoomModelFromJson(Map<String, dynamic> json) => GetRoomModel(
      id: json['id'] as int?,
      members: (json['members'] as List<dynamic>?)
          ?.map((e) => Member.fromJson(e as Map<String, dynamic>))
          .toList(),
      lastMessageBody: json['last_message_body'] as String?,
      lastMessageCreatedDt: json['last_message_created_dt'] as String?,
    );

Map<String, dynamic> _$GetRoomModelToJson(GetRoomModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'members': instance.members,
      'last_message_body': instance.lastMessageBody,
      'last_message_created_dt': instance.lastMessageCreatedDt,
    };

Member _$MemberFromJson(Map<String, dynamic> json) => Member(
      id: json['id'] as int?,
      type: json['type'] as String?,
      name: json['name'] as String?,
      image: json['image'] as String?,
    );

Map<String, dynamic> _$MemberToJson(Member instance) => <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'name': instance.name,
      'image': instance.image,
    };
