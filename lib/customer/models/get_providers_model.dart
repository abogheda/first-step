import 'package:base_flutter/customer/models/provider_model.dart';
import 'package:base_flutter/customer/models/slider_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'get_providers_model.g.dart';

@JsonSerializable( ignoreUnannotated: false)
class GetProvidersModel {
  @JsonKey(name: 'sliders')
  List<SliderModel>? sliders;

  @JsonKey(name: 'providers')
  List<ProviderModel>? providers;

  GetProvidersModel({this.sliders,  this.providers});

   factory GetProvidersModel.fromJson(Map<String, dynamic> json) => _$GetProvidersModelFromJson(json);

   Map<String, dynamic> toJson() => _$GetProvidersModelToJson(this);
}


