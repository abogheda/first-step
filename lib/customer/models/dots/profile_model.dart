
import 'dart:io';

class ProfileModel {
  String? name;
  String? address;
  String? lat;
  String? lng;
  String? email;
  File? image;
  String? phone;

  ProfileModel({
    this.name,
    this.phone,
    this.email,
    this.image,
    this.lat,
    this.lng,
    this.address,
  });

  Map<String, dynamic> toJson() => {
    "map_desc": address,
    "lat": lat,
    "lng": lng,
    "phone": phone,
    "name": name,
    "email": email,
    "image": image,
  };
}