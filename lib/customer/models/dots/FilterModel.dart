class FilterModel {
  int id;
  String name;

  FilterModel({required this.id, required this.name});

  static List<FilterModel> get filters => [
        FilterModel(id: 0, name: "الاقرب مني"),
        FilterModel(id: 1, name: "الاعلي تقييما"),
      ];
}
