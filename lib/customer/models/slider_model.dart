import 'package:json_annotation/json_annotation.dart'; 

part 'slider_model.g.dart'; 

@JsonSerializable( ignoreUnannotated: false)
class SliderModel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'title')
  String? title;
  @JsonKey(name: 'description')
  String? description;
  @JsonKey(name: 'image')
  String? image;

  SliderModel({required this.id, this.title, this.description, this.image});

   factory SliderModel.fromJson(Map<String, dynamic> json) => _$SliderModelFromJson(json);

   Map<String, dynamic> toJson() => _$SliderModelToJson(this);
}

