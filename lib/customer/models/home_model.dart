import 'package:base_flutter/customer/models/slider_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'category_model.dart';

part 'home_model.g.dart'; 

@JsonSerializable( ignoreUnannotated: false)
class HomeModel {
  @JsonKey(name: 'categories')
  List<CategoryModel> categories;
  @JsonKey(name: 'sliders')
  List<SliderModel> sliders;
  @JsonKey(name: 'user_location')
  String userLocation;

  HomeModel({required this.categories,required this.sliders,
    this.userLocation = ''});

   factory HomeModel.fromJson(Map<String, dynamic> json) => _$HomeModelFromJson(json);

   Map<String, dynamic> toJson() => _$HomeModelToJson(this);
}



