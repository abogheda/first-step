// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_rooms.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatRooms _$ChatRoomsFromJson(Map<String, dynamic> json) => ChatRooms(
      id: (json['id'] as num).toDouble(),
      lastMessageBody: json['last_message_body'] as String,
      lastMessageCreatedDt: json['last_message_created_dt'] as String,
      members: (json['Memebers'] as List<dynamic>)
          .map((e) => Member.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ChatRoomsToJson(ChatRooms instance) => <String, dynamic>{
      'id': instance.id,
      'last_message_body': instance.lastMessageBody,
      'last_message_created_dt': instance.lastMessageCreatedDt,
      'Memebers': instance.members,
    };

Member _$MemberFromJson(Map<String, dynamic> json) => Member(
      id: json['id'] as int,
      name: json['name'] as String,
      avatar: json['avatar'] as String,
    );

Map<String, dynamic> _$MemberToJson(Member instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'avatar': instance.avatar,
    };
