// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatModel _$ChatModelFromJson(Map<String, dynamic> json) => ChatModel(
      room: json['room'] == null
          ? null
          : Room.fromJson(json['room'] as Map<String, dynamic>),
      members: (json['members'] as List<dynamic>?)
          ?.map((e) => Member.fromJson(e as Map<String, dynamic>))
          .toList(),
      messages: json['messages'] == null
          ? null
          : Messages.fromJson(json['messages'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ChatModelToJson(ChatModel instance) => <String, dynamic>{
      'room': instance.room,
      'members': instance.members,
      'messages': instance.messages,
    };

Room _$RoomFromJson(Map<String, dynamic> json) => Room(
      id: json['id'] as int?,
    );

Map<String, dynamic> _$RoomToJson(Room instance) => <String, dynamic>{
      'id': instance.id,
    };

Member _$MemberFromJson(Map<String, dynamic> json) => Member(
      id: json['id'] as int?,
      type: json['type'] as String?,
      name: json['name'] as String?,
      image: json['image'] as String?,
    );

Map<String, dynamic> _$MemberToJson(Member instance) => <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'name': instance.name,
      'image': instance.image,
    };

Messages _$MessagesFromJson(Map<String, dynamic> json) => Messages(
      pagination: json['pagination'] == null
          ? null
          : Pagination.fromJson(json['pagination'] as Map<String, dynamic>),
      chatMessage: (json['data'] as List<dynamic>?)
          ?.map((e) => ChatMessage.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MessagesToJson(Messages instance) => <String, dynamic>{
      'pagination': instance.pagination,
      'data': instance.chatMessage,
    };

Pagination _$PaginationFromJson(Map<String, dynamic> json) => Pagination(
      totalItems: json['total_items'] as int?,
      countItems: json['count_items'] as int?,
      perPage: json['per_page'] as int?,
      totalPages: json['total_pages'] as int?,
      currentPage: json['current_page'] as int?,
      nextPageUrl: json['next_page_url'] as String?,
      pervPageUrl: json['perv_page_url'] as String?,
    );

Map<String, dynamic> _$PaginationToJson(Pagination instance) =>
    <String, dynamic>{
      'total_items': instance.totalItems,
      'count_items': instance.countItems,
      'per_page': instance.perPage,
      'total_pages': instance.totalPages,
      'current_page': instance.currentPage,
      'next_page_url': instance.nextPageUrl,
      'perv_page_url': instance.pervPageUrl,
    };

ChatMessage _$ChatMessageFromJson(Map<String, dynamic> json) => ChatMessage(
      id: json['id'] as int?,
      isSender: json['is_sender'] as int?,
      body: json['body'] as String?,
      type: json['type'] as String?,
      duration: json['duration'] as int?,
      name: json['name'] as String?,
      createdDt: json['created_dt'] as String?,
    );

Map<String, dynamic> _$ChatMessageToJson(ChatMessage instance) =>
    <String, dynamic>{
      'id': instance.id,
      'is_sender': instance.isSender,
      'body': instance.body,
      'type': instance.type,
      'duration': instance.duration,
      'name': instance.name,
      'created_dt': instance.createdDt,
    };
