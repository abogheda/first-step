import 'package:json_annotation/json_annotation.dart'; 

part 'pagination_model.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class PaginationModel {
  @JsonKey(name: 'total_items')
  int? totalItems;
  @JsonKey(name: 'count_items')
  int? countItems;
  @JsonKey(name: 'per_page')
  String? perPage;
  @JsonKey(name: 'total_pages')
  int? totalPages;
  @JsonKey(name: 'current_page')
  int? currentPage;
  @JsonKey(name: 'next_page_url')
  String? nextPageUrl;
  @JsonKey(name: 'perv_page_url')
  String? pervPageUrl;

  PaginationModel({this.totalItems, this.countItems, this.perPage, this.totalPages, this.currentPage, this.nextPageUrl, this.pervPageUrl});

   factory PaginationModel.fromJson(Map<String, dynamic> json) => _$PaginationModelFromJson(json);

   Map<String, dynamic> toJson() => _$PaginationModelToJson(this);
}

