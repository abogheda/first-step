import 'package:base_flutter/customer/models/cart_item_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cart_model.g.dart'; 

@JsonSerializable( ignoreUnannotated: false)
class CartModel {
  @JsonKey(name: 'cartItems')
  List<CartItemModel>? cartItems;
  @JsonKey(name: 'orderPrice')
  num? orderPrice;

  CartModel({this.cartItems, this.orderPrice});

   factory CartModel.fromJson(Map<String, dynamic> json) => _$CartModelFromJson(json);

   Map<String, dynamic> toJson() => _$CartModelToJson(this);
}



