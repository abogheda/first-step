import 'package:json_annotation/json_annotation.dart';

import 'coupon_model.dart';

part 'get_discount_model.g.dart'; 

@JsonSerializable( ignoreUnannotated: false)
class GetDiscountModel {
  @JsonKey(name: 'disc_amount')
  num? discAmount;
  @JsonKey(name: 'coupon')
  CouponModel? coupon;

  GetDiscountModel({this.discAmount, this.coupon});

   factory GetDiscountModel.fromJson(Map<String, dynamic> json) => _$GetDiscountModelFromJson(json);

   Map<String, dynamic> toJson() => _$GetDiscountModelToJson(this);
}


