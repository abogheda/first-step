import 'package:json_annotation/json_annotation.dart'; 

part 'provider_timing_model.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class ProviderTimingModel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'day')
  String? day;
  @JsonKey(name: 'from')
  String? from;
  @JsonKey(name: 'to')
  String? to;

  ProviderTimingModel({required this.id, this.day, this.from, this.to});

   factory ProviderTimingModel.fromJson(Map<String, dynamic> json) => _$ProviderTimingModelFromJson(json);

   Map<String, dynamic> toJson() => _$ProviderTimingModelToJson(this);
}

