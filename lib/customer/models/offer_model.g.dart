// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'offer_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OfferModel _$OfferModelFromJson(Map<String, dynamic> json) => OfferModel(
      id: json['id'] as int,
      title: json['title'] as String?,
      description: json['description'] as String?,
      price: json['price'] as num?,
      priceAfter: json['price_after'] as num?,
      image: json['image'] as String?,
      avgRate: json['avg_rate'] as num?,
      isFavorite: json['is_favorite'] as bool?,
      categoryId: json['category_id'] as int,
      categoryName: json['category_name'] as String?,
      subCategoryId: json['sub_category_id'] as int,
      subCategoryName: json['sub_category_name'] as String?,
      offerCategoryId: json['offer_category_id'] as int,
      offerCategoryName: json['offer_category_name'] as String?,
      providerId: json['provider_id'] as int,
      providerName: json['provider_name'] as String?,
    )..priceBefore = json['price_before'] as num?;

Map<String, dynamic> _$OfferModelToJson(OfferModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'price': instance.price,
      'price_before': instance.priceBefore,
      'price_after': instance.priceAfter,
      'image': instance.image,
      'avg_rate': instance.avgRate,
      'category_id': instance.categoryId,
      'category_name': instance.categoryName,
      'sub_category_id': instance.subCategoryId,
      'sub_category_name': instance.subCategoryName,
      'offer_category_id': instance.offerCategoryId,
      'offer_category_name': instance.offerCategoryName,
      'provider_id': instance.providerId,
      'provider_name': instance.providerName,
      'is_favorite': instance.isFavorite,
    };
