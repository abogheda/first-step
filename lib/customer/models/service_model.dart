import 'package:json_annotation/json_annotation.dart'; 

part 'service_model.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class ServiceModel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'title')
  String? title;
  @JsonKey(name: 'description')
  String? description;
  @JsonKey(name: 'price')
  num? price;
  @JsonKey(name: 'image')
  String? image;
  @JsonKey(name: 'avg_rate')
  num? avgRate;
  @JsonKey(name: 'category_id')
  int? categoryId;
  @JsonKey(name: 'category_name')
  String? categoryName;
  @JsonKey(name: 'sub_category_id')
  int? subCategoryId;
  @JsonKey(name: 'sub_category_name')
  String? subCategoryName;
  @JsonKey(name: 'service_category_id')
  int? serviceCategoryId;
  @JsonKey(name: 'service_category_name')
  String? serviceCategoryName;
  @JsonKey(name: 'provider_id')
  int? providerId;
  @JsonKey(name: 'provider_name')
  String? providerName;
  @JsonKey(name: 'is_favorite')
  bool? isFavorite;

  ServiceModel({required this.id, this.title, this.description, this.price, this.image,this.isFavorite ,this.avgRate, this.categoryId, this.categoryName, this.subCategoryId, this.subCategoryName, this.serviceCategoryId, this.serviceCategoryName, this.providerId, this.providerName});

   factory ServiceModel.fromJson(Map<String, dynamic> json) => _$ServiceModelFromJson(json);

   Map<String, dynamic> toJson() => _$ServiceModelToJson(this);
}

