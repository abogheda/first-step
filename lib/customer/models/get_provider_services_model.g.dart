// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_provider_services_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetProviderServicesModel _$GetProviderServicesModelFromJson(
        Map<String, dynamic> json) =>
    GetProviderServicesModel(
      serviceCategories: (json['serviceCategories'] as List<dynamic>?)
          ?.map((e) => SectionsModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      services: (json['services'] as List<dynamic>?)
          ?.map((e) => ServiceModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GetProviderServicesModelToJson(
        GetProviderServicesModel instance) =>
    <String, dynamic>{
      'serviceCategories': instance.serviceCategories,
      'services': instance.services,
    };
