import 'package:json_annotation/json_annotation.dart'; 

part 'get_coupon_model.g.dart';

@JsonSerializable( ignoreUnannotated: false)
class GetCouponModel {
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'type')
  String? type;
  @JsonKey(name: 'discount')
  int? discount;
  @JsonKey(name: 'expire_date')
  String? expireDate;

  GetCouponModel({this.name, this.type, this.discount, this.expireDate});

   factory GetCouponModel.fromJson(Map<String, dynamic> json) => _$GetCouponModelFromJson(json);

   Map<String, dynamic> toJson() => _$GetCouponModelToJson(this);
}

