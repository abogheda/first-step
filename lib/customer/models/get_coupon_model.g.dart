// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_coupon_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetCouponModel _$GetCouponModelFromJson(Map<String, dynamic> json) =>
    GetCouponModel(
      name: json['name'] as String?,
      type: json['type'] as String?,
      discount: json['discount'] as int?,
      expireDate: json['expire_date'] as String?,
    );

Map<String, dynamic> _$GetCouponModelToJson(GetCouponModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'type': instance.type,
      'discount': instance.discount,
      'expire_date': instance.expireDate,
    };
