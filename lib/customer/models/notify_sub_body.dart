import 'package:json_annotation/json_annotation.dart'; 

part 'notify_sub_body.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class NotifySubBody {
  @JsonKey(name: 'sender')
  int sender;
  @JsonKey(name: 'invoice_id')
  int invoiceId;
  @JsonKey(name: 'invoice_num')
  String invoiceNum;
  @JsonKey(name: 'sender_name')
  String? senderName;
  @JsonKey(name: 'type')
  String? type;

  NotifySubBody({required this.sender, this.senderName, this.type,required this.invoiceId,required this.invoiceNum});

   factory NotifySubBody.fromJson(Map<String, dynamic> json) => _$NotifySubBodyFromJson(json);

   Map<String, dynamic> toJson() => _$NotifySubBodyToJson(this);
}

