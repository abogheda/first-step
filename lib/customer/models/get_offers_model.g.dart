// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_offers_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetOffersModel _$GetOffersModelFromJson(Map<String, dynamic> json) =>
    GetOffersModel(
      sectionsModel: (json['sections'] as List<dynamic>?)
          ?.map((e) => SectionsModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      offers: (json['offers'] as List<dynamic>?)
          ?.map((e) => OfferModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GetOffersModelToJson(GetOffersModel instance) =>
    <String, dynamic>{
      'sections': instance.sectionsModel,
      'offers': instance.offers,
    };
