// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pagination_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaginationModel _$PaginationModelFromJson(Map<String, dynamic> json) =>
    PaginationModel(
      totalItems: json['total_items'] as int?,
      countItems: json['count_items'] as int?,
      perPage: json['per_page'] as String?,
      totalPages: json['total_pages'] as int?,
      currentPage: json['current_page'] as int?,
      nextPageUrl: json['next_page_url'] as String?,
      pervPageUrl: json['perv_page_url'] as String?,
    );

Map<String, dynamic> _$PaginationModelToJson(PaginationModel instance) =>
    <String, dynamic>{
      'total_items': instance.totalItems,
      'count_items': instance.countItems,
      'per_page': instance.perPage,
      'total_pages': instance.totalPages,
      'current_page': instance.currentPage,
      'next_page_url': instance.nextPageUrl,
      'perv_page_url': instance.pervPageUrl,
    };
