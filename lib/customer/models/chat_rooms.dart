// ignore_for_file: deprecated_member_use

import 'package:json_annotation/json_annotation.dart';

part 'chat_rooms.g.dart';

@JsonSerializable( ignoreUnannotated: false)
class ChatRooms {
  @JsonKey(name: 'id')
  final double id;
  @JsonKey(name: 'last_message_body')
  final String lastMessageBody;
  @JsonKey(name: 'last_message_created_dt')
  final String lastMessageCreatedDt;
  @JsonKey(name: 'Memebers')
  final List<Member> members;

  ChatRooms({
    required this.id,
    required this.lastMessageBody,
    required this.lastMessageCreatedDt,
    required this.members,
  });

  factory ChatRooms.fromJson(Map<String, dynamic> json) =>
      _$ChatRoomsFromJson(json);

  Map<String, dynamic> toJson() => _$ChatRoomsToJson(this);
}

@JsonSerializable( ignoreUnannotated: false)
class Member {
  @JsonKey(name: 'id')
  final int id;
  @JsonKey(name: 'name')
  final String name;
  @JsonKey(name: 'avatar')
  final String avatar;

  Member({
    required this.id,
    required this.name,
    required this.avatar,
  });
  factory Member.fromJson(Map<String, dynamic> json) => _$MemberFromJson(json);

  Map<String, dynamic> toJson() => _$MemberToJson(this);
}
