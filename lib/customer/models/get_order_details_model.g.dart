// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_order_details_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetOrderDetailsModel _$GetOrderDetailsModelFromJson(
        Map<String, dynamic> json) =>
    GetOrderDetailsModel(
      invoiceId: json['invoice_id'] as int?,
      orderId: json['order_id'] as int?,
      orderNum: json['order_num'] as String?,
      invoiceNum: json['invoice_num'] as String?,
      serviceName: json['service_name'] as String?,
      providerName: json['provider_name'] as String?,
      providerCommericalNumber: json['provider_commerical_number'] as String?,
      providerImage: json['provider_image'] as String?,
      priceBeforeCoupon: json['price_before_coupon'] as num?,
      totalPrice: json['total_price'] as num?,
      haveCoupon: json['have_coupon'] as bool?,
      couponValue: json['coupon_value'] as int?,
      payType: json['pay_type'] as int?,
      payTypeText: json['pay_type_text'] as String?,
      status: json['status'] as int?,
      statusText: json['status_text'] as String?,
      payStatus: json['pay_status'] as int?,
      payStatusText: json['pay_status_text'] as String?,
      serviceRated: json['service_rated'] as bool?,
      providerRated: json['provider_rated'] as bool?,
      roomId: json['room_id'] as int?,
      date: json['date'] as String?,
      createdAt: json['created_at'] as String?,
    );

Map<String, dynamic> _$GetOrderDetailsModelToJson(
        GetOrderDetailsModel instance) =>
    <String, dynamic>{
      'order_id': instance.orderId,
      'invoice_id': instance.invoiceId,
      'order_num': instance.orderNum,
      'invoice_num': instance.invoiceNum,
      'service_name': instance.serviceName,
      'provider_name': instance.providerName,
      'provider_commerical_number': instance.providerCommericalNumber,
      'provider_image': instance.providerImage,
      'price_before_coupon': instance.priceBeforeCoupon,
      'total_price': instance.totalPrice,
      'have_coupon': instance.haveCoupon,
      'coupon_value': instance.couponValue,
      'pay_type': instance.payType,
      'pay_type_text': instance.payTypeText,
      'status': instance.status,
      'status_text': instance.statusText,
      'pay_status': instance.payStatus,
      'pay_status_text': instance.payStatusText,
      'service_rated': instance.serviceRated,
      'provider_rated': instance.providerRated,
      'room_id': instance.roomId,
      'date': instance.date,
      'created_at': instance.createdAt,
    };
