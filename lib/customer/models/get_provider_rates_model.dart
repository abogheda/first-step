import 'package:base_flutter/customer/models/provider_model.dart';
import 'package:base_flutter/customer/models/provider_rates_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'get_provider_rates_model.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class GetProviderRatesModel {
  @JsonKey(name: 'provider')
  ProviderModel? provider;
  @JsonKey(name: 'provider_rates')
  List<ProviderRatesModel>? providerRates;

  GetProviderRatesModel({this.provider, this.providerRates});

   factory GetProviderRatesModel.fromJson(Map<String, dynamic> json) => _$GetProviderRatesModelFromJson(json);

   Map<String, dynamic> toJson() => _$GetProviderRatesModelToJson(this);
}


