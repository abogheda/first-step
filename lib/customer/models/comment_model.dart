import 'package:json_annotation/json_annotation.dart'; 

part 'comment_model.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class CommentModel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'value')
  num? value;
  @JsonKey(name: 'comment')
  String? comment;
  @JsonKey(name: 'user_name')
  String? userName;
  @JsonKey(name: 'user_image')
  String? userImage;
  @JsonKey(name: 'created_at')
  String? createdAt;

  CommentModel({required this.id, this.value, this.comment, this.userName, this.userImage, this.createdAt});

   factory CommentModel.fromJson(Map<String, dynamic> json) => _$CommentModelFromJson(json);

   Map<String, dynamic> toJson() => _$CommentModelToJson(this);
}

