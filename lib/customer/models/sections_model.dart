import 'package:json_annotation/json_annotation.dart'; 

part 'sections_model.g.dart'; 

@JsonSerializable( ignoreUnannotated: false)
class SectionsModel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'title')
  String? title;
  @JsonKey(name: 'description')
  String? description;

  SectionsModel({required this.id, this.title, this.description});

   factory SectionsModel.fromJson(Map<String, dynamic> json) => _$SectionsModelFromJson(json);

   Map<String, dynamic> toJson() => _$SectionsModelToJson(this);
}

