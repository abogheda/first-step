// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notify_sub_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotifySubBody _$NotifySubBodyFromJson(Map<String, dynamic> json) =>
    NotifySubBody(
      sender: json['sender'] as int,
      senderName: json['sender_name'] as String?,
      type: json['type'] as String?,
      invoiceId: json['invoice_id'] as int,
      invoiceNum: json['invoice_num'] as String,
    );

Map<String, dynamic> _$NotifySubBodyToJson(NotifySubBody instance) =>
    <String, dynamic>{
      'sender': instance.sender,
      'invoice_id': instance.invoiceId,
      'invoice_num': instance.invoiceNum,
      'sender_name': instance.senderName,
      'type': instance.type,
    };
