
import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/models/cart_model.dart';
import 'package:base_flutter/customer/models/chat_model.dart';
import 'package:base_flutter/customer/models/get_coupon_model.dart';
import 'package:base_flutter/customer/models/dots/profile_model.dart';
import 'package:base_flutter/customer/models/get_offers_model.dart';
import 'package:base_flutter/customer/models/get_order_details_model.dart';
import 'package:base_flutter/customer/models/get_provider_services_model.dart';
import 'package:base_flutter/customer/models/get_providers_model.dart';
import 'package:base_flutter/customer/models/offer_model.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:dio_helper/ModaLs/LoadingDialog.dart';
import 'package:dio_helper/http/GenericHttp.dart';
import 'package:dio_helper/utils/DioUtils.dart';
import 'package:dio_helper/utils/GlobalState.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../general/blocks/auth_cubit/auth_cubit.dart';
import '../../general/resources/handleHttpData.dart';
import '../../general/utilities/utils_functions/ApiNames.dart';
import '../../general/utilities/utils_functions/UtilsImports.dart';
import '../models/get_discount_model.dart';
import '../models/get_favourite_services_model.dart';
import '../models/get_provider_images_model.dart';
import '../models/get_provider_rates_model.dart';
import '../models/get_room_model.dart';
import '../models/get_service_details.dart';
import '../models/home_model.dart';
import '../models/invoice_model.dart';
import '../models/notifications_model.dart';
import '../models/order_created_model.dart';
import '../models/order_model.dart';
import '../models/provider_model.dart';
import '../models/provider_timing_model.dart';
import '../models/sub_category_model.dart';




part 'customer_http_methods.dart';
part 'customer_repository.dart';