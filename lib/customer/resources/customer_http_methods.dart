part of 'customer_repository_imports.dart';

class CustomerHttpMethods {
  final BuildContext context;

  FirebaseMessaging messaging = FirebaseMessaging.instance;

  CustomerHttpMethods(this.context);

  Future<bool> logOut() async {
    // String? _token = await messaging.getToken();
    final data = await GenericHttp<bool>(context).callApi(
      name: ApiNames.signOut,
      returnType: ReturnType.Type,
      showLoader: true,
      methodType: MethodType.Delete,
      returnDataFun: (data) {
        print(data);
        return HandleData.instance.handlePostData(data, context);
      },
    );
    if (data != null) {
      context.read<AuthCubit>().onUpdateAuth(false);
      var auth = context.read<AuthCubit>().state.authorized;
      print("----------->$auth");
      Utils.clearSavedData();
      GlobalState.instance.set("token", "");
      CustomToast.showSimpleToast(msg: data["msg"]);
      AutoRouter.of(context).push(LoginRoute());
      return false;
    }
    return true;
  }

  Future<bool> updateUserProfile(ProfileModel model) async {
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.updateProfile + "?_method=put",
      json: model.toJson(),
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      toJsonFunc: (data) => data,
      showLoader: false,
    );
    return HandleData.instance.manipulateUpdateProfileData(result, context);
  }

  Future<bool> updateUserPassword(
      String newPassword, String oldPassword, String passConfirmation) async {
    Map<String, dynamic> body = {
      "password": "$newPassword",
      "old_password": "$oldPassword",
      "password_confirmation": "$passConfirmation",
    };
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.updatePassword + "?_method=patch",
      json: body,
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      toJsonFunc: (data) => data,
      showLoader: false,
    );
    if (result != null) {
      if (result["key"] == "success") {
        CustomToast.showSimpleToast(msg: result["msg"]);
        return true;
      } else {
        HandleData.instance.failure(result, context);
        return false;
      }
    }
    return false;
  }

  Future<bool> changeLanguage(String lang) async {
    String params = "?lang=$lang";
    dynamic data = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.changeLang + params,
      returnType: ReturnType.Type,
      showLoader: true,
      methodType: MethodType.Patch,
      returnDataFun: (data) =>
          HandleData.instance.handlePostData(data, context),
    );
    return (data != null);
  }

  Future<bool> switchNotify() async {
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.switchNotify,
      returnType: ReturnType.Type,
      methodType: MethodType.Patch,
      showLoader: true,
      toJsonFunc: (data) => data,
    );
    if (result != null) {
      if (result["key"] == "success") {
        CustomToast.showSimpleToast(msg: result["msg"]);
        if (result["data"] != null) {
          return result["data"]["notify"];
        }
      } else {
        return HandleData.instance.failure(result, context);
      }
    }
    return false;
  }

  Future<NotificationsModel> getNotifications(
      int pageNumber, int pageCount) async {
    NotificationsModel data = await GenericHttp<NotificationsModel>(context)
        .callApi(
            returnType: ReturnType.Model,
            methodType: MethodType.Get,
            returnDataFun: (data) {
              if (data['key'] == 'success') {
                return data;
              } else if (data['key'] == 'fail') {
                DioUtils.dismissDialog();
                return CustomToast.showSimpleToast(msg: data['msg']);
              } else if (data['key'] == 'exception') {
                DioUtils.dismissDialog();
                return CustomToast.showSimpleToast(
                    msg: tr(context, "errorOccurred"));
              } else if (data['key'] == 'blocked') {
                DioUtils.dismissDialog();
                Utils.unAuthenticatedFunc(context: context);
                // return CustomToast.showSimpleToast(msg: data['msg']);
              } else if (data['key'] == 'unauthenticated') {
                Utils.unAuthenticatedFunc(context: context);
                // return CustomToast.showSimpleToast(msg: data['msg']);
              }
            },
            toJsonFunc: (json) => NotificationsModel.fromJson(json),
            name: ApiNames.notifications +
                '?page=$pageNumber&paginate=$pageCount');
    return data;
  }

  Future<bool> deleteUser() async {
    var data = await GenericHttp<dynamic>(context).callApi(
        returnType: ReturnType.Type,
        methodType: MethodType.Delete,
        returnDataFun: (data) {
          if (data['key'] == 'success') {
            Utils.clearSavedData();
            CustomToast.showSimpleToast(msg: data['msg']);
            AutoRouter.of(context).push(SelectLangRoute());
            return true;
          } else if (data['key'] == 'fail') {
            DioUtils.dismissDialog();
            CustomToast.showSimpleToast(msg: data['msg']);
            return false;
          } else if (data['key'] == 'exception') {
            DioUtils.dismissDialog();
            CustomToast.showSimpleToast(msg: tr(context, "errorOccurred"));
            return false;
          } else if (data['key'] == 'blocked') {
            DioUtils.dismissDialog();
            Utils.unAuthenticatedFunc(context: context);
            // CustomToast.showSimpleToast(msg: data['msg']);
            return false;
          } else if (data['key'] == 'unauthenticated') {
            Utils.unAuthenticatedFunc(context: context);
            // CustomToast.showSimpleToast(msg: data['msg']);
            return false;
          } else {
            DioUtils.dismissDialog();
            CustomToast.showSimpleToast(msg: data['msg']);
            return false;
          }
        },
        name: ApiNames.deleteUser);
    return data;
  }

  Future<bool> deleteNotify() async {
    var data = await GenericHttp<dynamic>(context).callApi(
        returnType: ReturnType.Type,
        methodType: MethodType.Delete,
        returnDataFun: (data) {
          if (data['key'] == 'success') {
            CustomToast.showSimpleToast(msg: data['msg']);
            return true;
          } else if (data['key'] == 'fail') {
            DioUtils.dismissDialog();
            CustomToast.showSimpleToast(msg: data['msg']);
            return false;
          } else if (data['key'] == 'exception') {
            DioUtils.dismissDialog();
            CustomToast.showSimpleToast(msg: tr(context, "errorOccurred"));
            return false;
          } else if (data['key'] == 'blocked') {
            DioUtils.dismissDialog();
            Utils.unAuthenticatedFunc(context: context);
            // CustomToast.showSimpleToast(msg: data['msg']);
            return false;
          } else if (data['key'] == 'unauthenticated') {
            Utils.unAuthenticatedFunc(context: context);
            // CustomToast.showSimpleToast(msg: data['msg']);
            return false;
          } else {
            DioUtils.dismissDialog();
            CustomToast.showSimpleToast(msg: data['msg']);
            return false;
          }
        },
        name: ApiNames.deleteNotify);
    return data;
  }

  // Home
  Future<HomeModel?> getHomeData(bool refresh, String search) async {
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.home + "?search=$search",
      returnType: ReturnType.Type,
      showLoader: false,
      refresh: refresh,
      methodType: MethodType.Get,
      returnDataFun: (data) => HandleData.instance.handleGetData(data, context),
    );
    if (result != null) {
      return HomeModel.fromJson(result);
    }
    return null;
  }

  // coupons
  Future<List<GetCouponModel>> getCoupons(bool refresh) async {
    List<GetCouponModel> result =
        await GenericHttp<GetCouponModel>(context).callApi(
      returnType: ReturnType.List,
      methodType: MethodType.Get,
      refresh: refresh,
      returnDataFun: (data) =>
          HandleData.instance.handleGetData(data, context, fullData: false),
      toJsonFunc: (json) => GetCouponModel.fromJson(json),
      name: ApiNames.coupons,
    );
    return result;
  }

  Future<List<SubCategoryModel>> getSubCategories(bool refresh, int id) async {
    List<SubCategoryModel> result =
        await GenericHttp<SubCategoryModel>(context).callApi(
      returnType: ReturnType.List,
      methodType: MethodType.Get,
      refresh: refresh,
      returnDataFun: (data) {
        if (data["key"] == "success") {
          return data["data"]["categories"];
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
      toJsonFunc: (json) => SubCategoryModel.fromJson(json),
      name: ApiNames.subCategories + "/$id",
    );
    return result;
  }

  // subCategories
  Future<GetProvidersModel?> getProviders(
      bool refresh, int subCategoryId, String search, String location,String sort) async {
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.providers +
          "?sub_category_id=$subCategoryId&search=$search&location=$location&sort=$sort",
      returnType: ReturnType.Type,
      showLoader: false,
      refresh: refresh,
      methodType: MethodType.Get,
      returnDataFun: (data) => HandleData.instance.handleGetData(data, context),
    );
    if (result != null) {
      return GetProvidersModel.fromJson(result);
    }
    return null;
  }

  Future<GetOffersModel?> getOffers(
      bool refresh, int subCategoryId,int sectionId ,String searchText) async {
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.Offers +
          "?sub_category_id=$subCategoryId&search=$searchText&section_id=$sectionId",
      returnType: ReturnType.Type,
      showLoader: false,
      refresh: refresh,
      methodType: MethodType.Get,
      returnDataFun: (data) => HandleData.instance.handleGetData(data, context),
    );
    if (result != null) {
      return GetOffersModel.fromJson(result);
    }
    return null;
  }

  Future<bool> switchUserFav(int id) async {
    Map<String, dynamic> body = {
      "id": id,
    };
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.switchUserFavorite,
      json: body,
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      toJsonFunc: (data) => data,
      showLoader: true,
    );
    if (result != null) {
      if (result["key"] == "success") {
        CustomToast.showSimpleToast(msg: result["msg"]);
        return true;
      } else {
        HandleData.instance.failure(result, context);
        return false;
      }
    }
    return false;
  }

  Future<bool> switchServiceFav(int id) async {
    Map<String, dynamic> body = {
      "id": id,
    };
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.switchServiceFavorite,
      json: body,
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      toJsonFunc: (data) => data,
      showLoader: true,
    );
    if (result != null) {
      if (result["key"] == "success") {
        CustomToast.showSimpleToast(msg: result["msg"]);
        return true;
      } else {
        HandleData.instance.failure(result, context);
        return false;
      }
    }
    return false;
  }

  Future<bool> addToCart(int serviceId) async {
    Map<String, dynamic> body = {
      "service_id": serviceId,
    };
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.addToCart,
      json: body,
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      toJsonFunc: (data) => data,
      showLoader: true,
    );
    if (result != null) {
      if (result["key"] == "success") {
        CustomToast.showSimpleToast(msg: result["msg"]);
        return true;
      } else {
        HandleData.instance.failure(result, context);
        return false;
      }
    }
    return false;
  }

  Future<CartModel?> getCart(bool refresh) async {
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.getCart,
      returnType: ReturnType.Type,
      showLoader: false,
      refresh: refresh,
      methodType: MethodType.Get,
      returnDataFun: (data) => HandleData.instance.handleGetData(data, context),
    );
    if (result != null) {
      return CartModel.fromJson(result);
    }
    return null;
  }

  Future<GetDiscountModel?> checkCoupon(num totalPrice, String coupon) async {
    Map<String, dynamic> body = {
      "coupon_num": coupon,
      "total_price": totalPrice,
    };
    return await GenericHttp<GetDiscountModel>(context).callApi(
      name: ApiNames.checkCoupon,
      returnType: ReturnType.Model,
      showLoader: true,
      json: body,
      refresh: true,
      methodType: MethodType.Post,
      toJsonFunc: (json){

         return GetDiscountModel.fromJson(json);

      },
      returnDataFun: (data) {
        if (data["key"] == "success") {
          if (data["data"] != null) {
            return HandleData.instance.handlePostData(data, context,fullData: false);
          } else {
         return   HandleData.instance.failure(data['data'], context,);
          }
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
    ) as GetDiscountModel ;
  }

  Future<bool> removeFromCart(int serviceId) async {
    Map<String, dynamic> body = {
      "service_id": serviceId,
    };
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.removeFromCart,
      json: body,
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      // toJsonFunc: (data) => data,
      showLoader: true,
    );
    if (result != null) {
      if (result["key"] == "success") {
        CustomToast.showSimpleToast(msg: result["msg"]);
        return true;
      } else {
        HandleData.instance.failure(result, context);
        return false;
      }
    }
    return false;
  }

  Future<List<OfferModel>> getProviderOffers(bool refresh, int id) async {
    List<OfferModel> result = await GenericHttp<OfferModel>(context).callApi(
      returnType: ReturnType.List,
      methodType: MethodType.Get,
      refresh: refresh,
      returnDataFun: (data) {
        if (data["key"] == "success") {
          return data["data"]["offers"];
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
      toJsonFunc: (json) => OfferModel.fromJson(json),
      name: ApiNames.getProviderOffers + "?id=$id",
    );
    return result;
  }

  Future<ProviderModel> getProviderDetails(bool refresh, int id) async {
    var result = await GenericHttp<ProviderModel>(context).callApi(
      returnType: ReturnType.List,
      methodType: MethodType.Get,
      refresh: refresh,
      returnDataFun: (data) {
        if (data["key"] == "success") {
          return data["data"]["provider"];
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
      toJsonFunc: (json) => ProviderModel.fromJson(json),
      name: ApiNames.getProviderOffers + "?id=$id",
    );
    return result;
  }

  Future<GetProviderServicesModel?> getProviderServices(
      int id, String search,int sectionId) async {
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.getProviderServices + "?id=$id&search=$search&section_id=$sectionId",
      returnType: ReturnType.Model,
      showLoader: true,
      refresh: true,
      toJsonFunc: (json) => GetProviderServicesModel.fromJson(json),
      methodType: MethodType.Get,
      returnDataFun: (data) {
        if (data["key"] == "success") {
          DioUtils.dismissDialog();
          // CustomToast.showSimpleToast(msg: data['msg']);
          if (data["data"] != null) {
            return data["data"];
          } else {
            HandleData.instance.failure(data["data"], context);
          }
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
    );
    return result;
  }

  Future<GetServiceDetails?> getServiceDetails(int id) async {
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.getServiceDetails + "?id=$id",
      returnType: ReturnType.Model,
      showLoader: true,
      refresh: true,
      toJsonFunc: (json) => GetServiceDetails.fromJson(json),
      methodType: MethodType.Get,
      returnDataFun: (data) {
        if (data["key"] == "success") {
          DioUtils.dismissDialog();
          // CustomToast.showSimpleToast(msg: data['msg']);
          if (data["data"] != null) {
            return data["data"];
          } else {
            HandleData.instance.failure(data["data"], context);
          }
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
    );
    return result;
  }

  Future<GetProviderRatesModel?> getProviderRates(int id) async {
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.getProviderRates + "?id=$id",
      returnType: ReturnType.Model,
      showLoader: true,
      refresh: true,
      toJsonFunc: (json) => GetProviderRatesModel.fromJson(json),
      methodType: MethodType.Get,
      returnDataFun: (data) {
        if (data["key"] == "success") {
          DioUtils.dismissDialog();
          // CustomToast.showSimpleToast(msg: data['msg']);
          if (data["data"] != null) {
            return data["data"];
          } else {
            HandleData.instance.failure(data["data"], context);
          }
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
    );
    return result;
  }

  Future<GetProviderImagesModel?> getProviderFiles(int id) async {
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.getProviderFiles + "?id=$id",
      returnType: ReturnType.Model,
      showLoader: true,
      refresh: true,
      toJsonFunc: (json) => GetProviderImagesModel.fromJson(json),
      methodType: MethodType.Get,
      returnDataFun: (data) {
        if (data["key"] == "success") {
          DioUtils.dismissDialog();
          // CustomToast.showSimpleToast(msg: data['msg']);
          if (data["data"] != null) {
            return data["data"];
          } else {
            HandleData.instance.failure(data["data"], context);
          }
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
    );
    return result;
  }

  Future<List<ProviderModel>> getFavoriteProviders() async {
    List<ProviderModel> result =
        await GenericHttp<ProviderModel>(context).callApi(
      returnType: ReturnType.List,
      methodType: MethodType.Get,
      refresh: true,
      returnDataFun: (data) {
        if (data["key"] == "success") {
          return data["data"];
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
      toJsonFunc: (json) => ProviderModel.fromJson(json),
      name: ApiNames.getFavoriteProviders,
    );
    return result;
  }

  Future<List<GetFavouriteServicesModel>> getFavoriteServices() async {
    List<GetFavouriteServicesModel> result =
        await GenericHttp<GetFavouriteServicesModel>(context).callApi(
      returnType: ReturnType.List,
      methodType: MethodType.Get,
      refresh: true,
      returnDataFun: (data) {
        if (data["key"] == "success") {
          return data["data"];
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
      toJsonFunc: (json) => GetFavouriteServicesModel.fromJson(json),
      name: ApiNames.getFavoriteServices,
    );
    return result;
  }

  Future<List<OrderModel>> getMyNewOrders() async {
    List<OrderModel> result = await GenericHttp<OrderModel>(context).callApi(
      returnType: ReturnType.List,
      methodType: MethodType.Get,
      refresh: true,
      returnDataFun: (data) {
        if (data["key"] == "success") {
          return data["data"]["orders"];
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
      toJsonFunc: (json) => OrderModel.fromJson(json),
      name: ApiNames.getMyNewOrders,
    );
    return result;
  }

  Future<List<OrderModel>> getMyFinishOrders() async {
    List<OrderModel> result = await GenericHttp<OrderModel>(context).callApi(
      returnType: ReturnType.List,
      methodType: MethodType.Get,
      refresh: true,
      returnDataFun: (data) {
        if (data["key"] == "success") {
          return data["data"]["orders"];
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
      toJsonFunc: (json) => OrderModel.fromJson(json),
      name: ApiNames.getMyFinishedOrders,
    );
    return result;
  }

  Future<OrderCreatedModel> createOrder(String couponName) async {
    Map<String, dynamic> body = {
      "coupon_num": couponName,
    };
    dynamic data = await GenericHttp<OrderCreatedModel>(context).callApi(
      name: ApiNames.createOrder,
      json: body,
      returnType: ReturnType.Model,
      showLoader: true,
      methodType: MethodType.Post,
      toJsonFunc: (json){

        return OrderCreatedModel.fromJson(json);

      },
      returnDataFun: (data) {
        if (data["key"] == "success") {
          if (data["data"] != null) {
            return HandleData.instance.handlePostData(data, context,fullData: false);
          } else {
            return   HandleData.instance.failure(data['data'], context,);
          }
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
    );
    return data;
  }

  Future<List<GetOrderDetailsModel>> getOrderDetails(int orderId) async {
    List<GetOrderDetailsModel> result =
        await GenericHttp<GetOrderDetailsModel>(context).callApi(
      returnType: ReturnType.List,
      methodType: MethodType.Get,
      refresh: true,
      returnDataFun: (data) {
        if (data["key"] == "success") {
          return data["data"]["orders"];
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
      toJsonFunc: (json) => GetOrderDetailsModel.fromJson(json),
      name: ApiNames.getOrderDetails + "?order_id=$orderId",
    );
    return result;
  }

  Future<List<InvoiceModel>> getMyInvoices(bool refresh) async {
    List<InvoiceModel> result =
        await GenericHttp<InvoiceModel>(context).callApi(
      returnType: ReturnType.List,
      methodType: MethodType.Get,
      refresh: refresh,
      returnDataFun: (data) {
        if (data["key"] == "success") {
          return data["data"]["invoices"];
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
      toJsonFunc: (json) => InvoiceModel.fromJson(json),
      name: ApiNames.getMyInvoices,
    );
    return result;
  }

  Future<InvoiceModel?> getInvoicesDetails(int invoiceId) async {
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.getInvoicesDetails + "?invoice_id=$invoiceId",
      returnType: ReturnType.Type,
      showLoader: false,
      refresh: true,
      methodType: MethodType.Get,
      returnDataFun: (data) {
        if (data["key"] == "success") {
          return data["data"]["invoice"];
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
    );
    if (result != null) {
      return InvoiceModel.fromJson(result);
    }
    return null;
  }

  // Chats
  Future<List<GetRoomModel>> getChatRooms() async {
    String params = "get-rooms";
    return await GenericHttp<GetRoomModel>(context).callApi(
      methodType: MethodType.Get,
      name: params,
      showLoader: false,
      returnType: ReturnType.List,
      returnDataFun: (data) {
        if (data["key"] == "success") {
          return data["data"]["rooms"];
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
      toJsonFunc: (json) {
        if (json != null) {
          if (json['key'] == 'fail') {
            return CustomToast.showSimpleToast(msg: json['msg']);
          } else {
            return GetRoomModel.fromJson(json);
          }
        }
      },
    ) as List<GetRoomModel>;
  }

  //---------------------------> Get Room Messages
  Future<ChatModel> getRoomMesaages({
    // required String paginate,
    // required String page,
    required bool refresh,
    required int id,
  }) async {
    String params = "/$id";
    return await GenericHttp<ChatModel>(context).callApi(
      methodType: MethodType.Get,
      name: ApiNames.getRoomMessages + params,
      returnType: ReturnType.Model,
      showLoader: false,
      returnDataFun: (data) {
        if (data["key"] == "success") {
          return data["data"];
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
      toJsonFunc: (json) {
        if (json != null) {
          return ChatModel.fromJson(json);
        }
      },
    ) as ChatModel;
  }

  //---------------------------> Get Room Messages
  Future<bool> sendMessage({
    required int roomId,
    required String message,
  }) async {
    var data = await GenericHttp<bool>(context).callApi(
      methodType: MethodType.Post,
      name: ApiNames.sendMessage + roomId.toString(),
      returnType: ReturnType.Type,
      showLoader: false,
      json: {"message": message},
      returnDataFun: (data) {
        if (data["key"] == "success") {
          return data;
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
      toJsonFunc: (json) {
        if (json != null) {
          print(json['msg']);
        }
      },
    );
    if (data != null) {
      return true;
    } else {
      return false;
    }
  }

  // rating

  Future<bool> rateProvider(int invoiceId, double value, String comment) async {
    Map<String, dynamic> body = {
      "value": value,
      "invoice_id": invoiceId,
      "comment": comment,
    };
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.rateProvider,
      json: body,
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      showLoader: true,
    );
    if (result != null) {
      if (result["key"] == "success") {
        CustomToast.showSimpleToast(msg: result["msg"]);
        return true;
      } else {
        HandleData.instance.failure(result, context);
        return false;
      }
    }
    return false;
  }

  Future<bool> rateService(int invoiceId, double value, String comment) async {
    Map<String, dynamic> body = {
      "invoice_id": invoiceId,
      "value": value,
      "comment": comment,
    };
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.rateService,
      json: body,
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      // toJsonFunc: (data) => data,
      showLoader: true,
    );
    if (result != null) {
      if (result["key"] == "success") {
        CustomToast.showSimpleToast(msg: result["msg"]);
        return true;
      } else {
        HandleData.instance.failure(result, context);
        return false;
      }
    }
    return false;
  }

  // wallet
  Future<String?> wallet() async {
    String? result = await GenericHttp<String>(context).callApi(
      name: ApiNames.getUserBalance,
      returnType: ReturnType.Type,
      refresh: true,
      showLoader: false,
      returnDataFun: (data) {
        if (data["key"] == "success") {
          return data["data"]["balance"];
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
      methodType: MethodType.Get,
    );
    return result;
  }

  Future<List<ProviderTimingModel>> getProviderTime(bool refresh, int id) async {
    List<ProviderTimingModel> result =
    await GenericHttp<ProviderTimingModel>(context).callApi(
      returnType: ReturnType.List,
      methodType: MethodType.Get,
      refresh: refresh,
      returnDataFun: (data) {
        if (data["key"] == "success") {
          return data["data"];
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
      toJsonFunc: (json) => ProviderTimingModel.fromJson(json),
      name: ApiNames.getProviderTime + "?id=$id",
    );
    return result;
  }
  Future<bool> completePayment(int orderId,String payType) async {
    Map<String, dynamic> body = {
      "order_id": orderId,
      "pay_type": payType,

    };
     await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.completePayment + "?_method=patch",
      json: body,
      returnType: ReturnType.Model,
      methodType: MethodType.Post,
      toJsonFunc: (data) => data,
      showLoader: true,
      returnDataFun: (data){
        if (data["key"] == "success") {
          return true;
        } else {
          return HandleData.instance.failure(data, context);
        }
      },
    );
    return true;
  }

}
