part of 'customer_repository_imports.dart';

class CustomerRepository {
  late BuildContext _context;
  late CustomerHttpMethods _customerHttpMethods;

  CustomerRepository(BuildContext context) {
    _context = context;
    _customerHttpMethods = new CustomerHttpMethods(_context);
  }

  Future<bool> logOut() => _customerHttpMethods.logOut();

  Future<bool> updateUserProfile(ProfileModel model) =>
      _customerHttpMethods.updateUserProfile(model);

  Future<bool> updateUserPassword(
          String newPassword, String oldPassword, String passConfirmation) =>
      _customerHttpMethods.updateUserPassword(
          newPassword, oldPassword, passConfirmation);

  Future<bool> changeLanguage(String lang) =>
      _customerHttpMethods.changeLanguage(lang);

  Future<bool> switchNotify() => _customerHttpMethods.switchNotify();
  Future<NotificationsModel> getNotifications(int pageNumber, int pageCount) =>
      _customerHttpMethods.getNotifications(pageNumber, pageCount);

  Future<bool> deleteUser() => _customerHttpMethods.deleteUser();

  Future<HomeModel?> getHomeData(bool refresh, String search) =>
      _customerHttpMethods.getHomeData(refresh, search);

  Future<List<GetCouponModel>> getCoupons(bool refresh) =>
      _customerHttpMethods.getCoupons(refresh);

  Future<List<SubCategoryModel>> getSubCategories(bool refresh, int id) =>
      _customerHttpMethods.getSubCategories(refresh, id);

  Future<GetProvidersModel?> getProviders(
          bool refresh, int subCategoryId, String search, location, sort) =>
      _customerHttpMethods.getProviders(
          refresh, subCategoryId, search, location, sort);

  Future<GetOffersModel?> getOffers(
          bool refresh, int subCategoryId,int sectionId ,String searchText) =>
      _customerHttpMethods.getOffers(refresh, subCategoryId,sectionId ,searchText);

  Future<bool> addToCart(int serviceId) =>
      _customerHttpMethods.addToCart(serviceId);

  Future<CartModel?> getCart(bool refresh) =>
      _customerHttpMethods.getCart(refresh);

  Future<GetDiscountModel?> checkCoupon(num totalPrice, String coupon) =>
      _customerHttpMethods.checkCoupon(totalPrice, coupon);

  Future<bool> removeFromCart(int serviceId) =>
      _customerHttpMethods.removeFromCart(serviceId);

  Future<List<OfferModel>> getProviderOffers(bool refresh, int id) =>
      _customerHttpMethods.getProviderOffers(refresh, id);
  Future<ProviderModel> getProviderDetails(bool refresh, int id) =>
      _customerHttpMethods.getProviderDetails(refresh, id);
  Future<GetProviderServicesModel?> getProviderServices(
          int id, String search,int sectionId) =>
      _customerHttpMethods.getProviderServices(id, search,sectionId);

  Future<GetServiceDetails?> getServiceDetails(int id) =>
      _customerHttpMethods.getServiceDetails(id);

  Future<GetProviderRatesModel?> getProviderRates(int id) =>
      _customerHttpMethods.getProviderRates(id);

  Future<GetProviderImagesModel?> getProviderFiles(int id) =>
      _customerHttpMethods.getProviderFiles(id);

  Future<List<ProviderModel>> getFavoriteProviders() =>
      _customerHttpMethods.getFavoriteProviders();

  Future<List<GetFavouriteServicesModel>> getFavoriteServices() =>
      _customerHttpMethods.getFavoriteServices();

  Future<List<OrderModel>> getMyNewOrders() =>
      _customerHttpMethods.getMyNewOrders();

  Future<List<OrderModel>> getMyFinishOrders() =>
      _customerHttpMethods.getMyFinishOrders();

  Future<OrderCreatedModel> createOrder(String couponName) =>
      _customerHttpMethods.createOrder(couponName);

  Future<List<GetOrderDetailsModel>> getOrderDetails(int orderId) =>
      _customerHttpMethods.getOrderDetails(orderId);

  Future<List<InvoiceModel>> getMyInvoices(bool refresh) =>
      _customerHttpMethods.getMyInvoices(refresh);

  Future<InvoiceModel?> getInvoicesDetails(int invoiceId) =>
      _customerHttpMethods.getInvoicesDetails(invoiceId);
  Future<bool> deleteNotify() => _customerHttpMethods.deleteNotify();

  Future<List<GetRoomModel>> getChatRooms() =>
      _customerHttpMethods.getChatRooms();

  Future<ChatModel> getRoomMesaages({
    required bool refresh,
    required int id,
  }) =>
      _customerHttpMethods.getRoomMesaages(refresh: refresh, id: id);
  Future<bool> rateProvider(int invoiceId, double value, String comment) =>
      _customerHttpMethods.rateProvider(invoiceId, value, comment);

  Future<bool> rateService(int invoiceId, double value, String comment) =>
      _customerHttpMethods.rateService(invoiceId, value, comment);

  Future<String?> wallet() => _customerHttpMethods.wallet();

  Future<bool> switchUserFav(int id) => _customerHttpMethods.switchUserFav(id);

  Future<bool> switchServiceFav(int id) =>
      _customerHttpMethods.switchServiceFav(id);
  Future<List<ProviderTimingModel>> getProviderTime(bool refresh, int id) =>
      _customerHttpMethods.getProviderTime(refresh, id);

  Future<bool> completePayment(int orderId,String payType)=>_customerHttpMethods.completePayment(orderId, payType);
}
