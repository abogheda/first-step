
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class BuildSubServiceItem extends StatelessWidget {
  final String title;
  final Function() onTap;
  const BuildSubServiceItem({required this.title,required this.onTap}) ;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15),
        margin: EdgeInsets.symmetric(horizontal: 10),
        decoration:
        BoxDecoration(color: MyColors.primary, borderRadius: BorderRadius.circular(5)),
        alignment: Alignment.center,
        child: MyText(
          title: "$title",
          size: 11,
          color: MyColors.white,
        ),
      ),
    );
  }
}
