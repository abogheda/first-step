import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class BuildDetailsItem extends StatelessWidget {
  final String title;
  final String details;

  const BuildDetailsItem({required this.title, required this.details});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            MyText(title: title, color: MyColors.grey, size: 11),
            MyText(title: details, color: MyColors.black, size: 13),
          ],
        ),
      ),
    );
  }
}
