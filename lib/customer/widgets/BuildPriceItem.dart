import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class BuildPriceItem extends StatelessWidget {
  final num afterPrice;
  final num beforePrice;

  const BuildPriceItem({required this.afterPrice, required this.beforePrice});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 130,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            width: 80,
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
              color: MyColors.purple,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(10),
                bottomRight: Radius.circular(10),
                topLeft: Radius.circular(30),
                bottomLeft: Radius.circular(30),
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MyText(title: "الان", color: MyColors.white, size: 10),
                MyText(title: "$afterPrice", color: MyColors.white, size: 10),
                MyText(title: "ريال", color: MyColors.white, size: 10)
              ],
            ),
          ),
          Positioned(
            left: 0,
            child: Container(
              width: 60,
              padding: const EdgeInsets.all(3),
              decoration: BoxDecoration(
                  border: Border.all(color: MyColors.white, width: 4),
                  color: MyColors.primary,
                  shape: BoxShape.circle),
              child: Column(
                children: [
                  MyText(
                    title: "$beforePrice",
                    color: MyColors.white,
                    alien: TextAlign.center,
                    size: 10,
                    decoration: TextDecoration.lineThrough,
                  ),
                  MyText(
                    title: "ريال",
                    color: MyColors.white,
                    alien: TextAlign.center,
                    size: 10,
                    decoration: TextDecoration.lineThrough,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
