import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../res.dart';

class BuildAuthCheckScreen extends StatelessWidget {
  const BuildAuthCheckScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Lottie.asset(
            Res.api_error,
            width: 200,
            height: 200,
            repeat: true,
          ),
          MyText(
            title: tr(context,
                "toUse"), //"قم بتسجيل الدخول حتي يمكنك اضافة طلبات",
            size: 13, color: MyColors.grey,
          ),
          DefaultButton(
            width: 150,
            title: tr(context, "login"),
            onTap: () => AutoRouter.of(context).push(LoginRoute(),),
          ),
        ],
      ),
    );
  }
}
