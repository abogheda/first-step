import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
class BuildTimeItem extends StatelessWidget {
  final String day;
  final String from;
  final String to;

  const BuildTimeItem(
      {required this.day, required this.from, required this.to});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          MyText(title: day, color: MyColors.black, size: 13),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              border: Border.all(color: MyColors.grey),
            ),
            child: MyText(title: from, color: MyColors.grey, size: 10),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              border: Border.all(color: MyColors.grey),
            ),
            child: MyText(title: to, color: MyColors.grey, size: 10),
          ),
        ],
      ),
    );
  }
}
