part of 'ServicesDetailsImports.dart';

class ServicesDetailsData {

  final GenericBloc<GetServiceDetails?> getDetailsCubit = GenericBloc(null);
  final GenericBloc<bool> favCubit = GenericBloc(false);

  void showAddService(
      BuildContext context, ServicesDetailsData servicesDetailsData) {
    showDialog(
      context: context,
      builder: (_) => BuildAddService(),
    );
  }
  fetchServiceDetails(BuildContext context,int id) async{
    var data = await CustomerRepository(context).getServiceDetails(id);
    getDetailsCubit.onUpdateData(data);
  }


  switchFav(BuildContext context , int id) async{
    await CustomerRepository(context).switchServiceFav(id);

  }
  addToCart(BuildContext context,int serviceId,ServicesDetailsData servicesDetailsData) async{
    var result = await CustomerRepository(context).addToCart(serviceId);
    if(result){
      showAddService(context,servicesDetailsData);
    }
  }

}
