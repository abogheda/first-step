part of 'ServicesDetailsWidgetsImports.dart';
class BuildServiceDescription extends StatelessWidget {
final String? desc;

  const BuildServiceDescription({Key? key, this.desc}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
          color: MyColors.white, borderRadius: BorderRadius.circular(5)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(
            title: tr(context, "desc"),
            color: MyColors.black,
            size: 14,
          ),
          MyText(
            title: "$desc",
            color: MyColors.grey,
            size: 12,
          ),
        ],
      ),
    );
  }
}
