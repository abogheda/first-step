part of 'ServicesDetailsWidgetsImports.dart';

class BuildServiceSwiper extends StatefulWidget {
  final List<ServiceFileModel>? servicesFileModel;
  final ServicesDetailsData servicesData;
  final bool isFav;
  final int id;
  final int? index;
  const BuildServiceSwiper(
      {Key? key,
      this.servicesFileModel,
      this.index,
      required this.servicesData,
      required this.isFav,
      required this.id})
      : super(key: key);

  @override
  State<BuildServiceSwiper> createState() => _BuildServiceSwiperState();
}

class _BuildServiceSwiperState extends State<BuildServiceSwiper> {
  @override
  void initState() {
    widget.servicesData.favCubit.onUpdateData(
        widget.servicesData.getDetailsCubit.state.data?.service?.isFavorite ??
            false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 180,
      margin: const EdgeInsets.only(bottom: 10),
      child: Swiper(
        itemCount: widget.servicesFileModel?.length,
        pagination: SwiperPagination(),
        autoplay: true,
        itemBuilder: (context, index) => CachedImage(
          alignment: Alignment.topLeft,
          url: "${widget.servicesFileModel?[index].file}",
          fit: BoxFit.fill,
          borderRadius: BorderRadius.circular(10),
          child: Container(
            margin: const EdgeInsets.all(10),
            padding: const EdgeInsets.all(8),
            decoration: BoxDecoration(
              color: MyColors.white,
              shape: BoxShape.circle,
            ),
            child: BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
              bloc: widget.servicesData.favCubit,
              builder: (context, state) {
                return state.data
                    ? InkWell(
                        onTap: () {
                          widget.servicesData
                              .switchFav(context, widget.id)
                              .then((value) {
                            ProvidersData().favProviderCubit.state.data.removeAt(index);
                            ProvidersData().favProviderCubit.onUpdateData(ProvidersData().favProviderCubit.state.data);
                            ProvidersServicesData()
                                .servicesCubit
                                .state
                                .data
                                ?.services![index]
                                .isFavorite = state.data;
                            ProvidersServicesData().servicesCubit.onUpdateData(
                                ProvidersServicesData()
                                    .servicesCubit
                                    .state
                                    .data);
                            widget.servicesData.favCubit.onUpdateData(false);
                          });
                        },
                        child: Icon(
                          MdiIcons.thumbUpOutline,
                          color: Colors.red,
                          size: 18,
                        ),
                      )
                    : InkWell(
                        onTap: () {
                          widget.servicesData
                              .switchFav(context, widget.id)
                              .then(
                            (value) {
                              ProvidersData().favProviderCubit.state.data.removeAt(index);
                              ProvidersData().favProviderCubit.onUpdateData(ProvidersData().favProviderCubit.state.data);
                              ProvidersServicesData()
                                  .servicesCubit
                                  .state
                                  .data
                                  ?.services![index]
                                  .isFavorite = state.data;
                              ProvidersServicesData()
                                  .servicesCubit
                                  .onUpdateData(ProvidersServicesData()
                                      .servicesCubit
                                      .state
                                      .data);
                              widget.servicesData.favCubit.onUpdateData(true);
                            },
                          );
                        },
                        child: Icon(
                          MdiIcons.thumbUpOutline,
                          color: MyColors.grey,
                          size: 18,
                        ),
                      );
              },
            ),
          ),
        ),
      ),
    );
  }
}
