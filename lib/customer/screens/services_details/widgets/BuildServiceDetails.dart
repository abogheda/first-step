part of 'ServicesDetailsWidgetsImports.dart';

class BuildServiceDetails extends StatelessWidget {
  final num price;
  final num rate;
  final String providerName;

  const BuildServiceDetails({Key? key,required this.price,required this.providerName,required this.rate}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: MyColors.white, borderRadius: BorderRadius.circular(5)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyText(
                title: tr(context, "serviceName"),
                color: MyColors.black,
                size: 14,
              ),
              RatingBar.builder(
                initialRating: rate.toDouble(),
                minRating: 0,
                direction: Axis.horizontal,
                allowHalfRating: false,
                updateOnDrag: false,
                ignoreGestures: true,
                itemCount: 5,
                itemSize: 17,
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: (double value) {},
              ),
            ],
          ),
          Row(
            children: [
              BuildDetailsItem(title: tr(context, "orderPrice"), details: "$price"),
              BuildDetailsItem(title: tr(context, "providerName"), details: "$providerName")
            ],
          ),
        ],
      ),
    );
  }
}
