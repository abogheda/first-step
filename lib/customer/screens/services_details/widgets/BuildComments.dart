part of 'ServicesDetailsWidgetsImports.dart';

class BuildComments extends StatelessWidget {
  final List<CommentModel>? commentsModel;

  const BuildComments({Key? key, this.commentsModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: MyColors.white, borderRadius: BorderRadius.circular(5)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(
            title: tr(context, "comments"),
            color: MyColors.black,
            size: 14,
            fontWeight: FontWeight.bold,
          ),
          commentsModel!.isNotEmpty ?
          Column(
            children: List.generate(
              commentsModel?.length ?? 0,
                  (index) => BuildCommentItem(
                commentModel: commentsModel![index],
              ),
            ),
          ):
              Center(child: MyText(title: tr(context, "noComments"),color: MyColors.black,size: 12,),),
        ],
      ),
    );
  }
}
