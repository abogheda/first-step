part of 'ServicesDetailsWidgetsImports.dart';

class BuildCommentItem extends StatelessWidget {
  final CommentModel? commentModel;
  const BuildCommentItem({Key? key, this.commentModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: [
          CachedImage(
            url:"${commentModel?.userImage}",
            fit: BoxFit.fill,
            height: 70,
            width: 70,
            haveRadius: false,
            boxShape: BoxShape.circle,
          ),
          SizedBox(width: 10),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    MyText(
                        title: "${commentModel?.userName}", color: MyColors.black, size: 13),
                    RatingBar.builder(
                      initialRating: 3,
                      minRating: 0,
                      direction: Axis.horizontal,
                      allowHalfRating: false,
                      updateOnDrag: false,
                      ignoreGestures: true,
                      itemCount: 5,
                      itemSize: 15,
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (double value) {},
                    ),
                  ],
                ),
                MyText(
                  title: "${commentModel?.comment}",
                  color: MyColors.grey,
                  size: 11,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
