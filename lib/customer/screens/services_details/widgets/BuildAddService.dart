part of 'ServicesDetailsWidgetsImports.dart';

class BuildAddService extends StatelessWidget {
  const BuildAddService({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Lottie.asset(
            Res.correct,
            width: 100,
            height: 100,
            repeat: true,
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: MyText(
              title: tr(context, "serviceAddedSuccessfully"),
              color: MyColors.black,
              size: 17,
            ),
          ),
          MyText(
            title:
                tr(context, "serviceAdded"),
            color: MyColors.grey,
            size: 12,
            alien: TextAlign.center,
          ),
          Row(
            children: [
              Expanded(
                child: DefaultButton(
                  margin: const EdgeInsets.symmetric(horizontal: 2,vertical: 10),
                  title: tr(context, "addOtherServices"),
                  onTap: () => AutoRouter.of(context).pop(),
                ),
              ),
              Expanded(
                child: DefaultButton(
                  title: tr(context, "completeOrder"),
                  onTap: () => AutoRouter.of(context).push(HomeRoute(index: 3)),
                  color: MyColors.secondary,
                  margin:
                      const EdgeInsets.symmetric(horizontal: 2, vertical: 10),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
