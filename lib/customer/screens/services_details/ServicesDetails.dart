part of 'ServicesDetailsImports.dart';

class ServicesDetails extends StatefulWidget {
  final int id;
  final int? index;

  const ServicesDetails({Key? key, required this.id, this.index}) : super(key: key);

  @override
  _ServicesDetailsState createState() => _ServicesDetailsState();
}

class _ServicesDetailsState extends State<ServicesDetails> {
  ServicesDetailsData servicesDetailsData = ServicesDetailsData();


  @override
  void initState() {
    servicesDetailsData.fetchServiceDetails(context, widget.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(title: tr(context,"details"),
      ),
      backgroundColor: MyColors.greyWhite,
      bottomNavigationBar: DefaultButton(
        title: tr(context, "addService"),
        onTap: () =>
            servicesDetailsData.addToCart(context,widget.id,servicesDetailsData),
        margin: const EdgeInsets.all(20),
      ),
      body: BlocBuilder<GenericBloc<GetServiceDetails?>, GenericState<GetServiceDetails?>>(
        bloc: servicesDetailsData.getDetailsCubit,
        builder: (context, state) {
          if(state is GenericUpdateState) {
            return ListView(
              padding: const EdgeInsets.all(20),
              children: [
                BuildServiceSwiper(servicesFileModel: state.data?.serviceFiles, servicesData: servicesDetailsData, isFav: state.data?.service?.isFavorite ?? false, id:widget.id,
                index: widget.index,),
                BuildServiceDetails(price: state.data?.service?.price ?? 0,
                    providerName: state.data?.service?.providerName ?? "",
                    rate: state.data?.service?.avgRate ?? 0),
                BuildServiceDescription(
                  desc: state.data?.service?.description ?? "",),
                BuildComments(commentsModel: state.data?.comments,),
              ],
            );
          }
          return LoadingDialog.showLoadingView();
        },
      ),
    );
  }
}
