
import 'package:base_flutter/customer/models/chat_model.dart';
import 'package:base_flutter/customer/resources/customer_repository_imports.dart';
import 'package:base_flutter/general/blocks/user_cubit/user_cubit.dart';

import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:dio_helper/dio_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:socket_io_client/socket_io_client.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../../general/models/UserModel.dart';
import 'widgets/CustomerChatWidgetImports.dart';

part 'CustomerChatRoom.dart';

part 'CustomerChatRoomData.dart';
