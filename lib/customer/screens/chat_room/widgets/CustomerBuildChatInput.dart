part of 'CustomerChatWidgetImports.dart';

class CustomerBuildChatInput extends StatelessWidget {
  final CustomerChatRoomData chatsData;


  const CustomerBuildChatInput({
    required this.chatsData,

  });

  @override
  Widget build(BuildContext context) {
    // var user = context.read<UserCubit>().state.model;
    return Container(
      color: MyColors.primary,
      padding: EdgeInsets.only(bottom: 10),
      width: MediaQuery.of(context).size.width,
      child: Row(
        children: [
          Expanded(
            flex: 6,
            child: GenericTextField(
              contentPadding: EdgeInsets.symmetric(horizontal: 10),
              controller: chatsData.msgInput,
              action: TextInputAction.newline,
              fieldTypes: FieldTypes.chat,
              type: TextInputType.multiline,
              fillColor: MyColors.white,
              enableBorderColor: MyColors.white,
              margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
              hint: tr(context, "WriteMsgHere"),
              validate: (value) => value!.noValidate(),
            ),
          ),
          InkWell(
            onTap: () =>
                chatsData.sendMessage(context),
            child: Padding(
              padding: const EdgeInsetsDirectional.only(end: 16, start: 5),
              child: Icon(
                Icons.send,
                size: 30,
                color: MyColors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}