
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/utils_functions/LoadingDialog.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_validator/tf_validator.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import '../../../models/chat_model.dart';
import '../CustomerChatRoomImports.dart';


part 'CustomerBuildChatMessages.dart';
part 'CustomerBuildRightMsg.dart';
part 'CustomerBuildLeftMsg.dart';
part 'CustomerBuildChatInput.dart';
