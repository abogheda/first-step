part of 'CustomerChatWidgetImports.dart';

class CustomerBuildChatMessages extends StatelessWidget {
  final CustomerChatRoomData chatsData;
  final String receiverName;

  const CustomerBuildChatMessages(
      {required this.chatsData, required this.receiverName});

  @override
  Widget build(BuildContext context) {
    // var user = context.watch<UserCubit>().state.model;
    return Flexible(
      child: BlocBuilder<GenericBloc<List<ChatMessage>>, GenericState<List<ChatMessage>>>(
        bloc: chatsData.messages,
        builder: (context, state) {
          if(state is GenericUpdateState) {
            return ListView.builder(
              reverse: true,
              itemCount: state.data.length,
              itemBuilder: (context, index) {
                if (state.data[index].isSender == 1) {
                  return CustomerBuildRightMsg(model: state.data[index]);
                } else {
                  return CustomerBuildLeftMsg(
                    model: state.data[index],
                    receiverName: receiverName,
                  );
                }
              },);
          }
         return LoadingDialog.showLoadingView();
        },
      ),
    );
  }
}
