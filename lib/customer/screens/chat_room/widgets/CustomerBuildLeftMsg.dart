part of 'CustomerChatWidgetImports.dart';

class CustomerBuildLeftMsg extends StatelessWidget {
  final ChatMessage model;
  final String receiverName;

  const CustomerBuildLeftMsg({required this.model, required this.receiverName});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      margin: EdgeInsets.only(top: 10),
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(
              maxHeight: MediaQuery.of(context).size.height,
              minHeight: 30,
              maxWidth: MediaQuery.of(context).size.width * .7,
              minWidth: MediaQuery.of(context).size.width * .4,
            ),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 5, vertical: 4),
              decoration: BoxDecoration(
                color: MyColors.blackOpacity,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(8),
                  bottomRight: Radius.circular(8),
                  bottomLeft: Radius.circular(8),
                ),
              ),
              child: MyText(
                title: model.body ??'',
                size: 12,
                color: MyColors.white,
              ),
            ),
          ),
          MyText(
            title: model.createdDt ??"",
            size: 10,
            color: MyColors.black.withOpacity(.6),
          ),
        ],
      ),
    );
  }
}

