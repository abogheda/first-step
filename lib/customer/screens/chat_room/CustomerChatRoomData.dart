part of 'CustomerChatRoomImports.dart';

class CustomerChatRoomData {
  final GlobalKey btnKey = new GlobalKey();
  final TextEditingController msgInput = new TextEditingController();
  GenericBloc<List<ChatMessage>> messages = GenericBloc([]);
  GenericBloc<ChatModel?> chatModel = GenericBloc(null);

  List<ChatMessage> chatList = [];

  late UserModel userModel;
  int pageSize = 10;
  Socket? socket;


  final PagingController<int, ChatMessage> pagingController =
      PagingController(firstPageKey: 1);

  void initializeSocket(BuildContext context, int roomID) {
    print("User : ${context.read<UserCubit>().state.model.id}");
    socket = io("https://firststep1.net:4665", <String, dynamic>{
      "transports": ["websocket"],
      "autoConnect": false,
    });
    socket!.connect(); //connect the Socket.IO Client to the Server
    // --> listening for connection
    socket!.on('connect', (data) {
      print("connect : $data");
      socket!.emit("enterChat", {
        "user_id": "${context.read<UserCubit>().state.model.id}",
        "room_id": "$roomID",
        "user_type": "User",
      });
    });
    //listen for incoming messages from the Server.
    socket!.on('sendMessageRes', (data) {
      print("sendMessageRes : $data");
      chatList.insert(
        0,
        ChatMessage(
          id: data['id'],
          isSender: data['is_sender'],
          body: data['body'],
          type: data['type'],
          duration: data['duration'],
          name: data['name'],
          createdDt: data['created_dt'],
        ),
      );
      messages.onUpdateData(chatList);
    });
    //listens when the client is disconnected from the Server
    socket!.on('disconnect', (data) {
      print('disconnect');
    });
  }

  fetchData(BuildContext context,{
    required int id,
    bool refresh = true,
    bool showLoading = true,
  }) async {
    if (showLoading) DioUtils.showLoadingDialog();
    final data = await CustomerRepository(context)
        .getRoomMesaages(id: id, refresh: refresh);
    chatModel.onUpdateData(data);
    chatList = data.messages?.chatMessage ??[];
    messages.onUpdateData(data.messages?.chatMessage ??[]);
    if (showLoading) DioUtils.dismissDialog();

  }

  sendMessage(BuildContext context) async {
    timeago.setLocaleMessages('ar', timeago.ArMessages());
    timeago.setLocaleMessages('en', timeago.EnMessages());

        if (msgInput.text.isNotEmpty) {
          socket!.emit("sendMessage", {
            "room_id": "${chatModel.state.data!.room?.id}",
            "sender_id": "${context.read<UserCubit>().state.model.id}",
            "sender_name": "${context.read<UserCubit>().state.model.name}",
            "receiver_id": "${chatModel.state.data!.members![0].id}",
            "receiver_name": "${chatModel.state.data!.members![0].name}",
            "sender_type": "User",
            "receiver_type": "User",
            "type": "text",
            "lang": DioUtils.lang,
            "body": msgInput.text,
          });
          chatList.insert(
            0,
            ChatMessage(
              id: 00,
              isSender: 1,
              body: msgInput.text,
              createdDt:
              "${timeago.format(DateTime.now(), locale: DioUtils.lang == "ar" ? "ar" : "en")}",
              type: "text",
              duration: 0,
              name: "name",
            ),
          );
          messages.onUpdateData(chatList);

          msgInput.clear();
        }
      }
  }

