part of 'CustomerChatRoomImports.dart';

class CustomerChatRoom extends StatefulWidget {

  final int roomId;
  final String receiverName;


  const CustomerChatRoom({
    required this.roomId,
    this.receiverName = "",

  });

  @override
  _CustomerChatRoomState createState() => _CustomerChatRoomState();
}

class _CustomerChatRoomState extends State<CustomerChatRoom> {
  final CustomerChatRoomData chatsData = new CustomerChatRoomData();


  @override
  void initState() {
    super.initState();
    chatsData.fetchData(context, id: widget.roomId);
    chatsData.initializeSocket(context, widget.roomId);
  }

  @override
  void dispose() {
    chatsData.msgInput.dispose();
    chatsData.socket!.dispose();
    super.dispose();
  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
          title: widget.receiverName,
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Column(
          children: [
            CustomerBuildChatMessages(
              receiverName: widget.receiverName,
              chatsData: chatsData,
            ),
            CustomerBuildChatInput(
              chatsData: chatsData,
            ),
          ],
        ),
      ),
    );
  }
}
