import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/reservation_details/ReservationDetailsImports.dart';
import 'package:base_flutter/customer/widgets/BuildDetailsItem.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'package:tf_validator/validator/Validator.dart';

import '../../../models/get_order_details_model.dart';
part 'BuildBillCard.dart';
part 'BuildReservationDetailsItem.dart';
part 'BuildRateService.dart';
part 'BuildRateProvider.dart';