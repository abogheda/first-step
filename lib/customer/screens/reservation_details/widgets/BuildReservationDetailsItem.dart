part of 'ReservationDetailsWidgetsImports.dart';

class BuildReservationDetailsItem extends StatelessWidget {
  final ReservationDetailsData reservationDetailsData;
  final GetOrderDetailsModel? model;
  const BuildReservationDetailsItem(
      {required this.reservationDetailsData, this.model});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(
          color: MyColors.white, borderRadius: BorderRadius.circular(5)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(
              title: "${model?.providerName}", color: MyColors.black, size: 14),
          Divider(color: MyColors.grey),
          Row(
            children: [
              BuildDetailsItem(
                  title: tr(context, "serviceNAme"),
                  details: "${model?.serviceName}"),
              BuildDetailsItem(
                  title: tr(context, "price"), details: "${model?.totalPrice}"),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              model?.status != 4
                  ? const SizedBox()
                  : GestureDetector(
                      onTap: () => reservationDetailsData.showRateService(
                          context, reservationDetailsData, model?.invoiceId ?? 0),
                      child: MyText(
                        title: tr(context, "rateOrder"),
                        color: MyColors.primary,
                        size: 11,
                        decoration: TextDecoration.underline,
                      ),
                    ),
              model?.status != 4
                  ? const SizedBox()
                  : GestureDetector(
                      onTap: () => reservationDetailsData.showRateProvider(
                          context, reservationDetailsData, model?.invoiceId ?? 0),
                      child: MyText(
                        title: tr(context, "rateProvider"),
                        color: MyColors.primary,
                        size: 11,
                        decoration: TextDecoration.underline,
                      ),
                    ),
              // GestureDetector(
              //   onTap: () => AutoRouter.of(context).push(
              //     CustomerChatRoomRoute(
              //         roomId: model?.roomId ?? 0,
              //         receiverName: model?.providerName ?? ''),
              //   ),
              //   child: MyText(
              //     title: tr(context, "chattingNow"),
              //     color: MyColors.primary,
              //     size: 11,
              //     decoration: TextDecoration.underline,
              //   ),
              // ),
            ],
          )
        ],
      ),
    );
  }
}
