part of 'ReservationDetailsWidgetsImports.dart';

class BuildRateService extends StatelessWidget {
  final ReservationDetailsData reservationDetailsData;
  final int? id;

  const BuildRateService({required this.reservationDetailsData,this.id});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyText(
                  title: " تقييم الخدمة ", color: MyColors.secondary, size: 17),
              IconButton(
                onPressed: () => AutoRouter.of(context).pop(),
                icon: Icon(MdiIcons.closeCircle, size: 27),
              )
            ],
          ),
          BlocBuilder<GenericBloc<double>, GenericState<double>>(
            bloc: reservationDetailsData.rateServiceCubit,
            builder: (_, state) {
              return RatingBar.builder(
                initialRating: state.data,
                minRating: 0,
                direction: Axis.horizontal,
                allowHalfRating: false,
                updateOnDrag: false,
                itemCount: 5,
                itemSize: 37,
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: (double value) =>
                    reservationDetailsData.rateServiceCubit.onUpdateData(value),
              );
            },
          ),
          GenericTextField(
            hint: "رأيك يهمنا",
            max: 3,
            controller: reservationDetailsData.serviceComment,
            fieldTypes: FieldTypes.rich,
            type: TextInputType.multiline,
            action: TextInputAction.newline,
            validate: (v) => v!.validateEmpty(context),
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          ),
          DefaultButton(
            title: "تقييم",
            onTap: () => reservationDetailsData.rateService(context, id ??0),
            margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 5),
          ),
        ],
      ),
    );
  }
}
