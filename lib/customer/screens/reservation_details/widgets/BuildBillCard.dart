part of 'ReservationDetailsWidgetsImports.dart';

class BuildBillCard extends StatelessWidget {
  final GetOrderDetailsModel? model;
  final int invoiceId;
  const BuildBillCard({Key? key, this.model,required this.invoiceId}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()=>AutoRouter.of(context).push(BillDetailsRoute(id: invoiceId)),
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
            color: MyColors.white, borderRadius: BorderRadius.circular(5)),
        child: Container(
          margin: const EdgeInsets.symmetric(vertical: 5),
          decoration: BoxDecoration(
              color: MyColors.white, borderRadius: BorderRadius.circular(5)),
          child: ListTile(
            leading: Container(
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: MyColors.primary.withOpacity(.3),
                borderRadius: BorderRadius.circular(5),
              ),
              child: Icon(MdiIcons.walletOutline),
            ),
            title: MyText(
              title: "الفاتورة",
              color: MyColors.black,
              size: 12,
            ),
            trailing: Icon(MdiIcons.chevronLeft),
          ),
        ),
      ),
    );
  }
}
