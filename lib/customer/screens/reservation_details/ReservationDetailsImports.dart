import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/reservation_details/widgets/ReservationDetailsWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/utils_functions/LoadingDialog.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:dio_helper/dio_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../models/get_order_details_model.dart';
import '../../resources/customer_repository_imports.dart';
part 'ReservationDetails.dart';
part 'ReservationDetailsData.dart';