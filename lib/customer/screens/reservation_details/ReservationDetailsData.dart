part of 'ReservationDetailsImports.dart';

class ReservationDetailsData {
  final TextEditingController serviceComment = TextEditingController();
  final TextEditingController providerComment = TextEditingController();
  final GenericBloc<double> rateServiceCubit = GenericBloc(0.0);
  final GenericBloc<double> rateProviderCubit = GenericBloc(0.0);
  final GenericBloc<List<GetOrderDetailsModel>> detailsCubit = GenericBloc([]);

  void showRateProvider(BuildContext context,
      ReservationDetailsData reservationDetailsData, int id) {
    showDialog(
      context: context,
      builder: (context) {
        return BuildRateProvider(
          reservationDetailsData: reservationDetailsData,
          id: id,
        );
      },
    );
  }

  getOrderDetails(BuildContext context, int orderId) async {
    var data = await CustomerRepository(context).getOrderDetails(orderId);
    detailsCubit.onUpdateData(data);
  }

  void showRateService(BuildContext context,
      ReservationDetailsData reservationDetailsData, int id) {
    showDialog(
      context: context,
      builder: (_) => BuildRateService(
        reservationDetailsData: reservationDetailsData,
        id: id,
      ),
    );
  }

  rateProvider(BuildContext context, int id) async {
    if (providerComment.text.isEmpty) {
      CustomToast.showSimpleToast(msg: tr(context, "setComment"));
      return;
    }
    if (rateProviderCubit.state.data == 0) {
      CustomToast.showSimpleToast(msg: tr(context, "setRate"));
      return;
    }
    var data = await CustomerRepository(context)
        .rateProvider(id, rateProviderCubit.state.data, providerComment.text);
    if (data) {
      providerComment.clear();
      rateProviderCubit.onUpdateData(0);
      AutoRouter.of(context).pop();
    }
  }

  rateService(BuildContext context, int id) async {
    if (serviceComment.text.isEmpty) {
      CustomToast.showSimpleToast(msg: tr(context, "setComment"));
      return;
    }
    if (rateServiceCubit.state.data == 0) {
      CustomToast.showSimpleToast(msg: tr(context, "setRate"));
      return;
    }
    var data = await CustomerRepository(context)
        .rateService(id, rateServiceCubit.state.data, serviceComment.text);
    if (data) {
      serviceComment.clear();
      rateServiceCubit.onUpdateData(0);
      AutoRouter.of(context).pop();
    }
  }
}
