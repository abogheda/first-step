part of 'ReservationDetailsImports.dart';

class ReservationDetails extends StatefulWidget {
  final int invoiceId;
  final int orderId;

  const ReservationDetails({Key? key, required this.invoiceId,required this.orderId}) : super(key: key);

  @override
  _ReservationDetailsState createState() => _ReservationDetailsState();
}

class _ReservationDetailsState extends State<ReservationDetails> {
  final ReservationDetailsData reservationDetailsData =
  ReservationDetailsData();


  @override
  void initState() {
    reservationDetailsData.getOrderDetails(context, widget.orderId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.greyWhite,
      appBar: DefaultAppBar(title: tr(context,"orderDetails")),
      body: BlocBuilder<GenericBloc<List<GetOrderDetailsModel>>, GenericState<List<GetOrderDetailsModel>>>(
        bloc: reservationDetailsData.detailsCubit,
        builder: (context, state) {
          if(state is GenericUpdateState) {
            return ListView.builder(
              padding: const EdgeInsets.all(20),
              itemCount: state.data.length,
              itemBuilder: (_,index){
                return Column(
                  children: [
                    BuildReservationDetailsItem(
                        model: state.data[index],
                        reservationDetailsData: reservationDetailsData),
                   BuildBillCard(model: state.data[index], invoiceId: widget.invoiceId,)
                  ],
                );
              },
            );
          }
          return LoadingDialog.showLoadingView();
        },
      ),
    );
  }
}
