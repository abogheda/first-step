part of 'BillsImports.dart';
class BillsData{

  final GenericBloc<List<InvoiceModel>> invoiceCubit = GenericBloc([]);

  fetchInvoice(BuildContext context,{bool refresh = true}) async{
    var data = await CustomerRepository(context).getMyInvoices(refresh);
    invoiceCubit.onUpdateData(data);
  }
}