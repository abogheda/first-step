part of 'BillsWidgetsImports.dart';
class BuildBillsItem extends StatelessWidget {
  final InvoiceModel? model;

  const BuildBillsItem({Key? key, this.model}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()=>AutoRouter.of(context).push(BillDetailsRoute(id: model?.id ?? 0)),
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(vertical: 5),
        decoration: BoxDecoration(
            color: MyColors.white, borderRadius: BorderRadius.circular(5)),
        child: Row(
          children: [
            CachedImage(
              url: "${model?.providerImage}",
              fit: BoxFit.fill,
              height: 70,
              width: 70,
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(title: "${model?.providerName}", color: MyColors.black, size: 13),
                  MyText(title: "${model?.serviceName}", color: MyColors.grey, size: 11),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                MyText(title: "${model?.createdAt}", color: MyColors.grey, size: 11),
                Row(
                  children: [
                    MyText(title: "${tr(context, "orderNum")} :", color: MyColors.grey, size: 11),
                    MyText(title: "${model?.orderNum}", color: MyColors.black, size: 11),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
