part of 'BillsImports.dart';

class Bills extends StatefulWidget {
  const Bills({Key? key}) : super(key: key);

  @override
  _BillsState createState() => _BillsState();
}

class _BillsState extends State<Bills> {
  final BillsData billsData = BillsData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(title: tr(context, "bills")),
      backgroundColor: MyColors.greyWhite,
      body: GenericListView(
        padding: const EdgeInsets.all(15),
        type: ListViewType.api,
        emptyStr: tr(context, "noData"),
        onRefresh: billsData.fetchInvoice,
        params: [context],
        cubit: billsData.invoiceCubit,
        itemBuilder: (_, index, item) => BuildBillsItem(model: item,),
      )

    );
  }
}
