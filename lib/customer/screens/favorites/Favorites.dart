part of 'FavoritesImports.dart';

class Favorites extends StatefulWidget {
  const Favorites({Key? key}) : super(key: key);

  @override
  _FavoritesState createState() => _FavoritesState();
}

class _FavoritesState extends State<Favorites> {
  final FavoritesData favoritesData = FavoritesData();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: MyColors.greyWhite,
        appBar: DefaultAppBar(title: tr(context, "fav")),
        body: Column(
          children: [
            BuildTabsView(favoritesData: favoritesData),
            BuildTabsPages(),
          ],
        ),
      ),
    );
  }
}
