part of 'ServicesImports.dart';

class Services extends StatefulWidget {
  const Services({Key? key}) : super(key: key);

  @override
  _ServicesState createState() => _ServicesState();
}

class _ServicesState extends State<Services> {
  final ServicesData servicesData = ServicesData();


  @override
  void initState() {
    servicesData.fetchFavServices(context);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: BlocBuilder<GenericBloc<List<GetFavouriteServicesModel>>, GenericState<List<GetFavouriteServicesModel>>>(
        bloc: servicesData.favServicesCubit,
        builder: (context, state) {
          if(state is GenericUpdateState) {
            if(state.data.isNotEmpty) {
              return ListView.builder(
                  itemCount: state.data.length, itemBuilder: (_, index) {
                return BuildServiceItem(model: state.data[index],);
              });
            }
            return Center(child: MyText(title: tr(context, "noServices"),size: 12,color: MyColors.black,),);
          }
          return LoadingDialog.showLoadingView();
        },
      ),
    );
  }
}
