part of 'ServicesImports.dart';

class ServicesData {
  final GenericBloc<List<GetFavouriteServicesModel>> favServicesCubit =
      GenericBloc([]);

  fetchFavServices(BuildContext context) async {
    var data = await CustomerRepository(context).getFavoriteServices();
    favServicesCubit.onUpdateData(data);
  }
}
