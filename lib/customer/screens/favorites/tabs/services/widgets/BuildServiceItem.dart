part of 'ServicesWidgetsImports.dart';

class BuildServiceItem extends StatelessWidget {
 final GetFavouriteServicesModel? model;

  const BuildServiceItem({Key? key, this.model}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      closedColor: Colors.transparent,
      openColor: Colors.transparent,
      closedElevation: 0,
      openElevation: 0,
      transitionDuration: Duration(milliseconds: 800),
      transitionType: ContainerTransitionType.fadeThrough,
      openBuilder: (_, action) => ServicesDetails(id: model?.id ??0, index: null,),
      closedBuilder: (_, action) => Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(vertical: 5,horizontal: 15),
        decoration: BoxDecoration(
            color: MyColors.white, borderRadius: BorderRadius.circular(5)),
        child: Row(
          children: [
            CachedImage(
              url: model?.image ?? '',
              fit: BoxFit.fill,
              height: 70,
              width: 70,
            ),
            SizedBox(width: 20),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      MyText(
                          title: "${model?.title}", color: MyColors.black, size: 13),
                      Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          color: MyColors.greyWhite,
                          shape: BoxShape.circle,
                        ),
                        child: Icon(
                          MdiIcons.thumbUpOutline,
                          color: Colors.red,
                          size: 18,
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      MyText(title: "${tr(context,"price")} : ", color: MyColors.black, size: 11),
                      MyText(title: "${model?.price} ${tr(context, "sr")} ", color: MyColors.primary, size: 11),
                    ],
                  ),
                  RatingBar.builder(
                    initialRating: model?.avgRate?.toDouble() ??0,
                    minRating: 0,
                    direction: Axis.horizontal,
                    allowHalfRating: false,
                    updateOnDrag: false,
                    ignoreGestures: true,
                    itemCount: 5,
                    itemSize: 15,
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (double value) {},
                  ),
                ],
              ),
            )
          ],
        ),
      )
    );
  }
}
