import 'package:animations/animations.dart';
import 'package:base_flutter/customer/screens/services_details/ServicesDetailsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../../../../models/get_favourite_services_model.dart';
part 'BuildServiceItem.dart';