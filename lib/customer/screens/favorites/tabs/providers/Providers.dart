part of 'ProvidersImports.dart';

class Providers extends StatefulWidget {
  const Providers({Key? key}) : super(key: key);

  @override
  _ProvidersState createState() => _ProvidersState();
}

class _ProvidersState extends State<Providers> {
  final ProvidersData providersData = ProvidersData();

  @override
  void initState() {
    providersData.fetchFavProvider(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: BlocBuilder<GenericBloc<List<ProviderModel>>,
          GenericState<List<ProviderModel>>>(
        bloc: providersData.favProviderCubit,
        builder: (context, state) {
          if (state is GenericUpdateState) {
            if(state.data.isNotEmpty) {
              return ListView.builder(
                  padding: const EdgeInsets.all(10),
                  itemCount: state.data.length,
                  itemBuilder: (_, index) {
                    return BuildProvidersItem(
                      model: state.data[index], index: index,
                    );
                  });
            }
            return Center(child: MyText(title: tr(context,"noProviders"), color: MyColors.black, size: 12),);
          }
          return LoadingDialog.showLoadingView();
        },
      ),
    );
  }
}
