import 'package:animations/animations.dart';
import 'package:base_flutter/customer/screens/providers_details/ProvidersDetailsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../models/provider_model.dart';
part 'BuildProvidersItem.dart';