part of 'ProvidersWidgetsImports.dart';

class BuildProvidersItem extends StatelessWidget {
final  ProviderModel? model;
final int index;

  const BuildProvidersItem({Key? key, this.model,required this.index}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      closedColor: Colors.transparent,
      openColor: Colors.transparent,
      closedElevation: 0,
      openElevation: 0,
      transitionDuration: Duration(milliseconds: 800),
      transitionType: ContainerTransitionType.fadeThrough,
      openBuilder: (_, action) => ProvidersDetails(id: model!.id,providerModel: model,index: index,),
      closedBuilder: (_, action) => Container(
        padding: const EdgeInsets.all(15),
        margin: const EdgeInsets.symmetric(vertical: 5,horizontal: 10),
        decoration: BoxDecoration(
            color: MyColors.white, borderRadius: BorderRadius.circular(5)),
        child: Row(
          children: [
            CachedImage(
              url:"${model?.image}",
              fit: BoxFit.fill,
              height: 70,
              width: 70,
            ),
            SizedBox(width: 20),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      MyText(
                          title: "${model?.name}", color: MyColors.black, size: 13),
                      Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          color: MyColors.greyWhite,
                          shape: BoxShape.circle,
                        ),
                        child: Icon(
                          MdiIcons.thumbUpOutline,
                          color: Colors.red,
                          size: 18,
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Icon(MdiIcons.mapMarker, color: MyColors.grey, size: 18),
                      Container(
                        width: 150,
                        child: MyText(
                          overflow: TextOverflow.ellipsis,
                            title: "${model?.mapDesc}",
                            color: MyColors.grey,
                            size: 11),
                      ),
                    ],
                  ),
                  RatingBar.builder(
                    initialRating: double.parse(model?.avgRate ?? ""),
                    minRating: 0,
                    direction: Axis.horizontal,
                    allowHalfRating: false,
                    updateOnDrag: false,
                    ignoreGestures: true,
                    itemCount: 5,
                    itemSize: 15,
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (double value) {},
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
