part of 'ProvidersImports.dart';

class ProvidersData {
  static final ProvidersData _providersData = ProvidersData._internal();
  factory ProvidersData() {
    return _providersData;
  }

  ProvidersData._internal();
  final GenericBloc<List<ProviderModel>> favProviderCubit =
      GenericBloc([]);

  fetchFavProvider(BuildContext context) async {
    var data = await CustomerRepository(context).getFavoriteProviders();
    favProviderCubit.onUpdateData(data);
  }
}
