import 'package:base_flutter/customer/resources/customer_repository_imports.dart';
import 'package:base_flutter/customer/screens/favorites/tabs/providers/widgets/ProvidersWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/utils_functions/LoadingDialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../../../models/provider_model.dart';
part 'Providers.dart';
part 'ProvidersData.dart';