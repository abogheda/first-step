import 'package:base_flutter/customer/screens/favorites/FavoritesImports.dart';
import 'package:base_flutter/customer/screens/favorites/tabs/providers/ProvidersImports.dart';
import 'package:base_flutter/customer/screens/favorites/tabs/services/ServicesImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

part 'BuildTabsItem.dart';
part 'BuildTabsPages.dart';
part 'BuildTabsView.dart';