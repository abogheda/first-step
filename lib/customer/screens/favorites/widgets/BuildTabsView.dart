part of 'FavoritesWidgetsImports.dart';

class BuildTabsView extends StatelessWidget {
  final FavoritesData favoritesData;

  const BuildTabsView({required this.favoritesData});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
      bloc: favoritesData.tabsCubit,
      builder: (_, state) {
        return Container(
          margin: const EdgeInsets.symmetric(vertical: 15),
          child: TabBar(
            indicatorColor: Colors.transparent,
            onTap: (value) => favoritesData.tabsCubit.onUpdateData(value),
            tabs: [
              BuildTabsItem(title: tr(context, "services"), index: 0, current: state.data),
              BuildTabsItem(
                  title: tr(context,"providers"), index: 1, current: state.data)
            ],
          ),
        );
      },
    );
  }
}
