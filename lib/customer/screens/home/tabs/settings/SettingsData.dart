part of 'SettingsImports.dart';
class SettingsData{
  final GenericBloc<bool> changeSwitchBloc =GenericBloc(true) ;


  void switchNotify(BuildContext context, bool value) async {
    var user = context.read<UserCubit>().state.model;
    bool result = await CustomerRepository(context).switchNotify();
    changeSwitchBloc.onUpdateData(!value);//true
    user.isNotify= result;
    context.read<UserCubit>().onUpdateUserData(user);
  }

}