part of 'SettingsImports.dart';

class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  final SettingsData settingsData = SettingsData();
  @override
  Widget build(BuildContext context) {
    var visitor = GlobalState.instance.get("visitor");
    return Scaffold(
      backgroundColor: MyColors.greyWhite,
      appBar: BuildCustomAppBar(title: tr(context, "settings")),
      body: ListView(
        padding: const EdgeInsets.all(20),
        children: [
          BuildProfileInfo(settingsData: settingsData),
          // BuildSettingItem(title: "طلباتي", iconData: MdiIcons.noteTextOutline, page: Orders()),
          BuildSettingItem(
              title: tr(context, "myReservations"),
              iconData: MdiIcons.noteTextOutline,
              page: visitor
                  ? BuildAuthCheckScreen()
                  : Reservations()),
          BuildSettingItem(
              title: tr(context, "wallet"),
              iconData: MdiIcons.walletOutline,
              page:visitor
                  ? BuildAuthCheckScreen(): Wallet()),
          BuildSettingItem(
              title: tr(context, "fav"),
              iconData: MdiIcons.heart,
              page:visitor
                  ? BuildAuthCheckScreen() :Favorites()),
          BuildSettingItem(
              title: tr(context, "myCoupon"),
              iconData: MdiIcons.ticketPercentOutline,
              page:visitor
                  ? BuildAuthCheckScreen(): Coupons()),
          BuildSettingItem(
              title: tr(context, "bills"),
              iconData: MdiIcons.walletPlusOutline,
              page:visitor
                  ? BuildAuthCheckScreen(): Bills()),
          BuildSettingItem(
              title: tr(context, "contactUs"),
              iconData: MdiIcons.messageQuestionOutline,
              page: ContactUs()),
          BuildSettingItem(
              title: tr(context, "lang"),
              iconData: MdiIcons.web,
              page: Languages()),
          BuildSettingItem(
              title: tr(context, "terms"),
              iconData: MdiIcons.bookOpenOutline,
              page: Terms()),
          BuildSettingItem(
              title: tr(context, "aboutApp"),
              iconData: MdiIcons.bookOpen,
              page: About()),
          BuildSwitchNotify(
            settingsData: settingsData,
          ),
        ],
      ),
    );
  }
}
