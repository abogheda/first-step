part of 'SettingWidgetImports.dart';

class BuildSwitchNotify extends StatelessWidget {
  final SettingsData settingsData;
  const BuildSwitchNotify({Key? key,required this.settingsData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(
          color: MyColors.white, borderRadius: BorderRadius.circular(5)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
      Row(
        children: [
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 15),
            padding: const EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: MyColors.primary.withOpacity(.3),
              borderRadius: BorderRadius.circular(5),),
            child: Icon(MdiIcons.bellOutline),
          ),
          MyText(
            title: tr(context, "notifications"),
            size: 13,
            alien: TextAlign.center, color: MyColors.black,
          ),
        ],
      ),

          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
            bloc: settingsData.changeSwitchBloc,
            builder: (context, state) {
              return Switch(
                activeColor: MyColors.primary,
                  value: state.data,
                  onChanged: (value) {
                    settingsData.switchNotify(context, state.data);
                  });
            },
          ),
        ],
      ),
    );
  }
}
