part of 'SettingWidgetImports.dart';

class BuildProfileInfo extends StatelessWidget {
  final SettingsData settingsData;

  const BuildProfileInfo({required this.settingsData});

  @override
  Widget build(BuildContext context) {
    var user = context.watch<UserCubit>().state.model;
    var visitor = GlobalState.instance.get("visitor");
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 5),
      margin: const EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
          color: MyColors.white, borderRadius: BorderRadius.circular(5)),
      child:visitor ==false? ListTile(
        horizontalTitleGap: 5,
        leading: CachedImage(
          url:user.image,
          fit: BoxFit.fill,
          height: 70,
          width: 70,
          boxShape: BoxShape.circle,
          haveRadius: false,
        ),
        title: MyText(
          title: user.name,
          size: 13,
          color: MyColors.black,
        ),
        subtitle:  InkWell(
    onTap: ()=> AutoRouter.of(context).push(EditProfileRoute()),
          child: MyText(
                title: tr(context, "editProfile"),
                size: 12,
                color: MyColors.secondary,
              ),
        ),
        trailing: IconButton(
          onPressed: ()=> CustomerRepository(context).logOut(),
          icon: Icon(
            MdiIcons.logout,
            color: MyColors.secondary,
            size: 30,
          ),
        ),
      )
    : InkWell(
        onTap: ()=> AutoRouter.of(context).push(LoginRoute()),
      child: Center(
        child: MyText(title: tr(context, "login"),
              color: MyColors.grey, size: 12),
      ),
    ));
  }
}
