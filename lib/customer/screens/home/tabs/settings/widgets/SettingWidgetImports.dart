import 'package:animations/animations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/home/tabs/settings/SettingsImports.dart';
import 'package:base_flutter/general/blocks/user_cubit/user_cubit.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:dio_helper/utils/GlobalState.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../../../../resources/customer_repository_imports.dart';
part 'BuildSettingItem.dart';
part 'BuildProfileInfo.dart';
part 'BuildSwitchNotify.dart';