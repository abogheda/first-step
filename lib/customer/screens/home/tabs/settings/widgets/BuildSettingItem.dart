part of 'SettingWidgetImports.dart';

class BuildSettingItem extends StatelessWidget {
  final String title;
  final IconData iconData;
  final Widget page;

  const BuildSettingItem(
      {required this.title, required this.iconData, required this.page});

  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      closedElevation: 0,
      openElevation: 0,
      closedColor: Colors.transparent,
      openColor: Colors.transparent,
      transitionDuration: Duration(milliseconds: 800),
      transitionType: ContainerTransitionType.fadeThrough,
      closedBuilder: (context, action) {
        return Container(
          margin: const EdgeInsets.symmetric(vertical: 5),
          decoration: BoxDecoration(
              color: MyColors.white, borderRadius: BorderRadius.circular(5)),
          child: ListTile(
            leading: Container(
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: MyColors.primary.withOpacity(.3),
                borderRadius: BorderRadius.circular(5),
              ),
              child: Icon(iconData),
            ),
            title: MyText(
              title: title,
              color: MyColors.black,
              size: 12,
            ),
            trailing: Icon(Icons.adaptive.arrow_forward,color: MyColors.grey ,size: 20,),
          ),
        );
      },
      openBuilder: (context, action) {
        return page;
      },
    );
  }
}
