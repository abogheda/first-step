import 'package:base_flutter/customer/resources/customer_repository_imports.dart';
import 'package:base_flutter/customer/screens/bills/BillsImports.dart';
import 'package:base_flutter/customer/screens/coupons/CouponsImports.dart';
import 'package:base_flutter/customer/screens/favorites/FavoritesImports.dart';
import 'package:base_flutter/customer/screens/home/tabs/settings/widgets/SettingWidgetImports.dart';
import 'package:base_flutter/customer/screens/home/widgets/HomeWidgetsImports.dart';
import 'package:base_flutter/customer/screens/languages/LanguagesImports.dart';
import 'package:base_flutter/customer/screens/reservations/ReservationsImports.dart';
import 'package:base_flutter/customer/screens/wallet/WalletImports.dart';
import 'package:base_flutter/customer/widgets/BuildAuthCheckScreen.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/screens/about/AboutImports.dart';
import 'package:base_flutter/general/screens/contact_us/ContactUsImports.dart';
import 'package:base_flutter/general/screens/terms/TermsImports.dart';
import 'package:dio_helper/dio_helper.dart';
import 'package:dio_helper/utils/GlobalState.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../../../../general/blocks/user_cubit/user_cubit.dart';

part 'Settings.dart';
part 'SettingsData.dart';