part of 'ConversationWidgetsImports.dart';

class BuildConversationItem extends StatelessWidget {
  final GetRoomModel chatRoom;
  final int index;
  const BuildConversationItem({Key? key,required this.chatRoom,required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()=>AutoRouter.of(context).push(CustomerChatRoomRoute(receiverName: chatRoom.members![index].name ?? "",roomId: chatRoom.id ?? 0)),
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(vertical: 5),
        decoration: BoxDecoration(
            color: MyColors.white, borderRadius: BorderRadius.circular(5)),
        child: Row(
          children: [
            CachedImage(
              url:"${chatRoom.members![index].image}",
              fit: BoxFit.fill,
              height: 70,
              width: 70,
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      MyText(
                          title: "${chatRoom.members![index].name}", color: MyColors.black, size: 12),
                      MyText(title: "${chatRoom.lastMessageCreatedDt}", color: MyColors.black, size: 11),
                    ],
                  ),
                  MyText(
                      title: "${chatRoom.lastMessageBody}", color: MyColors.grey, size: 11),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
