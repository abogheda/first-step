part of 'ConversationsImports.dart';

class ConversationsData {
  GenericBloc<List<GetRoomModel>> chats = GenericBloc<List<GetRoomModel>>([]);

  fetchRooms(
      BuildContext context, {
        bool showLoading = true,
      }) async {
    if (showLoading == true) DioUtils.showLoadingDialog();

    final data =
    await CustomerRepository(context).getChatRooms();


    chats.onUpdateData(data.reversed.toList());
    if (showLoading == true) DioUtils.dismissDialog();
  }
}
