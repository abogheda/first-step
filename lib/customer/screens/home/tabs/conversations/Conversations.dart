part of 'ConversationsImports.dart';

class Conversations extends StatefulWidget {
  const Conversations({Key? key}) : super(key: key);

  @override
  _ConversationsState createState() => _ConversationsState();
}

class _ConversationsState extends State<Conversations> {

  final ConversationsData conversationsData = ConversationsData();
  @override
  void initState() {
    super.initState();
    if (GlobalState.instance.get("visitor") == false) {

      conversationsData.fetchRooms(context, showLoading: false);
      conversationsData.fetchRooms(context);
    }
  }
  @override
  Widget build(BuildContext context) {
    var visitor= GlobalState.instance.get("visitor");
    return Scaffold(
      backgroundColor: MyColors.greyWhite,
      appBar: BuildCustomAppBar(title: tr(context, "chat")),
      body:visitor? BuildAuthCheckScreen(): BlocBuilder<GenericBloc<List<GetRoomModel>>,
          GenericState<List<GetRoomModel>>>(
        bloc: conversationsData.chats,
        builder: (context, chatsState) {
          // if(chatsState is GenericUpdateState) {
            if(chatsState.data.isNotEmpty) {
              return RefreshIndicator(
                onRefresh: () =>
                    Future.sync(
                          () =>
                          conversationsData.fetchRooms(
                              context, showLoading: false),
                    ),
                child: ListView.builder(
                  padding: const EdgeInsets.all(15),
                  itemCount: chatsState.data.length,
                  itemBuilder: (context, index) {
                    return BuildConversationItem(
                      chatRoom: chatsState.data[index], index: index,
                    );
                  },
                ),
              );
            }
            return Center(
              child: MyText(title: tr(context, "noChat"), color: MyColors.black, size: 12),
            );
          }
          // return LoadingDialog.showLoadingView();
        // },
      ),
    );
  }
}
