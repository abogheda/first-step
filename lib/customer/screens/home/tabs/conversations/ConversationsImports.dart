import 'package:base_flutter/customer/screens/home/tabs/conversations/widgets/ConversationWidgetsImports.dart';
import 'package:base_flutter/customer/screens/home/widgets/HomeWidgetsImports.dart';
import 'package:base_flutter/customer/widgets/BuildAuthCheckScreen.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:dio_helper/dio_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../../../models/get_room_model.dart';
import '../../../../resources/customer_repository_imports.dart';
part 'Conversations.dart';
part 'ConversationsData.dart';