part of 'FinishOrderImports.dart';

class FinishOrderData {
  final TextEditingController coupon = TextEditingController();
  final GenericBloc<CartModel?> getCartCubit = GenericBloc(null);
  final GenericBloc<GetDiscountModel?> getDiscountCubit = GenericBloc(null);
  final GenericBloc<bool> checkClickedCoupon = GenericBloc(false);
  final GenericBloc<num> totalPrice = GenericBloc(0);


  fetchCart(BuildContext context,{bool refresh = true}) async{
    var data = await CustomerRepository(context).getCart(refresh);
    getCartCubit.onUpdateData(data);
    print("ttttttttttt");
    totalPrice.onUpdateData(data?.orderPrice ?? 0);
    print("total Price is : ${totalPrice.state.data}");
  }
  checkCoupon(BuildContext context)async{
    if (coupon.text.isEmpty) {
      CustomToast.showSimpleToast(msg: tr(context,"sendCodeFirst"));
      return;
    }
   var data = await CustomerRepository(context).checkCoupon(getCartCubit.state.data?.orderPrice ?? 0, coupon.text);
    debugPrint("Print data : $data");
   getDiscountCubit.onUpdateData(data);
    debugPrint("Print Cubit : ${getDiscountCubit.state.data}");

    // coupon.clear();
    if(data?.discAmount != null){
      checkClickedCoupon.onUpdateData(true);

    }
    num total = getCartCubit.state.data!.orderPrice! - data!.discAmount!;
   totalPrice.onUpdateData(total);
  }

  removeFromCart(BuildContext context , int serviceId,int index)  async{
    var data = await CustomerRepository(context).removeFromCart(serviceId);
    if(data){
      getCartCubit.state.data?.cartItems?.removeAt(index);
      getCartCubit.onUpdateData(getCartCubit.state.data);
      CustomToast.showSimpleToast(msg: tr(context,"deletedSuccessfully"));
    }
  }
  createOrder(BuildContext context) async{
    if(getCartCubit.state.data!.cartItems!.isEmpty){
      CustomToast.showSimpleToast(msg: tr(context, "cartEmpty"));
      return ;
    }
    var data = await CustomerRepository(context).createOrder(coupon.text);
    // if(data != null){
      AutoRouter.of(context).push(PaymentRoute(id: data.id ?? 0, totalPrice: data.totalPrice ?? 0));
    // }
  }
}
