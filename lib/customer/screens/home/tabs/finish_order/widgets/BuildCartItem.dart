part of 'FinishOrderWidgetsImports.dart';

class BuildCartItem extends StatelessWidget {
  final CartItemModel? cartItemModel;
  final FinishOrderData finishOrderData;
  final int index;
  const BuildCartItem({Key? key, this.cartItemModel,required this.finishOrderData,required this.index}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(
          color: MyColors.white, borderRadius: BorderRadius.circular(5)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyText(title: tr(context, "serviceName"), color: MyColors.grey, size: 11),
              MyText(
                  title: "${cartItemModel?.serviceName}",
                  color: MyColors.black,
                  size: 12),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyText(title: tr(context, "price"), color: MyColors.grey, size: 11),
              MyText(
                  title: "${cartItemModel?.servicePrice} ${tr(context, "sr")} ",
                  color: MyColors.black,
                  size: 12),
            ],
          ),
          IconButton(
            icon: Icon(MdiIcons.trashCanOutline),
            color: MyColors.secondary,
            onPressed: () {
              finishOrderData.removeFromCart(context, cartItemModel?.id ?? 0,index);
            },
            padding: const EdgeInsets.all(0),
          ),
        ],
      ),
    );
  }
}
