part of 'FinishOrderWidgetsImports.dart';

class BuildCoupon extends StatelessWidget {
  final FinishOrderData finishOrderData;

  const BuildCoupon({required this.finishOrderData});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: GenericTextField(
            hint: tr(context, "coupon"),
            fillColor: MyColors.white,
            fieldTypes: FieldTypes.normal,
            type: TextInputType.text,
            controller: finishOrderData.coupon,
            action: TextInputAction.next,
            margin: const EdgeInsets.symmetric(vertical: 15),
            validate: (value) => value!.validateEmpty(context),
          ),
        ),
        SizedBox(width: 10),
        BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
          bloc: finishOrderData.checkClickedCoupon,
          builder: (context, state) {
            return InkWell(
              onTap: () => state.data == false?finishOrderData.checkCoupon(context) : {},
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                decoration: BoxDecoration(
                    color:state.data == false? MyColors.secondary: MyColors.grey,
                    borderRadius: BorderRadius.circular(5)),
                child: MyText(
                  title: tr(context, "send"),
                  size: 12,
                  alien: TextAlign.center,
                  color: MyColors.white,
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}
