import 'package:base_flutter/customer/models/get_discount_model.dart';
import 'package:base_flutter/customer/screens/home/tabs/finish_order/FinishOrderImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:tf_validator/tf_validator.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../models/cart_item_model.dart';
part 'BuildTotalPrice.dart';
part 'BuildCartItem.dart';
part 'BuildCartList.dart';
part 'BuildCoupon.dart';