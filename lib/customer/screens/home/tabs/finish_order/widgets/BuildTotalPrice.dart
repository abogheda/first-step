part of 'FinishOrderWidgetsImports.dart';

class BuildTotalPrice extends StatelessWidget {
  final num? totalPrice;
  final FinishOrderData finishOrderData;

  const BuildTotalPrice(
      {Key? key, this.totalPrice, required this.finishOrderData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: MyColors.white, borderRadius: BorderRadius.circular(5)),
      child: BlocBuilder<GenericBloc<num>,
          GenericState<num>>(
        bloc: finishOrderData.totalPrice,
        builder: (context, state) {
          return Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  MyText(
                    title: tr(context, "total"),
                    color: MyColors.black,
                    size: 12,
                    fontWeight: FontWeight.bold,
                  ),
                  MyText(
                      title: "$totalPrice ${tr(context, "sr")} ",
                      color: MyColors.secondary,
                      size: 11)
                ],
              ),
              SizedBox(
                height: 5,
              ),
              BlocBuilder<GenericBloc<GetDiscountModel?>, GenericState<GetDiscountModel?>>(
                bloc: finishOrderData.getDiscountCubit,
                builder: (context, discountState) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      MyText(
                          title: tr(context, "coupon"),
                          color: Colors.red,
                          size: 12,
                          fontWeight: FontWeight.bold),
                      MyText(
                          title: "${discountState.data?.discAmount ??0} ${tr(context, "sr")} ",
                          color: MyColors.secondary,
                          size: 11)
                    ],
                  );
                },
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  MyText(
                    title: tr(context, "priceAfterDiscount"),
                    color: MyColors.black,
                    size: 12,
                    fontWeight: FontWeight.bold,
                  ),
                  MyText(
                      title: "${state.data} ${tr(context, "sr")} ",
                      color: MyColors.secondary,
                      size: 11)
                ],
              ),
            ],
          );
        },
      ),
    );
  }
}
