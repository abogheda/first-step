part of 'FinishOrderWidgetsImports.dart';

class BuildCartList extends StatelessWidget {
  final List<CartItemModel>? cartItemModel;
  final FinishOrderData finishOrderData;
  const BuildCartList(
      {Key? key, this.cartItemModel, required this.finishOrderData})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(
        cartItemModel?.length ?? 0,
        (index) => BuildCartItem(
          cartItemModel: cartItemModel![index],
          finishOrderData: finishOrderData,
          index : index,
        ),
      ),
    );
  }
}
