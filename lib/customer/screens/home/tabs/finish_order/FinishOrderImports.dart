import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/models/get_discount_model.dart';
import 'package:base_flutter/customer/resources/customer_repository_imports.dart';
import 'package:base_flutter/customer/screens/home/tabs/finish_order/widgets/FinishOrderWidgetsImports.dart';
import 'package:base_flutter/customer/screens/home/widgets/HomeWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/general/utilities/utils_functions/LoadingDialog.dart';
import 'package:dio_helper/dio_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../../../models/cart_model.dart';
import '../../../../widgets/BuildAuthCheckScreen.dart';
part 'FinishOrder.dart';
part 'FinishOrderData.dart';