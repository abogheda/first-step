part of 'FinishOrderImports.dart';

class FinishOrder extends StatefulWidget {
  const FinishOrder({Key? key}) : super(key: key);

  @override
  _FinishOrderState createState() => _FinishOrderState();
}

class _FinishOrderState extends State<FinishOrder> {
  final FinishOrderData finishOrderData = FinishOrderData();

  @override
  void initState() {
    if (GlobalState.instance.get("visitor") == false) {
      finishOrderData.fetchCart(context);
      finishOrderData.checkClickedCoupon.onUpdateData(false);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var visitor = GlobalState.instance.get("visitor");

    return Scaffold(
      backgroundColor: MyColors.greyWhite,
      appBar: BuildCustomAppBar(title: tr(context, "completeOrder")),
      body:visitor? BuildAuthCheckScreen(): BlocBuilder<GenericBloc<CartModel?>, GenericState<CartModel?>>(
        bloc: finishOrderData.getCartCubit,
        builder: (context, state) {
          if (state is GenericUpdateState) {
            return Column(
              children: [
                Flexible(
                  child: ListView(
                    padding: const EdgeInsets.all(20),
                    children: [
                      BuildCartList(
                        cartItemModel: state.data?.cartItems,
                        finishOrderData: finishOrderData,
                      ),
                      BuildCoupon(finishOrderData: finishOrderData),
                      BuildTotalPrice(
                        totalPrice: state.data?.orderPrice,
                        finishOrderData: finishOrderData,
                      ),
                    ],
                  ),
                ),
                DefaultButton(
                  title: tr(context, "confirm"),
                  onTap: () => finishOrderData.createOrder(context),
                  margin: const EdgeInsets.all(20),
                )
              ],
            );
          }
          return LoadingDialog.showLoadingView();
        },
      ),
    );
  }
}
