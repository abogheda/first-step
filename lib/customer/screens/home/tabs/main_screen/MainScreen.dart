part of 'MainScreenImports.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final MainScreenData mainScreenData = MainScreenData();

  @override
  void initState() {
    mainScreenData.fetchData(context, refresh: false);
    mainScreenData.fetchData(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.greyWhite,
      appBar: BuildMainAppBar(mainScreenData: mainScreenData,),
      body: BlocBuilder<GenericBloc<HomeModel?>, GenericState<HomeModel?>>(
        bloc: mainScreenData.homeCubit,
        builder: (context, state) {
          if (state is GenericUpdateState) {
            if (state.data != null) {
              return ListView(
                padding: const EdgeInsets.all(20),
                children: [
                  BuildSwiper(sliderModel: state.data?.sliders,),
                  state.data!.categories.isNotEmpty?  BuildMainList(categoryModel: state.data?.categories,)
                  :Center(
                      child: MyText(
                        title: tr(context, "noData"),
                        size: 10,
                        color: MyColors.black,
                      )),
                ],
              );
            }
            return Center(
                child: MyText(
              title: tr(context, "noData"),
              size: 10,
              color: MyColors.black,
            ));
          }
          return LoadingDialog.showLoadingView();
        },
      ),
    );
  }
}
