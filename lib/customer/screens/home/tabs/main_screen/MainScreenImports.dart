import 'package:base_flutter/customer/screens/home/tabs/main_screen/widget/MainScreenWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/utils_functions/LoadingDialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../../../models/home_model.dart';
import '../../../../resources/customer_repository_imports.dart';
part 'MainScreen.dart';
part 'MainScreenData.dart';