part of 'MainScreenImports.dart';
class MainScreenData{
  final GenericBloc<HomeModel?> homeCubit = new GenericBloc(null);

  final TextEditingController search = new TextEditingController();
  // final GlobalKey<ScaffoldState> scaffold = new GlobalKey<ScaffoldState>();

  void fetchData(BuildContext context,{bool refresh=true}) async {
    var data = await CustomerRepository(context).getHomeData(refresh,search.text);
    print("========= data =======>$data");
    homeCubit.onUpdateData(data);
  }
}