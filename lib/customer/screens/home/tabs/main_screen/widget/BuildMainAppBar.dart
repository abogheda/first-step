part of 'MainScreenWidgetsImports.dart';

class BuildMainAppBar extends StatelessWidget implements PreferredSizeWidget {
final MainScreenData mainScreenData;

  const BuildMainAppBar({Key? key,required this.mainScreenData}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var user = context.read<UserCubit>().state.model;
    var visitor = GlobalState.instance.get("visitor");
    return AppBar(
      backgroundColor: MyColors.primary,
      elevation: 0,
      automaticallyImplyLeading: false,
      flexibleSpace: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
        visitor? Container() :   Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(MdiIcons.mapMarker, color: MyColors.white, size: 18),
              Container(
                width: 150,
                child: MyText(
                  overflow: TextOverflow.ellipsis,
                    title: user.address, color: MyColors.white, size: 11),
              ),
            ],
          ),
          GenericTextField(
            hint: tr(context, "search"),
            fillColor: MyColors.white,
            controller: mainScreenData.search,
            margin: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
            fieldTypes: FieldTypes.normal,
            suffixIcon: Icon(MdiIcons.magnify),
            type: TextInputType.text,
            action: TextInputAction.done,
            onChange: (value){
              mainScreenData.fetchData(context,refresh: true);
              // FocusScope.of(context).requestFocus(FocusNode());
            },
            onSubmit: (){
              mainScreenData.fetchData(context,refresh: true);
              FocusScope.of(context).requestFocus(FocusNode());
            },
            validate: (value) => value!.validateEmpty(context),
          )
        ],
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(kToolbarHeight + 70);
}
