part of 'MainScreenWidgetsImports.dart';

class BuildMainItem extends StatelessWidget {
final CategoryModel model;

  const BuildMainItem({Key? key,required this.model}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      closedColor: Colors.transparent,
      openColor: Colors.transparent,
      closedElevation: 0,
      openElevation: 0,
      transitionDuration: Duration(milliseconds: 800),
      transitionType: ContainerTransitionType.fadeThrough,
      openBuilder: (_, action) => Departments(title: model.name ?? "", catId: model.id,),
      closedBuilder: (_, action) =>  Container(
        decoration: BoxDecoration(
          color: MyColors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CachedImage(
              url:model.image! ,fit: BoxFit.fill,
              height: 100,
              width: 100,
            ),
            SizedBox(height: 10),
            MyText(title: "${model.name}", color: MyColors.black, size: 14),
          ],
        ),
      ),
    );
  }
}
