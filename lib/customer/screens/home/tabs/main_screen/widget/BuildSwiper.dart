part of 'MainScreenWidgetsImports.dart';

class BuildSwiper extends StatelessWidget {
final List<SliderModel>? sliderModel;

  const BuildSwiper({Key? key, this.sliderModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 180,
      child: Swiper(
        itemCount: sliderModel?.length,
        pagination: SwiperPagination(),
        autoplay: true,
        itemBuilder: (context, index) => CachedImage(
          url:sliderModel![index].image ?? "",fit: BoxFit.fill,
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }
}
