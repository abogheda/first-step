part of 'MainScreenWidgetsImports.dart';

class BuildMainList extends StatelessWidget {
final List<CategoryModel>? categoryModel;

  const BuildMainList({Key? key, this.categoryModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      padding: const EdgeInsets.symmetric(vertical: 20),
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: categoryModel?.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2, mainAxisSpacing: 15, crossAxisSpacing: 15),
      itemBuilder: (_, index) => BuildMainItem(model: categoryModel![index],),
    );
  }
}
