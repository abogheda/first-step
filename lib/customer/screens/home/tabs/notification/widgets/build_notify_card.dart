part of 'notifications_widgets_imports.dart';

class BuildNotifyCard extends StatelessWidget {
  final String title;
  final String date;

  const BuildNotifyCard({
    required this.title,
    required this.date,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Card(
          margin: const EdgeInsets.symmetric(vertical: 8),
          elevation: 6,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
          child: Container(
              height: MediaQuery.of(context).size.height * 0.12,
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 11),
              child: Row(children: [
                CircleAvatar(
                    radius: 30.0,
                    backgroundColor: Color(0xffF1F5F8),
                    child: ClipRRect(
                        child: Image.asset(
                          Res.logo,
                          width: 50,
                        ),
                        borderRadius: BorderRadius.circular(10.0))),
                SizedBox(width: 20),
                Expanded(
                    child:
                        MyText(title: title, color: MyColors.black, size: 10)),
                Align(
                    alignment: AlignmentDirectional.bottomStart,
                    child: MyText(
                        title: date, color: MyColors.blackOpacity, size: 9))
              ]))),
    );
  }
}
