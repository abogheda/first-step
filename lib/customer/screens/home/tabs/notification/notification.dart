part of 'notification_imports.dart';

class NotificationScreen extends StatefulWidget {
  @override
  State<NotificationScreen> createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  NotificationData notificationData = NotificationData();
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    if (GlobalState.instance.get("visitor") == false) {
      _scrollController.addListener(_scrollListener);
      notificationData.getNotifications(context, firstTime: true);
      context.read<NotificationCountCubit>().onUpdateUserData(0);    }


    super.initState();
  }

  _scrollToBottom() {
    Future.delayed(const Duration(milliseconds: 100)).then((value) {
      if (_scrollController.hasClients) {
        _scrollController.animateTo(
          _scrollController.position.maxScrollExtent,
          duration: const Duration(seconds: 15),
          curve: Curves.linear,
        );
      }
    });
  }

  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      print("End");
      if (notificationData.nextPage != '') {
        notificationData
            .getNotifications(context,
                firstTime: false, pageNumber: notificationData.currentPage + 1)
            .then((value) {
          print("djsaldjsalkdjskldjalkdjalkjda");
          _scrollToBottom();
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return GlobalState.instance.get("visitor") == false ?
    Scaffold(
      appBar: BuildCustomAppBar(title: tr(context, "notifications"),actions: [
        IconButton(
            onPressed: () => notificationData.deleteNotify(context),
            icon: Icon(
              Icons.delete,
              color: MyColors.white,
            )),
      ],),

      body: Stack(
        children: [
          BlocBuilder<GenericBloc<List<NotifyBodyModel>>,
              GenericState<List<NotifyBodyModel>>>(
            bloc: notificationData.allNotifications,
            builder: (context, state) {
              if (state is GenericUpdateState) {
                if (state.data.isNotEmpty) {
                  return RefreshIndicator(
                    onRefresh: () => notificationData.getNotifications(context,
                        firstTime: true, clearCount: true),
                    color: MyColors.primary,
                    child: ListView(
                      controller: _scrollController,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 15),
                      children: state.data
                          .map((e) => BuildNotifyCard(
                              title: e.body!, date: e.createdAt!))
                          .toList(),
                    ),
                  );
                } else {
                  return Center(
                    child: MyText(
                      title: tr(context, 'noNotifications'),
                      size: 15,
                      color: MyColors.black,
                    ),
                  );
                }
              } else {
                return LoadingDialog.showLoadingView();
              }
            },
          ),
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
            bloc: notificationData.isLoading,
            builder: (context, state) {
              if (state is GenericUpdateState) {
                return Visibility(
                    visible: state.data,
                    child: Center(
                        child: CupertinoActivityIndicator(
                            color: MyColors.primary, radius: 30)));
              } else {
                return SizedBox();
              }
            },
          ),
        ],
      ),
    ): BuildAuthCheckScreen();
  }
}
