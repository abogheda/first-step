part of 'notification_imports.dart';

class NotificationData {
  GenericBloc<List<NotifyBodyModel>> allNotifications = GenericBloc([]);
  GenericBloc<bool> isLoading = GenericBloc<bool>(false);
  List<NotifyBodyModel> notifications = [];
  int currentPage = 1;
  int totalPages = 0;
  int countItems = 0;
  int pageCount = 10;
  String nextPage = "";

  Future<void> getNotifications(BuildContext context,
      {int pageNumber = 1, bool firstTime = false,bool clearCount=false}) async {
    if(firstTime==false){
      isLoading.onUpdateData(true);
    }
    var data = await CustomerRepository(context)
        .getNotifications(pageNumber, pageCount);
    currentPage = data.pagination?.currentPage ?? 1;
    countItems = data.pagination?.countItems ?? 1;
    nextPage = data.pagination?.nextPageUrl ?? "";
    if (firstTime) {
      notifications = [];
      totalPages =
      data.pagination?.totalPages ?? 0;
    }

    data.notifyBody!
        .forEach((element) {
      notifications.add(element);
    });
    allNotifications.onUpdateData(notifications);
    if(firstTime==false){
      isLoading.onUpdateData(false);
    }
    if(clearCount){
      context.read<NotificationCountCubit>().onUpdateUserData(0);
    }
  }

  deleteNotify(BuildContext context) async {
    await CustomerRepository(context).deleteNotify();
    allNotifications.onUpdateData([]);
  }
}
