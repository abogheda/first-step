import 'package:base_flutter/customer/resources/customer_repository_imports.dart';
import 'package:base_flutter/customer/screens/home/tabs/notification/widgets/notifications_widgets_imports.dart';
import 'package:base_flutter/customer/widgets/BuildAuthCheckScreen.dart';
import 'package:base_flutter/general/utilities/utils_functions/LoadingDialog.dart';
import 'package:dio_helper/utils/GlobalState.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../../../../general/blocks/notification_count/notification_count_cubit.dart';
import '../../../../../general/constants/MyColors.dart';
import '../../../../models/notify_body_model.dart';
import '../../widgets/HomeWidgetsImports.dart';


part 'notification.dart';
part 'notification_data.dart';