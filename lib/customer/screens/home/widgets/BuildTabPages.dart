part of 'HomeWidgetsImports.dart';

class BuildTabPages extends StatelessWidget {
  const BuildTabPages({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TabBarView(
      children: [
        MainScreen(),
        NotificationScreen(),
        Conversations(),
        FinishOrder(),
        Settings(),
      ],
    );
  }
}
