
import 'package:base_flutter/customer/screens/home/HomeImports.dart';
import 'package:base_flutter/customer/screens/home/tabs/conversations/ConversationsImports.dart';
import 'package:base_flutter/customer/screens/home/tabs/finish_order/FinishOrderImports.dart';
import 'package:base_flutter/customer/screens/home/tabs/main_screen/MainScreenImports.dart';
import 'package:base_flutter/customer/screens/home/tabs/settings/SettingsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../tabs/notification/notification_imports.dart';
part 'BuildTabPages.dart';
part 'BuildTabView.dart';
part 'BuildTabItem.dart';
part 'BuildCustomAppBar.dart';