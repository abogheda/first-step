part of 'HomeWidgetsImports.dart';

class BuildTabView extends StatelessWidget {
  final HomeData homeData;

  const BuildTabView({required this.homeData});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
      bloc: homeData.homeCubit,
      builder: (_, state) {
        return TabBar(
          onTap: (int value) => homeData.homeCubit.onUpdateData(value),
          padding: EdgeInsets.symmetric(vertical: 20),
          tabs: [
            BuildTabItem(image: Res.home, index: 0, current: state.data),
            BuildTabItem(
                image: Res.notifications, index: 1, current: state.data),
            BuildTabItem(
                image: Res.conversations, index: 2, current: state.data),
            BuildTabItem(image: Res.finishOrder, index: 3, current: state.data),
            BuildTabItem(image: Res.settings, index: 4, current: state.data),
          ],
        );
      },
    );
  }
}
