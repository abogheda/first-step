part of 'HomeWidgetsImports.dart';

class BuildTabItem extends StatelessWidget {
  final String image;
  final int current;
  final int index;

  const BuildTabItem(
      {required this.image, required this.index, required this.current});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        color:current == index ? MyColors.primary:Colors.transparent,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          bottomRight: Radius.circular(20),
        ),
      ),
      child: ImageIcon(
        AssetImage(image),size: 27,
        color: current == index ? MyColors.white : MyColors.black,
      ),
    );
  }
}
