part of 'HomeWidgetsImports.dart';

class BuildCustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final List<Widget> actions;

  const BuildCustomAppBar({required this.title, this.actions =const []});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: MyColors.primary,
      elevation: 0,
      centerTitle: false,
      automaticallyImplyLeading: false,
      title: MyText(title: title, size: 17, color: MyColors.white),
      actions: actions,
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(kToolbarHeight+10);
}
