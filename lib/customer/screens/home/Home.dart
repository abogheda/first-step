part of 'HomeImports.dart';

class Home extends StatefulWidget {
  final int index;

  const Home({this.index = 0});

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final HomeData homeData = HomeData();

  @override
  void initState() {
    homeData.homeCubit.onUpdateData(widget.index);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: DefaultTabController(
        length: 5,
        initialIndex: widget.index,
        child: Scaffold(
          bottomNavigationBar: BuildTabView(homeData: homeData),
          body: BuildTabPages(),
        ),
      ),
      onWillPop: homeData.onBackPressed,
    );
  }
}
