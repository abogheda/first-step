part of 'PaymentWidgetsImports.dart';

class BuildPaymentWays extends StatelessWidget {
  final PaymentData paymentData;

  const BuildPaymentWays({required this.paymentData});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 5),
          child: MyText(
            title: "وسائل الدفع المتاحة",
            color: MyColors.black,
            size: 16,
          ),
        ),
        Container(
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.symmetric(vertical: 5),
          decoration: BoxDecoration(
              color: MyColors.white, borderRadius: BorderRadius.circular(5)),
          child: BlocBuilder<GenericBloc, GenericState>(
            bloc: paymentData.paymentCubit,
            builder: (_, state) {
              return Column(
                children: [
                  BuildPaymentItem(
                    value: 0,
                    groupValue: state.data,
                    title: "الدفع كاش",
                    paymentData: paymentData,
                    img: Res.dollar,
                  ),
                  BuildPaymentItem(
                    value: 1,
                    groupValue: state.data,
                    title: "الدفع بفيزا كارت",
                    paymentData: paymentData,
                    img: Res.visa,
                  ),
                  BuildPaymentItem(
                    value: 2,
                    groupValue: state.data,
                    title: "الدفع بماستر كارت",
                    paymentData: paymentData,
                    img: Res.master,
                  ),
                ],
              );
            },
          ),
        )
      ],
    );
  }
}
