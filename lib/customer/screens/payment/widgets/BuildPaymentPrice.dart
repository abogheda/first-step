part of 'PaymentWidgetsImports.dart';

class BuildPaymentPrice extends StatelessWidget {
final  num totalPrice;

  const BuildPaymentPrice({Key? key,required this.totalPrice}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(
          color: MyColors.white, borderRadius: BorderRadius.circular(5)),
      child: Column(
        children: [
          Lottie.asset(
            Res.payment,
            width: 200,
            height: 200,
            repeat: true,
          ),
          SizedBox(height: 30),
          MyText(
            title: " $totalPrice ${tr(context,"sr")}",
            color: MyColors.black,
            size: 25,
            alien: TextAlign.center,
          ),
          MyText(
            title: "${tr(context,"requiredToBeRepaid")}",
            color: MyColors.grey,
            size: 15,
            alien: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
