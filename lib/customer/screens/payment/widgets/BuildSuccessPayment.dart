part of 'PaymentWidgetsImports.dart';

class BuildSuccessPayment extends StatelessWidget {
final PaymentData paymentData;

  const BuildSuccessPayment({required this.paymentData}) ;
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Lottie.asset(
            Res.correct,
            width: 100,
            height: 100,
            repeat: true,
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: MyText(
              title: "تم الدفع بنجاح",
              color: MyColors.black,
              size: 17,
            ),
          ),
          MyText(
            title:
            "برجاء مراجعة قائمة الحجوزات لديك ومتابعتها لمعرفة حالة طلبكم",
            color: MyColors.grey,
            size: 12,
            alien: TextAlign.center,
          ),
          DefaultButton(
            title: "الرجوع للرئيسية",
            onTap: () =>AutoRouter.of(context).push(HomeRoute()),
            margin: const EdgeInsets.all(20),
          )
        ],
      ),
    );
  }
}
