part of 'PaymentWidgetsImports.dart';

class BuildPaymentItem extends StatelessWidget {
  final String img;
  final String title;
  final int groupValue;
  final int value;
  final PaymentData paymentData;

  const BuildPaymentItem(
      {required this.img,
      required this.title,
      required this.groupValue,
      required this.value,
      required this.paymentData});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.asset(img,scale: 3),
      title: MyText(title: title, size: 13, color: MyColors.black),
      trailing: Radio<int>(
        groupValue: groupValue,
        value: value,
        onChanged: (int? value) =>
            paymentData.paymentCubit.onUpdateData(value!),
      ),
    );
  }
}
