import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/payment/PaymentImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
part 'BuildPaymentItem.dart';
part 'BuildPaymentPrice.dart';
part 'BuildPaymentWays.dart';
part 'BuildSuccessPayment.dart';