part of 'PaymentImports.dart';

class PaymentData {
  final GenericBloc<int> paymentCubit = GenericBloc(0);

  completePayment(BuildContext context,int orderId,PaymentData paymentData) async{
    var data = await CustomerRepository(context).completePayment(orderId
        , paymentCubit.state.data == 0 ? 'cash' : 'online');
    if(data){
      showAcceptDialog(context,paymentData);
    }
  }
  void showAcceptDialog(BuildContext context, PaymentData paymentData) {
    showDialog(
      context: context,
      builder: (context) {
        return BuildSuccessPayment(paymentData: paymentData);
      },
    );
  }
}
