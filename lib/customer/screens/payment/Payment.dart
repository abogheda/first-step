part of 'PaymentImports.dart';

class Payment extends StatefulWidget {
final int id;
final num totalPrice;

  const Payment({Key? key,required this.id,required this.totalPrice}) : super(key: key);
  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  final PaymentData paymentData=PaymentData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.greyWhite,
      appBar: DefaultAppBar(title: tr(context,"payment")),
      body: ListView(
        padding: const EdgeInsets.all(20),
        children: [
          BuildPaymentPrice(totalPrice: widget.totalPrice,),
          BuildPaymentWays(paymentData:paymentData),
          DefaultButton(
            title: tr(context,"pay"),
            onTap: () =>paymentData.completePayment(context,widget.id ,paymentData),
            margin: const EdgeInsets.all(20),

          )
        ],
      ),
    );
  }
}
