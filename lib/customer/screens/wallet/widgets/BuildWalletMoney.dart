part of 'WalletWidgetsImports.dart';

class BuildWalletMoney extends StatelessWidget {
  final String walletMoney;

  const BuildWalletMoney({Key? key,required this.walletMoney}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: ListView(
        padding: const EdgeInsets.all(40),
        children: [
          Image.asset(Res.wallet),
          SizedBox(height: 30),
          MyText(
            title: " $walletMoney ${tr(context, "sr")} ",
            color: MyColors.black,
            size: 20,
            alien: TextAlign.center,
          ),
          MyText(
            title: tr(context,"totalBalance"),
            color: MyColors.black,
            size: 15,
            alien: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
