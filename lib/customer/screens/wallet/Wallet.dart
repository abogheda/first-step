part of 'WalletImports.dart';

class Wallet extends StatefulWidget {
  const Wallet({Key? key}) : super(key: key);

  @override
  _WalletState createState() => _WalletState();
}

class _WalletState extends State<Wallet> {
  final WalletData walletData = WalletData();


  @override
  void initState() {
    walletData.fetchData(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(title: tr(context, "wallet")),
      body: BlocBuilder<GenericBloc<String>, GenericState<String>>(
        bloc: walletData.walletCubit,
        builder: (context, state) {
          return Column(
            children: [
              BuildWalletMoney(walletMoney: state.data,),
              // DefaultButton(
              //   title: "تصفية المحفظة",
              //   onTap: () {},
              //   margin: const EdgeInsets.all(30),
              // )
            ],
          );
        },
      ),
    );
  }
}
