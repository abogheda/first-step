part of 'WalletImports.dart';
class WalletData{
  final GenericBloc<String> walletCubit = new GenericBloc("");

  void fetchData(BuildContext context)async {
    var data = await CustomerRepository(context).wallet();
    walletCubit.onUpdateData("$data");
  }
}