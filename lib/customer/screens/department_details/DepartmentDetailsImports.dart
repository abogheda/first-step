import 'package:base_flutter/customer/resources/customer_repository_imports.dart';
import 'package:base_flutter/customer/screens/department_details/tabs/department_offers/DepartmentOffersImports.dart';
import 'package:base_flutter/customer/screens/department_details/tabs/department_providers/DepartmentProvidersImports.dart';
import 'package:base_flutter/customer/screens/department_details/widgets/DepartmentDetailsWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/utils_functions/LoadingDialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../models/get_providers_model.dart';
part 'DepartmentDetails.dart';
part 'DepartmentDetailsData.dart';