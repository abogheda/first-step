part of 'DepartmentOffersImports.dart';
class DepartmentOffersData{

  final GenericBloc<GetOffersModel?> getOffersCubit = GenericBloc(null);

  void fetchOffers(BuildContext context,int subCategoryId,{int sectionId = 0,bool refresh = true,String search = ''}) async {
    var data = await CustomerRepository(context).getOffers(
        refresh, subCategoryId,sectionId,search);
    getOffersCubit.onUpdateData(data);
  }


}