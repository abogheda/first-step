part of 'DepartmentOffersWidgetsImports.dart';

class BuildOfferItem extends StatelessWidget {
  final OfferModel? model;

  const BuildOfferItem({Key? key, this.model}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var visitor = GlobalState.instance.get("visitor");

    return OpenContainer(
      closedColor: Colors.transparent,
      openColor: Colors.transparent,
      closedElevation: 0,
      openElevation: 0,
      transitionDuration: Duration(milliseconds: 800),
      transitionType: ContainerTransitionType.fadeThrough,
      openBuilder: (_, action) => visitor
          ? BuildAuthCheckScreen()
          : OfferDetails(
              model: model,
            ),
      closedBuilder: (_, action) => Container(
        padding: const EdgeInsets.all(8),
        margin: const EdgeInsets.symmetric(vertical: 5),
        decoration: BoxDecoration(
          color: MyColors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(
          children: [
            CachedImage(
              url: model?.image ?? "",
              fit: BoxFit.fill,
              height: 70,
              width: 70,
              haveRadius: false,
              boxShape: BoxShape.circle,
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      width: 150,
                      child: MyText(
                          title: "${model?.title}",
                          color: MyColors.black,
                          size: 12,
                        overflow: TextOverflow.ellipsis
                        ,
                      )),
                  RatingBar.builder(
                    initialRating: model?.avgRate?.toDouble() ?? 0,
                    minRating: 0,
                    direction: Axis.horizontal,
                    allowHalfRating: false,
                    updateOnDrag: false,
                    ignoreGestures: true,
                    itemCount: 5,
                    itemSize: 15,
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (double value) {},
                  ),
                ],
              ),
            ),
            BuildPriceItem(
                afterPrice: model?.price ?? 0,
                beforePrice: model?.priceBefore ?? 0),
          ],
        ),
      ),
    );
  }
}
