import 'package:animations/animations.dart';
import 'package:base_flutter/customer/models/offer_model.dart';
import 'package:base_flutter/customer/screens/department_details/tabs/department_offers/DepartmentOffersImports.dart';
import 'package:base_flutter/customer/screens/offer_details/OfferDetailsImports.dart';
import 'package:base_flutter/customer/widgets/BuildAuthCheckScreen.dart';
import 'package:base_flutter/customer/widgets/BuildPriceItem.dart';
import 'package:base_flutter/customer/widgets/BuildSubServiceItem.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:dio_helper/utils/GlobalState.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../models/sections_model.dart';
part 'BuildOfferItem.dart';
part 'BuildOfferList.dart';
part 'BuildListSubService.dart';
