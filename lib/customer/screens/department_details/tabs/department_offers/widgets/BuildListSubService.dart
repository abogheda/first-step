part of 'DepartmentOffersWidgetsImports.dart';

class BuildListSubService extends StatelessWidget {
final List<SectionsModel>? sectionsModel;
final int subCategoryId;
final DepartmentOffersData departmentOffersData;
  const BuildListSubService({Key? key, this.sectionsModel,required this.departmentOffersData,required this.subCategoryId}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 37,
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: sectionsModel?.length,
        itemBuilder: (_, index) {
          return BuildSubServiceItem(title: "${sectionsModel![index].title}", onTap: () {
            departmentOffersData.fetchOffers(context, subCategoryId,sectionId: sectionsModel![index].id);
          },);
        },
      ),
    );
  }
}
