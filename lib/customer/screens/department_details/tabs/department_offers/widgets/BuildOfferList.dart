part of 'DepartmentOffersWidgetsImports.dart';

class BuildOfferList extends StatelessWidget {
  final List<OfferModel>? offersModel;

  const BuildOfferList({Key? key, this.offersModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Flexible(
        child: ListView.builder(
            itemCount: offersModel?.length,
            itemBuilder: (_, index) {
              return BuildOfferItem(
                model: offersModel![index],
              );
            }));
  }
}
