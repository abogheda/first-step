part of 'DepartmentOffersImports.dart';

class DepartmentOffers extends StatefulWidget {
  final int subCategoryId;
  final String search;

  const DepartmentOffers(
      {Key? key, required this.subCategoryId, required this.search})
      : super(key: key);
  @override
  _DepartmentOffersState createState() => _DepartmentOffersState();
}

class _DepartmentOffersState extends State<DepartmentOffers> {
  final DepartmentOffersData departmentOffersData = DepartmentOffersData();

  @override
  void initState() {
    departmentOffersData.fetchOffers(context, widget.subCategoryId,
        search: widget.search, refresh: false);
    departmentOffersData.fetchOffers(context, widget.subCategoryId,
        search: widget.search);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: BlocBuilder<GenericBloc<GetOffersModel?>,
          GenericState<GetOffersModel?>>(
        bloc: departmentOffersData.getOffersCubit,
        builder: (context, state) {
          if (state is GenericUpdateState) {
            if (state.data!.offers!.isNotEmpty) {
              return Column(
                children: [
                  BuildListSubService(
                    sectionsModel: state.data?.sectionsModel, departmentOffersData: departmentOffersData, subCategoryId: widget.subCategoryId,
                  ),
                  BuildOfferList(
                    offersModel: state.data?.offers,
                  ),
                ],
              );
            }
            return Column(
              children: [
                BuildListSubService(
                  sectionsModel: state.data?.sectionsModel, departmentOffersData: departmentOffersData, subCategoryId: widget.subCategoryId,
                ),
                Align(
                  alignment: Alignment.center,
                  child: MyText(
                    title: tr(context,"noData"),
                    size: 12,
                    color: MyColors.black,
                  ),
                ),
              ],
            );
          }
          return LoadingDialog.showLoadingView();
        },
      ),
    );
  }
}
