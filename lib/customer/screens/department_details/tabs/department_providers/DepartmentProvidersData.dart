part of 'DepartmentProvidersImports.dart';

class DepartmentProvidersData {
  final GlobalKey<DropdownSearchState> selectedItem = GlobalKey();
  FilterModel? selectedSort;

  void onSelectType(FilterModel model) {
    selectedSort = model;
  }

}
