part of 'DepartmentProvidersImports.dart';

class DepartmentProviders extends StatefulWidget {
  final List<ProviderModel>? providerModel;
  final int subCategoryId;
  final DepartmentsDetailsData departmentsDetailsData;
  const DepartmentProviders(
      {Key? key,
      this.providerModel,
      required this.departmentsDetailsData,
      required this.subCategoryId})
      : super(key: key);
  @override
  _DepartmentProvidersState createState() => _DepartmentProvidersState();
}

class _DepartmentProvidersState extends State<DepartmentProviders> {
  final DepartmentProvidersData departmentProvidersData =
      DepartmentProvidersData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Column(
        children: [
          BuildArrange(
            departmentProvidersData: departmentProvidersData,
            departmentsDetailsData: widget.departmentsDetailsData,
            subCategoryId: widget.subCategoryId,
          ),
          Flexible(
            child: Visibility(
              visible: widget.providerModel!.isNotEmpty,
              child: ListView.builder(
                  itemCount: widget.providerModel?.length,
                  itemBuilder: (_, index) {
                    return BuildDepartProvidersItem(
                      index: index,
                      providerModel: widget.providerModel![index],
                      subCategoryId: widget.subCategoryId, departmentsDetailsData: widget.departmentsDetailsData,
                    );
                  }),
              replacement: Center(
                child: MyText(
                    title: tr(context, "noProviders"),
                    color: MyColors.black,
                    size: 12),
              ),
            ),
          )
        ],
      ),
    );
  }
}
