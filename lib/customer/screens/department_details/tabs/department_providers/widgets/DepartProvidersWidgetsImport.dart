import 'package:animations/animations.dart';
import 'package:base_flutter/customer/models/provider_model.dart';
import 'package:base_flutter/customer/screens/department_details/tabs/department_providers/DepartmentProvidersImports.dart';
import 'package:base_flutter/customer/screens/providers_details/ProvidersDetailsImports.dart';
import 'package:base_flutter/customer/widgets/BuildAuthCheckScreen.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:dio_helper/dio_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../../DepartmentDetailsImports.dart';
part 'BuildDepartProvidersItem.dart';
part 'BuildArrange.dart';