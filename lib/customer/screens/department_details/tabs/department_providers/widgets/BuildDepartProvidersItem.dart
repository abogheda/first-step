part of 'DepartProvidersWidgetsImport.dart';

class BuildDepartProvidersItem extends StatelessWidget {
  final ProviderModel? providerModel;
  final int? subCategoryId;
  final int index;
  final DepartmentsDetailsData departmentsDetailsData;
  const BuildDepartProvidersItem({Key? key,required this.index, this.providerModel,this.subCategoryId,required this.departmentsDetailsData})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    var visitor = GlobalState.instance.get("visitor");
    return OpenContainer(
      closedColor: Colors.transparent,
      openColor: Colors.transparent,
      closedElevation: 0,
      openElevation: 0,
      transitionDuration: Duration(milliseconds: 800),
      transitionType: ContainerTransitionType.fadeThrough,
      openBuilder: (_, action) => visitor
          ? BuildAuthCheckScreen()
          : ProvidersDetails(
        index: index,
              id: providerModel!.id,
              subCategoryId: subCategoryId,
              providerModel: providerModel,
            ),
      closedBuilder: (_, action) => Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 20),
        decoration: BoxDecoration(
            color: MyColors.white, borderRadius: BorderRadius.circular(5)),
        child: Row(
          children: [
            CachedImage(
              url: providerModel?.image ?? "",
              fit: BoxFit.fill,
              height: 70,
              width: 70,
              haveRadius: false,
              boxShape: BoxShape.circle,
            ),
            SizedBox(width: 20),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 150,
                        child: MyText(
                            title: "${providerModel?.name}",
                            color: MyColors.black,
                            overflow: TextOverflow.ellipsis,
                            size: 12),
                      ),
                      Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            color: MyColors.greyWhite,
                            shape: BoxShape.circle,
                          ),
                          child: providerModel?.isFavorite == true
                              ? Icon(
                                  MdiIcons.thumbUpOutline,
                                  color: Colors.red,
                                  size: 18,
                                )
                              : Icon(
                                  MdiIcons.thumbUpOutline,
                                  color: MyColors.grey,
                                  size: 18,
                                ))
                    ],
                  ),
                  Row(
                    children: [
                      Icon(MdiIcons.mapMarker, color: MyColors.grey, size: 18),
                      Container(
                        width: 180,
                        child: MyText(
                            title: "${providerModel?.mapDesc}",
                            color: MyColors.grey,
                            overflow: TextOverflow.ellipsis,
                            size: 10),
                      ),
                    ],
                  ),
                  RatingBar.builder(
                    initialRating: double.parse(providerModel?.avgRate ?? ''),
                    minRating: 0,
                    direction: Axis.horizontal,
                    allowHalfRating: false,
                    updateOnDrag: false,
                    ignoreGestures: true,
                    itemCount: 5,
                    itemSize: 15,
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (double value) {},
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
