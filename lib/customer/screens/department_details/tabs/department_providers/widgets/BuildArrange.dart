part of 'DepartProvidersWidgetsImport.dart';

class BuildArrange extends StatelessWidget {
  final DepartmentProvidersData departmentProvidersData;
  final DepartmentsDetailsData departmentsDetailsData;
  final int subCategoryId;

  const BuildArrange(
      {required this.departmentProvidersData,
      required this.departmentsDetailsData,
      required this.subCategoryId});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: MyText(title: tr(context, "arrange"), color: MyColors.black, size: 12),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              InkWell(
                onTap: () {
                  departmentsDetailsData.fetchProvider(context, subCategoryId,
                      location: "nearst");
                  CustomToast.showSimpleToast(msg: tr(context, "sendDone"));

                },
                child: Container(
                  height: 40,
                  width: 80,
                  decoration: BoxDecoration(
                    color: MyColors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Center(
                    child: MyText(
                        title: tr(context, "closest"), color: MyColors.black, size: 11),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  departmentsDetailsData.fetchProvider(context, subCategoryId,
                      sort: "top");
                  CustomToast.showSimpleToast(msg: tr(context, "sendDone"));
                },
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  height: 40,
                  width: 80,
                  decoration: BoxDecoration(
                    color: MyColors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Center(
                    child: MyText(
                        title: tr(context, "highest"),
                        color: MyColors.black,
                        size: 11),
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
