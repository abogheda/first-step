import 'package:base_flutter/customer/models/dots/FilterModel.dart';
import 'package:base_flutter/customer/screens/department_details/DepartmentDetailsImports.dart';
import 'package:base_flutter/customer/screens/department_details/tabs/department_providers/widgets/DepartProvidersWidgetsImport.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/Inputs/custom_dropDown/CustomDropDown.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/tf_validator.dart';

import '../../../../models/provider_model.dart';
part 'DepartmentProviders.dart';
part 'DepartmentProvidersData.dart';