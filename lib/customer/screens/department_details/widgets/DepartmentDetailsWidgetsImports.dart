import 'package:base_flutter/customer/models/slider_model.dart';
import 'package:base_flutter/customer/screens/department_details/DepartmentDetailsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'package:tf_validator/validator/Validator.dart';

part 'BuildImageSwiper.dart';

part 'BuildSearchAppBar.dart';

part 'BuildTabsItem.dart';

part 'BuildTabsView.dart';
