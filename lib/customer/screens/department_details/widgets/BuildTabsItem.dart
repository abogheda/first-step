part of 'DepartmentDetailsWidgetsImports.dart';

class BuildTabsItem extends StatelessWidget {
  final String title;
  final String? image;
  final int index;
  final int current;
  final bool giftImg;

  const BuildTabsItem(
      {
        required this.title,
         this.image,
      required this.index,
      required this.current,
      this.giftImg = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 85,
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: index == current ? MyColors.primary : MyColors.white),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Visibility(
            visible: giftImg,
            child: Image.asset(Res.gift, scale: 5),
            replacement: CachedImage(
              url: "$image",fit: BoxFit.fill,
              height: 30 ,
              width: 30,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(3.0),
            child: MyText(
              title: title,
              size: 11,
              color: index == current ? MyColors.white : MyColors.black,
            ),
          ),
        ],
      ),
    );
  }
}
