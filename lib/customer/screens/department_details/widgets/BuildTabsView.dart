part of 'DepartmentDetailsWidgetsImports.dart';

class BuildTabsView extends StatelessWidget {
  final DepartmentsDetailsData departmentsDetailsData;
  final String? title;
  final String? image;
  const BuildTabsView({required this.departmentsDetailsData, this.title, this.image});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
      bloc: departmentsDetailsData.tabsCubit,
      builder: (_, state) {
        return Container(
          margin: const EdgeInsets.symmetric(horizontal: 15),
          child: TabBar(
            indicatorColor: Colors.transparent,
            onTap: (value) =>
                departmentsDetailsData.tabsCubit.onUpdateData(value),
            tabs: [
              BuildTabsItem(
                title: "$title",
                index: 0,
                current: state.data,
                image: image,
              ),
              BuildTabsItem(
                title: tr(context, "offers"),
                index: 1,
                current: state.data,
                giftImg: true,
              )
            ],
          ),
        );
      },
    );
  }
}
