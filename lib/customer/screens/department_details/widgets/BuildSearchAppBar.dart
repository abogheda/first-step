part of 'DepartmentDetailsWidgetsImports.dart';

class BuildSearchAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final int subCategoryId;
  final DepartmentsDetailsData departmentsDetailsData;

  const BuildSearchAppBar(
      {required this.title, required this.departmentsDetailsData,required this.subCategoryId});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: MyColors.primary,
      title: MyText(
        title: "$title",
        size: 17,
        color: MyColors.white,
      ),
      centerTitle: false,
      leading: IconButton(
        icon: Icon(
          Icons.adaptive.arrow_back,
          size: 25,
          color: MyColors.white,
        ),
        onPressed: () => Navigator.of(context).pop(),
      ),
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(0),
        child: GenericTextField(
          fillColor: MyColors.white,
          controller: departmentsDetailsData.search,
          hint: tr(context, "search"),
          fieldTypes: FieldTypes.normal,
          type: TextInputType.emailAddress,
          margin: const EdgeInsets.all(10),
          action: TextInputAction.next,
          validate: (value) => value!.validateEmpty(context),
          onSubmit: (){
            departmentsDetailsData.fetchProvider(context, subCategoryId);
          },
          onChange: (val){
            departmentsDetailsData.fetchProvider(context, subCategoryId);
          },
          suffixIcon: Icon(MdiIcons.magnify, color: MyColors.primary),
        ),
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(130);
}
