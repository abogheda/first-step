part of 'DepartmentDetailsWidgetsImports.dart';

class BuildImageSwiper extends StatelessWidget {
final List<SliderModel>? sliderModel;

  const BuildImageSwiper({Key? key, this.sliderModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      margin: const EdgeInsets.symmetric(vertical: 10,horizontal: 20),
      child: Swiper(
        itemCount: sliderModel?.length ??1,
        pagination: SwiperPagination(),
        autoplay: true,
        itemBuilder: (context, index) => CachedImage(
          alignment: Alignment.topLeft,
          url: sliderModel?[index].image ?? "",fit: BoxFit.fill,
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }
}
