part of 'DepartmentDetailsImports.dart';

class DepartmentsDetails extends StatefulWidget {
  final int subCategoryId;
  final String? image;
  final String? title;

  const DepartmentsDetails(
      {Key? key, required this.subCategoryId, this.image, this.title})
      : super(key: key);
  @override
  _DepartmentsDetailsState createState() => _DepartmentsDetailsState();
}

class _DepartmentsDetailsState extends State<DepartmentsDetails> {
  final DepartmentsDetailsData departmentsDetailsData =
      DepartmentsDetailsData();

  @override
  void initState() {
    departmentsDetailsData.fetchProvider(context, widget.subCategoryId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: MyColors.greyWhite,
        appBar: BuildSearchAppBar(
          title: "${widget.title}",
          departmentsDetailsData: departmentsDetailsData, subCategoryId: widget.subCategoryId,
        ),
        body: BlocBuilder<GenericBloc<GetProvidersModel?>,
            GenericState<GetProvidersModel?>>(
          bloc: departmentsDetailsData.getProviderCubit,
          builder: (context, state) {
            if(state is GenericUpdateState) {
              return Column(
                children: [
                  BuildImageSwiper(sliderModel: state.data?.sliders,),
                  BuildTabsView(
                    departmentsDetailsData: departmentsDetailsData,
                    title: widget.title,
                    image: widget.image,
                  ),
                  Flexible(
                    child: TabBarView(
                      physics: NeverScrollableScrollPhysics(),
                      children: [
                        DepartmentProviders(
                          providerModel: state.data?.providers, departmentsDetailsData: departmentsDetailsData, subCategoryId: widget.subCategoryId,),
                        DepartmentOffers(search: departmentsDetailsData.search.text,
                        subCategoryId: widget.subCategoryId,),
                      ],
                    ),
                  )
                ],
              );
            }
            return LoadingDialog.showLoadingView();
          },
        ),
      ),
    );
  }
}
