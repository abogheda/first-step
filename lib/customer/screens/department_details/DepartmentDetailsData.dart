part of 'DepartmentDetailsImports.dart';

class DepartmentsDetailsData {
  static final DepartmentsDetailsData _departmentsDetailsData = DepartmentsDetailsData._internal();
  factory DepartmentsDetailsData() {
    return _departmentsDetailsData;
  }

  DepartmentsDetailsData._internal();
  final TextEditingController search = TextEditingController();
  final GenericBloc<int> tabsCubit = GenericBloc(0);

  final GenericBloc<GetProvidersModel?> getProviderCubit = GenericBloc(null);

   fetchProvider(BuildContext context, int subCategoryId,
      {bool refresh = true,String location = '',String sort = ''}) async {
    var data = await CustomerRepository(context)
        .getProviders(refresh, subCategoryId, search.text, location,sort);
    getProviderCubit.onUpdateData(data);
  }
}
