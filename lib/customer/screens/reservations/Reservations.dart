part of 'ReservationsImports.dart';

class Reservations extends StatefulWidget {
  const Reservations({Key? key}) : super(key: key);

  @override
  _ReservationsState createState() => _ReservationsState();
}

class _ReservationsState extends State<Reservations> {
  final ReservationsData reservationsData = ReservationsData();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: MyColors.greyWhite,
        appBar: DefaultAppBar(title: tr(context, "myReservations")),
        body: Column(
          children: [
            BuildTabView(reservationsData: reservationsData),
            BuildTabPages(),
          ],
        ),
      ),
    );
  }
}
