part of 'ReservationsWidgetsImports.dart';

class BuildTabView extends StatelessWidget {
  final ReservationsData reservationsData;

  const BuildTabView({required this.reservationsData});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
      bloc: reservationsData.tabsCubit,
      builder: (_, state) {
        return Container(
          margin: const EdgeInsets.symmetric(vertical: 15),
          child: TabBar(
            indicatorColor: Colors.transparent,
            onTap: (value) => reservationsData.tabsCubit.onUpdateData(value),
            tabs: [
              BuildTabItem(title: tr(context, "current"), index: 0, current: state.data),
              BuildTabItem(title: tr(context, "finished"), index: 1, current: state.data)
            ],
          ),
        );
      },
    );
  }
}
