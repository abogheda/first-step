part of 'ReservationsWidgetsImports.dart';

class BuildTabPages extends StatelessWidget {
  const BuildTabPages({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: TabBarView(
        physics: NeverScrollableScrollPhysics(),
        children: [
          Current(),
          Finished(),
        ],
      ),
    );
  }
}
