part of 'ReservationsWidgetsImports.dart';

class BuildReservationItem extends StatelessWidget {
  final OrderModel? orderModel;

  const BuildReservationItem({Key? key, this.orderModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      closedColor: Colors.transparent,
      openColor: Colors.transparent,
      closedElevation: 0,
      openElevation: 0,
      transitionDuration: Duration(milliseconds: 800),
      transitionType: ContainerTransitionType.fadeThrough,
      openBuilder: (_, action) => ReservationDetails(invoiceId: orderModel!.invoiceId,orderId: orderModel!.orderId,),
      closedBuilder: (_, action) => Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(vertical: 5),
        decoration: BoxDecoration(
            color: MyColors.white, borderRadius: BorderRadius.circular(5)),
        child: Row(
          children: [
            CachedImage(
              url:"${orderModel?.providerImage}" ,fit: BoxFit.fill,
              height: 70,
              width: 70,
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(title: "${orderModel?.providerName}", color: MyColors.black, size: 13),
                  MyText(title: "${orderModel?.serviceName}", color: MyColors.grey, size: 11),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                MyText(title: "${orderModel?.createdAt}", color: MyColors.grey, size: 11),
                Row(
                  children: [
                    MyText(
                        title: "${tr(context,"orderNum")} : ", color: MyColors.grey, size: 11),
                    MyText(title: "${orderModel?.orderNum}", color: MyColors.black, size: 11),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
