part of 'ReservationsWidgetsImports.dart';

class BuildTabItem extends StatelessWidget {
  final String title;
  final int index;
  final int current;

  const BuildTabItem(
      {required this.title, required this.index, required this.current});

  @override
  Widget build(BuildContext context) {
    return Tab(
      child: Container(
        height: 50,
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: index == current ? MyColors.primary : MyColors.white),
        child: MyText(
          title: title,
          size: 14,
          color: index == current ? MyColors.white : MyColors.black,
        ),
      ),
    );
  }
}
