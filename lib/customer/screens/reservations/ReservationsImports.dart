import 'package:base_flutter/customer/screens/reservations/widgets/ReservationsWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';


part 'Reservations.dart';
part 'ReservationsData.dart';