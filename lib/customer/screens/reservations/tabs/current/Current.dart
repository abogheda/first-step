part of 'CurrentImports.dart';

class Current extends StatefulWidget {
  const Current({Key? key}) : super(key: key);

  @override
  _CurrentState createState() => _CurrentState();
}

class _CurrentState extends State<Current> {
  final CurrentData currentData = CurrentData();

  @override
  void initState() {
    currentData.fetchNewOrder(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: BlocBuilder<GenericBloc<List<OrderModel>>, GenericState<List<OrderModel>>>(
        bloc: currentData.newOrdersCubit,
        builder: (context, state) {
          if(state is GenericUpdateState) {
            if(state.data.isNotEmpty) {
              return ListView.builder(
                padding: const EdgeInsets.all(10),
                  itemCount: state.data.length,
                  itemBuilder: (_, index) {
                    return BuildReservationItem(orderModel: state.data[index],);
                  });
            }
            return Center(child: MyText(title: tr(context, "noOrder"), color: MyColors.black, size: 12),);
          }
          return LoadingDialog.showLoadingView();
        },
      ),
    );
  }
}
