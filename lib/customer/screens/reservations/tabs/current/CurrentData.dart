part of 'CurrentImports.dart';
class CurrentData{

  final GenericBloc<List<OrderModel>> newOrdersCubit = GenericBloc([]);


  fetchNewOrder(BuildContext context) async {
    var data = await CustomerRepository(context).getMyNewOrders();
    newOrdersCubit.onUpdateData(data);
  }

}