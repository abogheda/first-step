import 'package:base_flutter/customer/screens/reservations/widgets/ReservationsWidgetsImports.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../../../../general/constants/MyColors.dart';
import '../../../../../general/utilities/utils_functions/LoadingDialog.dart';
import '../../../../models/order_model.dart';
import '../../../../resources/customer_repository_imports.dart';
part 'Finished.dart';
part 'FinishedData.dart';