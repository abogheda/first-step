part of 'FinishedImports.dart';
class FinishedData{

  final GenericBloc<List<OrderModel>> finishedOrdersCubit = GenericBloc([]);


  fetchFinishedOrder(BuildContext context) async {
    var data = await CustomerRepository(context).getMyFinishOrders();
    finishedOrdersCubit.onUpdateData(data);
  }
}