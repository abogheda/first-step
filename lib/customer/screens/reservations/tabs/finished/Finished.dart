part of 'FinishedImports.dart';

class Finished extends StatefulWidget {
  const Finished({Key? key}) : super(key: key);

  @override
  _FinishedState createState() => _FinishedState();
}

class _FinishedState extends State<Finished> {
  final FinishedData finishedData = FinishedData();


  @override
  void initState() {
    finishedData.fetchFinishedOrder(context);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: BlocBuilder<GenericBloc<List<OrderModel>>,
          GenericState<List<OrderModel>>>(
        bloc: finishedData.finishedOrdersCubit,
        builder: (context, state) {
          if (state is GenericUpdateState) {
            if (state.data.isNotEmpty) {
              return ListView.builder(
                  padding: const EdgeInsets.all(10),
                  itemCount: state.data.length,
                  itemBuilder: (_, index) {
                    return BuildReservationItem(
                      orderModel: state.data[index],
                    );
                  });
            }
            return Center(
              child: MyText(
                  title: tr(context, "noOrder"),
                  color: MyColors.black,
                  size: 12),
            );
          }
          return LoadingDialog.showLoadingView();
        },
      ),
    );
  }
}
