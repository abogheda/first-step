import 'package:base_flutter/customer/screens/languages/widgets/LanguagesWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:base_flutter/res.dart';
import 'package:lottie/lottie.dart';
import 'package:flutter/material.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';


import '../../../general/utilities/utils_functions/UtilsImports.dart';
import '../../resources/customer_repository_imports.dart';
part 'Languages.dart';
part 'LanguagesData.dart';