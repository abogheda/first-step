part of 'LanguagesImports.dart';

class Languages extends StatefulWidget {
  const Languages({Key? key}) : super(key: key);

  @override
  _LanguagesState createState() => _LanguagesState();
}

class _LanguagesState extends State<Languages> {
  final LanguagesData languagesData = LanguagesData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.greyWhite,
      appBar: DefaultAppBar(title: tr(context, "lang")),
      body: ListView(
        padding: const EdgeInsets.symmetric(vertical: 30),
        children: [
          Lottie.asset(
            Res.translate,
            width: 200,
            height: 200,
            repeat: true,
          ),
          BuildLangItem(
            title: tr(context, "langAr"),
            image: Res.arabic,
            onTap: () => languagesData.changeLanguage(context, "ar"), color: MyColors.secondary,
          ),
          BuildLangItem(
            title: tr(context, "langEn"),
            image: Res.english,
            color: MyColors.primary,
            onTap: () => languagesData.changeLanguage(context, "en"),
          ),
        ],
      ),
    );
  }
}
