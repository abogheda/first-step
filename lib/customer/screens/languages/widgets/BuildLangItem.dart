part of 'LanguagesWidgetsImports.dart';

class BuildLangItem extends StatelessWidget {
  final String title;
  final String image;
  final Color color;
  final Function() onTap;

  const BuildLangItem({required this.title, required this.image,required this.onTap,required this.color});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: color,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MyText(title: title, color: MyColors.white, size: 17,fontWeight: FontWeight.bold),
            SizedBox(width: 10),
            Image.asset(image, scale: 3),
          ],
        ),
      ),
    );
  }
}
