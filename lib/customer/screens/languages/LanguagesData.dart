part of 'LanguagesImports.dart';
class LanguagesData{


  void changeLanguage(BuildContext context,String lang) async {
    Utils.changeLanguage(lang, context);
      await CustomerRepository(context).changeLanguage(lang) ;
  }
}