import 'package:base_flutter/customer/models/offer_model.dart';
import 'package:base_flutter/customer/screens/offer_details/widgets/OfferDetailsWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/utils_functions/LoadingDialog.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../models/get_service_details.dart';
import '../../resources/customer_repository_imports.dart';
part 'OfferDetails.dart';
part 'OfferDetailsData.dart';