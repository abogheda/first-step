part of 'OfferDetailsImports.dart';

class OfferDetails extends StatefulWidget {
  final OfferModel? model;

  const OfferDetails({Key? key, this.model}) : super(key: key);
  @override
  _OfferDetailsState createState() => _OfferDetailsState();
}

class _OfferDetailsState extends State<OfferDetails> {
  final OfferDetailsData offerDetailsData = OfferDetailsData();

  @override
  void initState() {
    offerDetailsData.fetchServiceDetails(context, widget.model?.id ?? 0);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.greyWhite,
      appBar: DefaultAppBar(title: tr(context, "details")),
      bottomNavigationBar: DefaultButton(
        title: tr(context, "confirmReservation"),
        onTap: () => offerDetailsData.addToCart(
            context, widget.model?.id ?? 0, offerDetailsData),
        margin: const EdgeInsets.all(20),
      ),
      body: BlocBuilder<GenericBloc<GetServiceDetails?>,
          GenericState<GetServiceDetails?>>(
        bloc: offerDetailsData.getDetailsCubit,
        builder: (context, state) {
          if(state is GenericUpdateState) {
            return ListView(
              padding: const EdgeInsets.all(20),
              children: [
                BuildImagesSwiper(
                  offerDetailsData: offerDetailsData,
                  image: state.data?.service?.image,
                  id: state.data?.service?.id ?? 0,
                ),
                BuildTitle(
                  title: state.data?.service?.title,
                  rate: state.data?.service?.avgRate,
                ),
                BuildPrice(
                  priceAfter: state.data?.service?.price,
                  priceBefore: widget.model?.priceBefore,
                ),
                BuildProviderName(
                  providerName: state.data?.service?.providerName, providerId: state.data?.service?.providerId ??0,
                ),
                BuildDescription(
                  desc: state.data?.service?.description,
                ),
              ],
            );
          }
          return LoadingDialog.showLoadingView();
        },
      ),
    );
  }
}
