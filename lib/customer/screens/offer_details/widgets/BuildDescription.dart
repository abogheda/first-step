part of 'OfferDetailsWidgetsImports.dart';

class BuildDescription extends StatelessWidget {
final String? desc;

  const BuildDescription({Key? key, this.desc}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: MyColors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(title: tr(context,"desc"), color: MyColors.black, size: 13),
          MyText(
            title: " $desc ",
            color: MyColors.blackOpacity,
            size: 12,
          ),
        ],
      ),
    );
  }
}
