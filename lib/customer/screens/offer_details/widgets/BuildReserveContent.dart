part of 'OfferDetailsWidgetsImports.dart';

class BuildReserveContent extends StatelessWidget {
  final OfferDetailsData offerDetailsData;

  const BuildReserveContent({required this.offerDetailsData});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Lottie.asset(
            Res.correct,
            width: 100,
            height: 100,
            repeat: true,
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: MyText(
              title: "تم اضافة الخدمة بنجاح",
              color: MyColors.black,
              size: 17,
            ),
          ),
          MyText(
            title:
                "تم اضافة الخدمة ، في حالة استكمال الطلب برجاء التوجه الي صفحة اكمال الطلب او اضافة مزيد من الخدمات",
            color: MyColors.grey,
            size: 12,
            alien: TextAlign.center,
          ),
          Row(
            children: [
              Expanded(
                child: DefaultButton(
                  fontSize: 11,
                  margin:
                      const EdgeInsets.symmetric(horizontal: 2, vertical: 10),
                  title: "إضافة خدمات آخري",
                  onTap: () => AutoRouter.of(context).pop(),
                ),
              ),
              Expanded(
                child: DefaultButton(
                  title: "استكمال الطلب",
                  fontSize: 11,
                  onTap: () => AutoRouter.of(context).push(HomeRoute(index: 3)),
                  color: MyColors.secondary,
                  margin:
                      const EdgeInsets.symmetric(horizontal: 2, vertical: 10),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
