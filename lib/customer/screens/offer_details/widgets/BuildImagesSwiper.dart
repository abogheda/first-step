part of 'OfferDetailsWidgetsImports.dart';

class BuildImagesSwiper extends StatefulWidget {
  final String? image;
  final int id;
  final OfferDetailsData offerDetailsData;

  const BuildImagesSwiper(
      {Key? key, this.image, required this.id, required this.offerDetailsData})
      : super(key: key);

  @override
  State<BuildImagesSwiper> createState() => _BuildImagesSwiperState();
}

class _BuildImagesSwiperState extends State<BuildImagesSwiper> {
  @override
  void initState() {
    widget.offerDetailsData.favCubit.onUpdateData(widget
            .offerDetailsData.getDetailsCubit.state.data?.service?.isFavorite ??
        false);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      child: CachedImage(
        alignment: Alignment.topLeft,
        url: widget.image ?? "",
        fit: BoxFit.fill,
        borderRadius: BorderRadius.circular(10),
        child: BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
          bloc: widget.offerDetailsData.favCubit,
          builder: (context, state) {
            return state.data
                ? InkWell(
                    onTap: () {
                      widget.offerDetailsData.switchFav(context, widget.id);
                      widget.offerDetailsData.favCubit.onUpdateData(false);
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        color: MyColors.white,
                        shape: BoxShape.circle,
                      ),
                      child: Icon(
                        MdiIcons.thumbUpOutline,
                        color: Colors.red,
                        size: 18,
                      ),
                    ),
                  )
                : InkWell(
                    onTap: () {
                      widget.offerDetailsData.switchFav(context, widget.id);
                      widget.offerDetailsData.favCubit.onUpdateData(true);
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        color: MyColors.white,
                        shape: BoxShape.circle,
                      ),
                      child: Icon(
                        MdiIcons.thumbUpOutline,
                        color: MyColors.grey,
                        size: 18,
                      ),
                    ),
                  );
          },
        ),
      ),
    );
  }
}
