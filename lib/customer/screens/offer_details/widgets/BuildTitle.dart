part of 'OfferDetailsWidgetsImports.dart';

class BuildTitle extends StatelessWidget {
final  String? title;
final num? rate;

  const BuildTitle({Key? key, this.title,this.rate}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: MyColors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MyText(
            title:title??"",
            color: MyColors.black,
            size: 13,
          ),
          RatingBar.builder(
            initialRating: rate?.toDouble() ?? 0,
            minRating: 0,
            direction: Axis.horizontal,
            allowHalfRating: false,
            updateOnDrag: false,
            ignoreGestures: true,
            itemCount: 5,
            itemSize: 15,
            itemBuilder: (context, _) => Icon(
              Icons.star,
              color: Colors.amber,
            ),
            onRatingUpdate: (double value) {},
          ),
        ],
      ),
    );
  }
}
