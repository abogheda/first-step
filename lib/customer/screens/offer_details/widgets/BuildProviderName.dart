part of 'OfferDetailsWidgetsImports.dart';

class BuildProviderName extends StatelessWidget {
final String? providerName;
final int providerId;
  const BuildProviderName({Key? key, this.providerName,required this.providerId}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: MyColors.white,
      ),
      child: Row(
        children: [
          MyText(
            title: "${tr(context,"offerFrom")} : ",
            color: MyColors.black,
            size: 12,
          ),
          MyText(
            title: "$providerName",
            color: MyColors.secondary,
            size: 12,
          ),
        ],
      ),

    );
  }
}
