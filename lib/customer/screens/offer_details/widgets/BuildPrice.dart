part of 'OfferDetailsWidgetsImports.dart';

class BuildPrice extends StatelessWidget {
final num? priceAfter;
final num? priceBefore;

  const BuildPrice({Key? key, this.priceAfter, this.priceBefore}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: MyColors.white,
      ),
      child: Row(
        children: [
          Column(
            children: [
              MyText(
                title: "${tr(context,"priceBefore")} : ",
                color: MyColors.blackOpacity,
                size: 12,
              ),
              MyText(
                title: "$priceBefore ${tr(context,"sr")} ",
                color: MyColors.secondary,
                size: 12,
                decoration: TextDecoration.lineThrough,
              ),
            ],
          ),
          Expanded(
            child: Column(
              children: [
                MyText(
                  title: "${tr(context,"priceAfter")} : ",
                  color: MyColors.blackOpacity,
                  size: 12,
                ),
                MyText(title: "$priceAfter ${tr(context,"sr")} ", color: MyColors.secondary, size: 12),
              ],
            ),
          )
        ],
      ),
    );
  }
}
