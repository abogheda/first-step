part of 'OfferDetailsImports.dart';

class OfferDetailsData {

  final GenericBloc<bool> favCubit = GenericBloc(false);
  final GenericBloc<GetServiceDetails?> getDetailsCubit = GenericBloc(null);

  switchFav(BuildContext context , int id) async{
    await CustomerRepository(context).switchServiceFav(id);
  }
  void showReserve(BuildContext context, OfferDetailsData offerDetailsData) {
    showDialog(
      context: context,
      builder: (_) => BuildReserveContent(offerDetailsData: offerDetailsData),
    );
  }
  fetchServiceDetails(BuildContext context,int id) async{
    var data = await CustomerRepository(context).getServiceDetails(id);
    getDetailsCubit.onUpdateData(data);
  }
  addToCart(BuildContext context,int serviceId,OfferDetailsData offerDetailsData) async{
    var result = await CustomerRepository(context).addToCart(serviceId);
    if(result){
      showReserve(context,offerDetailsData);
    }
  }
}
