part of 'ProvidersOffersImports.dart';
class ProvidersOffersData{
  final GenericBloc<List<OfferModel>> offerCubit = GenericBloc([]);

  fetchOffers(BuildContext context,int id , {bool refresh = true}) async{
    var data = await CustomerRepository(context).getProviderOffers(refresh, id);
    offerCubit.onUpdateData(data);
  }
}