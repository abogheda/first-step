import 'package:animations/animations.dart';
import 'package:base_flutter/customer/models/offer_model.dart';
import 'package:base_flutter/customer/screens/offer_details/OfferDetailsImports.dart';
import 'package:base_flutter/customer/widgets/BuildPriceItem.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
part 'BuildOfferItem.dart';