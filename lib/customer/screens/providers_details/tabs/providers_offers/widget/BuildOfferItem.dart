part of 'ProvidersOffersWidgetsImports.dart';

class BuildOfferItem extends StatelessWidget {
  final OfferModel offerModel;

  const BuildOfferItem({Key? key, required this.offerModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      closedColor: Colors.transparent,
      openColor: Colors.transparent,
      closedElevation: 0,
      openElevation: 0,
      transitionDuration: Duration(milliseconds: 800),
      transitionType: ContainerTransitionType.fadeThrough,
      openBuilder: (_, action) => OfferDetails(
        model: offerModel,
      ),
      closedBuilder: (_, action) => Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(vertical: 5),
        decoration: BoxDecoration(
          color: MyColors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(
          children: [
            CachedImage(
              url: "${offerModel.title}",
              fit: BoxFit.fill,
              height: 70,
              width: 70,
              haveRadius: false,
              boxShape: BoxShape.circle,
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 150,
                    child: MyText(
                        title: "${offerModel.title}",
                        color: MyColors.black,
                        overflow: TextOverflow.ellipsis,
                        size: 13),
                  ),
                  Row(
                    children: [
                      MyText(
                          title: "${tr(context, "priceBefore")} : ",
                          color: MyColors.grey,
                          size: 11),
                      MyText(
                          title:
                              "${offerModel.priceBefore} ${tr(context, "sr")} ",
                          color: MyColors.secondary,
                          size: 11),
                    ],
                  ),
                  Row(
                    children: [
                      MyText(
                          title: "${tr(context, "priceAfter")} : ",
                          color: MyColors.grey,
                          size: 11),
                      MyText(
                          title: "${offerModel.price} ${tr(context, "sr")} ",
                          color: MyColors.secondary,
                          size: 11),
                    ],
                  ),
                  RatingBar.builder(
                    initialRating: offerModel.avgRate?.toDouble() ?? 0,
                    minRating: 0,
                    direction: Axis.horizontal,
                    allowHalfRating: false,
                    updateOnDrag: false,
                    ignoreGestures: true,
                    itemCount: 5,
                    itemSize: 15,
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (double value) {},
                  ),
                ],
              ),
            ),
            BuildPriceItem(
                afterPrice: offerModel.price ?? 0,
                beforePrice: offerModel.priceBefore ?? 0),
          ],
        ),
      ),
    );
  }
}
