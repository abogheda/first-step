part of 'ProvidersOffersImports.dart';

class ProvidersOffers extends StatefulWidget {
final int id;

  const ProvidersOffers({Key? key,required this.id}) : super(key: key);
  @override
  _ProvidersOffersState createState() => _ProvidersOffersState();
}

class _ProvidersOffersState extends State<ProvidersOffers> {
  final ProvidersOffersData providersOffersData = ProvidersOffersData();



  @override
  void initState() {
    providersOffersData.fetchOffers(context,widget.id,refresh: false);
    providersOffersData.fetchOffers(context, widget.id);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        body: BlocBuilder<GenericBloc<List<OfferModel>>, GenericState<List<OfferModel>>>(
          bloc: providersOffersData.offerCubit,
          builder: (context, state) {
            if(state is GenericUpdateState) {
              if(state.data.isNotEmpty) {
                return ListView.builder(
                    itemCount: state.data.length
                    , itemBuilder: (_, index) {
                  return BuildOfferItem(offerModel: state.data[index],);
                });
              }
              return Center(
                child: MyText(title: tr(context, "noOffers"),color: MyColors.black,size: 11,),
              );
            }
            return LoadingDialog.showLoadingView();
          },
        ));
  }
}
