part of 'ProvidersRatesImports.dart';

class ProvidersRates extends StatefulWidget {
  final int id;

  const ProvidersRates({Key? key, required this.id}) : super(key: key);

  @override
  _ProvidersRatesState createState() => _ProvidersRatesState();
}

class _ProvidersRatesState extends State<ProvidersRates> {
  final ProvidersRatesData providersRatesData = ProvidersRatesData();

  @override
  void initState() {
    providersRatesData.fetchRates(context, widget.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: BlocBuilder<GenericBloc<GetProviderRatesModel?>,
          GenericState<GetProviderRatesModel?>>(
        bloc: providersRatesData.getProviderRatesCubit,
        builder: (context, state) {
          if (state is GenericUpdateState) {
            if(state.data!.providerRates!.isNotEmpty) {
              return ListView.builder(
                padding: const EdgeInsets.all(10),
                  itemCount: state.data?.providerRates?.length,
                  itemBuilder: (_, index) {
                    return BuildRateItem(
                      providerRatesModel: state.data?.providerRates![index],
                    );
                  });
            }
            return Center(child: MyText(color: MyColors.black,title: tr(context, "noRates"),size: 12,),);
          }
          return LoadingDialog.showLoadingView();
        },
      ),
    );
  }
}
