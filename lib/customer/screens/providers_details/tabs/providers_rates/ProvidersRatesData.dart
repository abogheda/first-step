part of 'ProvidersRatesImports.dart';
class ProvidersRatesData{

  final GenericBloc<GetProviderRatesModel?> getProviderRatesCubit = GenericBloc(null);

  fetchRates(BuildContext context,int id) async{
    var data = await CustomerRepository(context).getProviderRates(id);
    getProviderRatesCubit.onUpdateData(data);
  }

}