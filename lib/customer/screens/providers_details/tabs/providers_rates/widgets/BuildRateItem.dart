part of'ProvidersRatesWidgetsImports.dart';
class BuildRateItem extends StatelessWidget {
final ProviderRatesModel? providerRatesModel;

  const BuildRateItem({Key? key, this.providerRatesModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: MyColors.white
      ),
      child: Row(
        children: [
          CachedImage(
            url: "${providerRatesModel?.userImage}",
            fit: BoxFit.fill,
            height: 60,
            width: 60,
            haveRadius: false,
            boxShape: BoxShape.circle,
          ),
          SizedBox(width: 10),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    MyText(
                        title: "${providerRatesModel?.userName}", color: MyColors.black, size: 13),
                    RatingBar.builder(
                      initialRating: double.parse("${providerRatesModel?.value}"),
                      minRating: 0,
                      direction: Axis.horizontal,
                      allowHalfRating: false,
                      updateOnDrag: false,
                      ignoreGestures: true,
                      itemCount: 5,
                      itemSize: 15,
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (double value) {},
                    ),
                  ],
                ),
                MyText(
                  title: "${providerRatesModel?.comment}",
                  color: MyColors.grey,
                  size: 11,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
