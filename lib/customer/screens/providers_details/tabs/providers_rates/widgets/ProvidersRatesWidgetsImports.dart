import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../models/provider_rates_model.dart';
part 'BuildRateItem.dart';