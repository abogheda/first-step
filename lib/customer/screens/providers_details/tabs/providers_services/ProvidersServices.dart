part of 'ProvidersServicesImports.dart';

class ProvidersServices extends StatefulWidget {
  final int id;

  const ProvidersServices({Key? key, required this.id}) : super(key: key);
  @override
  _ProvidersServicesState createState() => _ProvidersServicesState();
}

class _ProvidersServicesState extends State<ProvidersServices> {
  final ProvidersServicesData providersServicesData = ProvidersServicesData();

  @override
  void initState() {
    providersServicesData.fetchServices(context, widget.id);

    print(
        "-----------====================-------------->${providersServicesData.servicesCubit.state.data.toString()}");

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: BlocBuilder<GenericBloc<GetProviderServicesModel?>,
          GenericState<GetProviderServicesModel?>>(
        bloc: providersServicesData.servicesCubit,
        builder: (context, state) {
          if (state is GenericUpdateState) {
            print(
                "=============----------========>${providersServicesData.servicesCubit.state.data.toString()}");

            return Column(
              children: [
                BuildSubServices(
                  sectionsModel: state.data?.serviceCategories,
                  providersServicesData: providersServicesData,
                  id: widget.id,
                ),
                BuildFilter(
                  providersServicesData: providersServicesData,
                  id: widget.id,
                ),
                Visibility(
                  visible: state.data?.services?.length != 0,
                  child: Flexible(
                    child: ListView.builder(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 10),
                      itemBuilder: (_, index) => BuildServiceItem(
                        serviceModel: state.data?.services?[index],
                        index: index,
                      ),
                      itemCount: state.data?.services?.length,
                    ),
                  ),
                  replacement: Center(
                    child: MyText(
                        title: tr(context, "noServices"),
                        color: MyColors.black,
                        size: 12),
                  ),
                )
              ],
            );
          }
          return LoadingDialog.showLoadingView();
        },
      ),
    );
  }
}
