part of 'ProvidersServicesImports.dart';

class ProvidersServicesData {
  static final ProvidersServicesData _servicesData =
      ProvidersServicesData._internal();
  factory ProvidersServicesData() {
    return _servicesData;
  }

  ProvidersServicesData._internal();
  final TextEditingController search = TextEditingController();
  final ScrollController controller = ScrollController();
  final GenericBloc<GetProviderServicesModel?> servicesCubit =
      GenericBloc(null);
  final GlobalKey<DropdownSearchState> selectedItem = GlobalKey();
  FilterModel? selectedModel;

  void onSelectFilter(FilterModel model) {
    selectedModel = model;
  }

  void fetchServices(BuildContext context, int id, {int sectionId = 0}) async {
    var data = await CustomerRepository(context)
        .getProviderServices(id, search.text, sectionId);
    servicesCubit.onUpdateData(data);
  }
}
