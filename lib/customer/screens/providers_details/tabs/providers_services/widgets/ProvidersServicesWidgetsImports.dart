import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/models/dots/FilterModel.dart';
import 'package:base_flutter/customer/models/service_model.dart';
import 'package:base_flutter/customer/screens/providers_details/tabs/providers_services/ProvidersServicesImports.dart';
import 'package:base_flutter/customer/widgets/BuildSubServiceItem.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:tf_validator/tf_validator.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../../general/utilities/routers/RouterImports.gr.dart';
import '../../../../../models/sections_model.dart';
part 'BuildSubServices.dart';
part 'BuildFilter.dart';
part 'BuildServiceItem.dart';