part of 'ProvidersServicesWidgetsImports.dart';

class BuildServiceItem extends StatelessWidget {
  final ServiceModel? serviceModel;
  final int index;
  const BuildServiceItem({Key? key, this.serviceModel,required this.index}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => AutoRouter.of(context)
          .push(ServicesDetailsRoute(id: serviceModel?.id ?? 0, index: index)),
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(vertical: 5),
        decoration: BoxDecoration(
            color: MyColors.white, borderRadius: BorderRadius.circular(5)),
        child: Row(
          children: [
            CachedImage(
              url: serviceModel?.image ?? '',
              fit: BoxFit.fill,
              height: 70,
              width: 70,
              haveRadius: false,
              boxShape: BoxShape.circle,
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      MyText(
                          title: "${serviceModel?.title}",
                          color: MyColors.black,
                          size: 13),
                      Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          color: MyColors.greyWhite,
                          shape: BoxShape.circle,
                        ),
                        child: Icon(
                          MdiIcons.thumbUpOutline,
                          color: serviceModel?.isFavorite == true
                              ? Colors.red
                              : MyColors.grey,
                          size: 18,
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      MyText(
                          title: "${tr(context, "orderPrice")} :  ",
                          color: MyColors.black,
                          size: 11),
                      MyText(
                          title: "${serviceModel?.price} ${tr(context, "sr")}",
                          color: MyColors.primary,
                          size: 11),
                    ],
                  ),
                  RatingBar.builder(
                    initialRating: 5,
                    minRating: 0,
                    direction: Axis.horizontal,
                    allowHalfRating: false,
                    updateOnDrag: false,
                    ignoreGestures: true,
                    itemCount: 5,
                    itemSize: 15,
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (double value) {},
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
