part of 'ProvidersServicesWidgetsImports.dart';

class BuildSubServices extends StatelessWidget {
final List<SectionsModel>? sectionsModel;
final int id;
final ProvidersServicesData providersServicesData;
  const BuildSubServices({Key? key, this.sectionsModel,required this.providersServicesData,required this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 35,
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: sectionsModel?.length ?? 0,
        itemBuilder: (_, index) {
          return BuildSubServiceItem(title: "${sectionsModel![index].title}", onTap: () {
            providersServicesData.fetchServices(context, id,sectionId: sectionsModel![index].id);
          },);
        },
      ),
    );
  }
}
