part of 'ProvidersServicesWidgetsImports.dart';

class BuildFilter extends StatelessWidget {
  final ProvidersServicesData providersServicesData;
final int id;
  const BuildFilter({required this.providersServicesData,required this.id});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        children: [
          Expanded(
            flex: 3,
            child: GenericTextField(
              label: "اكتب كلمة بحثك",
              fieldTypes: FieldTypes.normal,
              type: TextInputType.text,
              fillColor: MyColors.white,
              suffixIcon: Icon(MdiIcons.magnify, color: MyColors.primary),
              controller: providersServicesData.search,
              action: TextInputAction.search,
              contentPadding: const EdgeInsets.all(10),
              validate: (value) => value!.validateEmpty(context),
              onSubmit: ()=>providersServicesData.fetchServices(context, id),
            ),
          ),
          SizedBox(width: 5),
          Expanded(
            flex: 2,
            child: DropdownTextField<FilterModel>(
              hint: "رتب حسب",
              validate: (value) => value.validateDropDown(context),
              useName: true,
              dropKey: providersServicesData.selectedItem,
              selectedItem: providersServicesData.selectedModel,
              fillColor: MyColors.white,
              contentPadding: const EdgeInsets.all(10),
              textSize: 12,
              data: FilterModel.filters.map((e) {
                e.name = e.name;
                return e;
              }).toList(),
              onChange: (FilterModel value) =>
                  providersServicesData.onSelectFilter(value),
            ),
          ),
        ],
      ),
    );
  }
}
