part of 'ProvidersImagesImports.dart';

class ProvidersImages extends StatefulWidget {
  final int id;

  const ProvidersImages({Key? key, required this.id}) : super(key: key);

  @override
  _ProvidersImagesState createState() => _ProvidersImagesState();
}

class _ProvidersImagesState extends State<ProvidersImages> {
  final ProvidersImagesData providersImagesData = ProvidersImagesData();

  @override
  void initState() {
    providersImagesData.fetchFiles(context, 112);
    // providersImagesData.fetchFiles(context, widget.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: BlocBuilder<GenericBloc<GetProviderImagesModel?>,
          GenericState<GetProviderImagesModel?>>(
        bloc: providersImagesData.getProviderRatesCubit,
        builder: (context, state) {
          if (state is GenericUpdateState) {
            if (state.data!.providerFiles!.isNotEmpty) {
              return GridView.builder(
                padding: const EdgeInsets.all(8),
                itemCount: state.data?.providerFiles?.length,
                itemBuilder: (_, index) {
                  return BuildImageItem(
                    serviceFileModel: state.data?.providerFiles?[index],
                  );
                },
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 5.0,
                  mainAxisSpacing: 5.0,
                ),
              );
            }
            return Center(
              child: MyText(
                title: tr(context, "noPhotos"),
                color: MyColors.black,
                size: 12,
              ),
            );
          }
          return LoadingDialog.showLoadingView();
        },
      ),
    );
  }
}
