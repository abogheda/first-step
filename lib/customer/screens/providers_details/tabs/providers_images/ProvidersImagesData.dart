part of 'ProvidersImagesImports.dart';
class ProvidersImagesData{
  final GenericBloc<GetProviderImagesModel?> getProviderRatesCubit = GenericBloc(null);

  fetchFiles(BuildContext context,int id) async{
    var data = await CustomerRepository(context).getProviderFiles(id);
    getProviderRatesCubit.onUpdateData(data);
  }

}