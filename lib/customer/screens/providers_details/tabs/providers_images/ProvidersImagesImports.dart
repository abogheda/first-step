import 'package:base_flutter/customer/screens/providers_details/tabs/providers_images/widgets/ProvidersImagesWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/utils_functions/LoadingDialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../../../models/get_provider_images_model.dart';
import '../../../../resources/customer_repository_imports.dart';
part 'ProvidersImages.dart';
part 'ProvidersImagesData.dart';