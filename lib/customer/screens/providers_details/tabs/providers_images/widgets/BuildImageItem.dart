part of 'ProvidersImagesWidgetsImports.dart';

class BuildImageItem extends StatelessWidget {
final  ServiceFileModel? serviceFileModel;

  const BuildImageItem({Key? key, this.serviceFileModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return CachedImage(
      url:serviceFileModel?.file ?? "" ,fit: BoxFit.fill,
      borderRadius: BorderRadius.circular(5),
    );
  }
}
