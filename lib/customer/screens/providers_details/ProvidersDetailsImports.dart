import 'package:base_flutter/customer/models/provider_timing_model.dart';
import 'package:base_flutter/customer/resources/customer_repository_imports.dart';
import 'package:base_flutter/customer/screens/providers_details/widgets/ProvidersDetailsWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../models/get_providers_model.dart';
import '../../models/provider_model.dart';
part 'ProvidersDetails.dart';
part 'ProvidersDetailsData.dart';