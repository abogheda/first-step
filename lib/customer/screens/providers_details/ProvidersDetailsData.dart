part of 'ProvidersDetailsImports.dart';

class ProvidersDetailsData {
  final GenericBloc<int> tabsCubit = GenericBloc(0);
  final GenericBloc<bool> favCubit = GenericBloc(false);
  final GenericBloc<List<ProviderTimingModel>> timingCubit = GenericBloc([]);


  switchFav(BuildContext context , int id) async{
     await CustomerRepository(context).switchUserFav(id);
  }
  getProviderTime(BuildContext context, int id , {bool refresh = true}) async{
    var data = await CustomerRepository(context).getProviderTime(refresh, id);
    timingCubit.onUpdateData(data);
  }
  void showWorkTime(BuildContext context,ProvidersDetailsData providersDetailsData) {
    showModalBottomSheet(
      backgroundColor: MyColors.white,
      context: context,
      builder: (_) => BuildTimeContent(providersDetailsData: providersDetailsData,),
    );
  }
  final GenericBloc<GetProvidersModel?> getProviderCubit = GenericBloc(null);

   fetchProvider(BuildContext context, int subCategoryId,
      {bool refresh = true,String location = '',String sort = ''}) async {
    var data = await CustomerRepository(context)
        .getProviders(refresh, subCategoryId, "", location,sort);
    getProviderCubit.onUpdateData(data);
  }
}
