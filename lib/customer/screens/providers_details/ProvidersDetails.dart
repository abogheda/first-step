part of 'ProvidersDetailsImports.dart';

class ProvidersDetails extends StatefulWidget {
final int id;
final int? index;
final int? subCategoryId;
final ProviderModel? providerModel;
  const ProvidersDetails({Key? key,required this.id,this.providerModel, this.subCategoryId, this.index}) : super(key: key);
  @override
  _ProvidersDetailsState createState() => _ProvidersDetailsState();
}

class _ProvidersDetailsState extends State<ProvidersDetails> {
  final ProvidersDetailsData providersDetailsData = ProvidersDetailsData();


  @override
  void initState() {
    providersDetailsData.getProviderTime(context, widget.id);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: DefaultAppBar(title: tr(context, "details"),
        ),
        backgroundColor: MyColors.greyWhite,
        bottomNavigationBar: DefaultButton(
          title: tr(context, "workTimes"),
          onTap: () =>providersDetailsData.showWorkTime(context,providersDetailsData,),
          margin: const EdgeInsets.all(20),
        ),
        body: Column(
          children: [
            BuildImage(
              model: widget.providerModel,
              id: widget.id,
              providersDetailsData: providersDetailsData, subCategoryId: widget.subCategoryId ?? 0, index: widget.index ??0,
            ),
            BuildTabsView(providersDetailsData: providersDetailsData),
            BuildTabsPages(id: widget.id,),
          ],
        ),
      ),
    );
  }
}
