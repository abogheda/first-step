part of 'ProvidersDetailsWidgetsImports.dart';

class BuildTimeContent extends StatelessWidget {
  final ProvidersDetailsData providersDetailsData;

  const BuildTimeContent({Key? key, required this.providersDetailsData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.all(20),
      decoration: BoxDecoration(
          color: MyColors.greyWhite, borderRadius: BorderRadius.circular(10)),
      child: BlocBuilder<GenericBloc<List<ProviderTimingModel>>, GenericState<List<ProviderTimingModel>>>(
        bloc: providersDetailsData.timingCubit,
        builder: (context, state) {
          if(state is GenericUpdateState) {
            if(state.data.isNotEmpty) {
              return ListView(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      MyText(
                        title:tr(context, "workTimes"),
                        color: MyColors.primary,
                        size: 15,
                        fontWeight: FontWeight.bold,
                      ),
                      IconButton(
                        onPressed: () => AutoRouter.of(context).pop(),
                        icon: Icon(
                          MdiIcons.close,
                          color: MyColors.black,
                        ),
                      )
                    ],
                  ),
                  ...List.generate(
                    state.data.length,
                        (index) =>
                        BuildTimeItem(
                          day: "${state.data[index].day}",
                          from: "${state.data[index].from}",
                          to: "${state.data[index].to}",
                        ),
                  ),
                ],
              );
            }
            return Center(child: MyText(title: tr(context, "noData"),size: 12,color: MyColors.black,),);
          }
          return LoadingDialog.showLoadingView();
        },
      ),
    );
  }
}
