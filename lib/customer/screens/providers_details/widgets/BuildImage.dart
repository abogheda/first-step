part of 'ProvidersDetailsWidgetsImports.dart';

class BuildImage extends StatefulWidget {
  final int id;
  final int subCategoryId;
  final int index;
  final ProviderModel? model;
  final ProvidersDetailsData providersDetailsData;

  const BuildImage(
      {Key? key,
      this.model,
      required this.id,
      required this.index,
      required this.subCategoryId,
      required this.providersDetailsData})
      : super(key: key);

  @override
  State<BuildImage> createState() => _BuildImageState();
}

class _BuildImageState extends State<BuildImage> {
  @override
  void initState() {
    widget.providersDetailsData.favCubit
        .onUpdateData(widget.model?.isFavorite ?? false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(20),
      child: CachedImage(
        height: 180,
        url: widget.model?.image ?? "",
        fit: BoxFit.fill,
        borderRadius: BorderRadius.circular(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
              bloc: widget.providersDetailsData.favCubit,
              builder: (context, state) {
                return state.data
                    ? InkWell(
                        onTap: () {
                          widget.providersDetailsData.switchFav(
                            context,
                            widget.id,
                          )..then((value) {
                              widget.providersDetailsData.favCubit
                                  .onUpdateData(false);
                              DepartmentsDetailsData()
                                  .fetchProvider(context, widget.subCategoryId);
                            });
                        },
                        child: Container(
                          margin: const EdgeInsets.all(10),
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                            color: MyColors.white,
                            shape: BoxShape.circle,
                          ),
                          child: Icon(
                            MdiIcons.thumbUpOutline,
                            color: Colors.red,
                            size: 18,
                          ),
                        ),
                      )
                    : InkWell(
                        onTap: () {
                          widget.providersDetailsData.switchFav(
                            context,
                            widget.id,
                          )..then((value) {
                              widget.providersDetailsData.favCubit
                                  .onUpdateData(true);
                              DepartmentsDetailsData()
                                  .fetchProvider(context, widget.subCategoryId);
                            });
                        },
                        child: Container(
                          margin: const EdgeInsets.all(10),
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                            color: MyColors.white,
                            shape: BoxShape.circle,
                          ),
                          child: Icon(
                            MdiIcons.thumbUpOutline,
                            color: MyColors.grey,
                            size: 18,
                          ),
                        ),
                      );
              },
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    MyText(
                      title: "${widget.model?.name}",
                      color: MyColors.white,
                      size: 13,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          onTap: () {
                            Utils.navigateToMapWithDirection(
                                lat: widget.model?.lat ?? '',
                                lng: widget.model?.lng ?? '',
                                context: context);
                          },
                          child: Container(
                            width: 190,
                            child: MyText(
                              title: "${widget.model?.mapDesc}",
                              color: MyColors.white,
                              overflow: TextOverflow.ellipsis,
                              decoration: TextDecoration.underline,
                              size: 11,
                            ),
                          ),
                        ),
                        RatingBar.builder(
                          initialRating:
                              double.parse(widget.model?.avgRate ?? ""),
                          minRating: 0,
                          direction: Axis.horizontal,
                          allowHalfRating: false,
                          updateOnDrag: false,
                          ignoreGestures: true,
                          itemCount: 5,
                          itemSize: 15,
                          unratedColor: MyColors.white,
                          itemBuilder: (context, _) => Icon(
                            Icons.star,
                            color: Colors.amber,
                          ),
                          onRatingUpdate: (double value) {},
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
