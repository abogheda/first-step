part of 'ProvidersDetailsWidgetsImports.dart';

class BuildTabsItem extends StatelessWidget {
  final String title;
  final int index;
  final int current;
  final IconData iconData;

  const BuildTabsItem({
    required this.title,
    required this.index,
    required this.current,
    required this.iconData,
  });

  @override
  Widget build(BuildContext context) {
    return Tab(
      child: Container(
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: index == current ? MyColors.primary : MyColors.white),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              iconData,
              color: index == current ? MyColors.white : MyColors.black,
              size: 20,
            ),
            SizedBox(width: 3),
            MyText(
              title: title,
              size: 11,
              color: index == current ? MyColors.white : MyColors.black,
            ),
          ],
        ),
      ),
    );
  }
}
