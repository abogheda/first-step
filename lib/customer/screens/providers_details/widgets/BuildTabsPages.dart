part of 'ProvidersDetailsWidgetsImports.dart';

class BuildTabsPages extends StatelessWidget {
final int id;

  const BuildTabsPages({Key? key,required this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: TabBarView(
        physics: NeverScrollableScrollPhysics(),
        children: [
          ProvidersOffers(id: id,),
          ProvidersServices(id: id,),
          ProvidersRates(id: id,),
          ProvidersImages(id: id,),
        ],
      ),
    );
  }
}
