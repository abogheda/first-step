part of 'ProvidersDetailsWidgetsImports.dart';

class BuildTabsView extends StatelessWidget {
  final ProvidersDetailsData providersDetailsData;

  const BuildTabsView({required this.providersDetailsData});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
      bloc: providersDetailsData.tabsCubit,
      builder: (_, state) {
        return TabBar(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          labelPadding: EdgeInsets.symmetric(horizontal: 5),
          indicatorSize: TabBarIndicatorSize.tab,
          indicatorColor: Colors.transparent,
          onTap: (value) => providersDetailsData.tabsCubit.onUpdateData(value),
          tabs: [
            BuildTabsItem(
              title: "عروض",
              index: 0,
              current: state.data,
              iconData: MdiIcons.tagOutline,
            ),
            BuildTabsItem(
              title: "خدمات",
              index: 1,
              current: state.data,
              iconData: MdiIcons.cubeOutline,
            ),
            BuildTabsItem(
              title: "تقييمات",
              index: 2,
              current: state.data,
              iconData: MdiIcons.star,
            ),
            BuildTabsItem(
              title: "صور",
              index: 3,
              current: state.data,
              iconData: MdiIcons.imageOutline,
            )
          ],
        );
      },
    );
  }
}
