part of 'BuildBillDetailsWidgetsImports.dart';
class BuildServiceInfo extends StatelessWidget {
  final InvoiceModel? model;

  const BuildServiceInfo({Key? key, this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.symmetric(vertical: 15),
      decoration: BoxDecoration(
          color: MyColors.white, borderRadius: BorderRadius.circular(5)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(
            title: tr(context, "serviceDataProvided"),
            color: MyColors.secondary,
            size: 15,
          ),
          Divider(color: MyColors.grey),
          Row(
            children: [
              BuildDetailsItem(
                  title: tr(context, "serviceName"), details: "${model?.serviceName}"),
              BuildDetailsItem(title: tr(context, "price"), details: "${model?.totalPrice}")
            ],
          ),

          Row(
            children: [
              BuildDetailsItem(
                  title: tr(context, "payMethod"), details: "${model?.payStatusText}"),
            ],
          ),
        ],
      ),
    );  }
}
