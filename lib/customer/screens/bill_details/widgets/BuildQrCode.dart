part of 'BuildBillDetailsWidgetsImports.dart';

class BuildQrCode extends StatelessWidget {
  final InvoiceModel? model;

  const BuildQrCode({Key? key, this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      margin: const EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(
          color: MyColors.white, borderRadius: BorderRadius.circular(5)),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyText(
                title: tr(context, "taxNumber"),
                color: MyColors.secondary,
                size: 15,
              ),
              MyText(
                title: "${model?.providerCommercialNumber}",
                color: MyColors.secondary,
                size: 13,
              ),
            ],
          ),
          Container(
            margin: const EdgeInsets.all(10),
            child: QrImage(
              data: model?.providerCommercialNumber ?? '',
              size: 200,
              version: QrVersions.auto,
              gapless: false,
              embeddedImage: AssetImage(Res.logo),
              embeddedImageStyle: QrEmbeddedImageStyle(
                color: MyColors.primary,
                size: Size(35, 35),
              ),
            )
          ),
        ],
      ),
    );
  }
}
