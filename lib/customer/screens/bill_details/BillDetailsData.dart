part of 'BillDetailsImports.dart';

class BillDetailsData {
  final GenericBloc<InvoiceModel?> invoiceCubit = GenericBloc(null);

  fetchBillDetails(BuildContext context,int invoiceId) async{
    var data = await CustomerRepository(context).getInvoicesDetails(invoiceId);
    invoiceCubit.onUpdateData(data);
  }
}
