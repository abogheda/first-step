part of 'BillDetailsImports.dart';

class BillDetails extends StatefulWidget {
  final int id;

  const BillDetails({Key? key, required this.id}) : super(key: key);

  @override
  _BillDetailsState createState() => _BillDetailsState();
}

class _BillDetailsState extends State<BillDetails> {
  final BillDetailsData billDetailsData = BillDetailsData();



  @override
  void initState() {
    billDetailsData.fetchBillDetails(context, widget.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.greyWhite,
      appBar: DefaultAppBar(title: tr(context, "bill")),
      body: BlocBuilder<GenericBloc<InvoiceModel?>, GenericState<InvoiceModel?>>(
        bloc: billDetailsData.invoiceCubit,
        builder: (context, state) {
          if(state is GenericUpdateState) {
            return ListView(
              padding: const EdgeInsets.all(20),
              children: [
                BuildTimeInfo(model: state.data,),
                BuildServiceInfo(model: state.data,),
                BuildQrCode(model: state.data,)
              ],
            );
          }
          return LoadingDialog.showLoadingView();
        },
      ),
    );
  }
}
