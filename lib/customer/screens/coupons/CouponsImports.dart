import 'package:base_flutter/customer/models/get_coupon_model.dart';
import 'package:base_flutter/customer/screens/coupons/widgets/CouponsWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../resources/customer_repository_imports.dart';
part 'Coupons.dart';
part 'CouponsData.dart';