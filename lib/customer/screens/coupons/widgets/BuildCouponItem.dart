part of 'CouponsWidgetsImports.dart';

class BuildCouponItem extends StatelessWidget {
  final GetCouponModel? getCouponModel;

  const BuildCouponItem({Key? key, this.getCouponModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(title: tr(context, "theCode"), color: MyColors.primary, size: 12),
                  MyText(title: "${getCouponModel?.name}", color: MyColors.black, size: 11),
                ],
              ),
              Column(
                children: [
                  MyText(title: tr(context, "discountPercentage"), color: MyColors.primary, size: 12),
                  MyText(title: "${getCouponModel?.discount}%", color: MyColors.black, size: 11),
                ],
              ),
              Column(
                children: [
                  MyText(title: tr(context, "endDate"), color: MyColors.primary, size: 12),
                  MyText(title: "${getCouponModel?.expireDate}", color: MyColors.black, size: 10),
                ],
              ),
            ],
          ),
          Divider(),
        ],
      )
    );
  }
}
