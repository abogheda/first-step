part of 'CouponsImports.dart';

class Coupons extends StatefulWidget {
  const Coupons({Key? key}) : super(key: key);

  @override
  _CouponsState createState() => _CouponsState();
}

class _CouponsState extends State<Coupons> {
  final CouponsData couponsData = CouponsData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(title: tr(context, "myCoupon")),
      backgroundColor: MyColors.greyWhite,
      body: GenericListView(
        padding: const EdgeInsets.all(20),
        cubit: couponsData.couponsCubit,
        emptyStr: tr(context,"noData"),
        onRefresh: couponsData.fetchCoupons,
        type: ListViewType.api ,
        params: [context],
        itemBuilder: (_,index,item){
          return  BuildCouponItem(getCouponModel: item,);
        },
      ),
    );
  }
}
