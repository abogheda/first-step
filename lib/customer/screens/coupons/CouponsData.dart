part of 'CouponsImports.dart';
class CouponsData{
  final GenericBloc<List<GetCouponModel>> couponsCubit = GenericBloc([]);

  fetchCoupons (BuildContext context,{bool refresh =true})async{
    var result = await CustomerRepository(context).getCoupons(refresh);
    couponsCubit.onUpdateData(result);
  }
}