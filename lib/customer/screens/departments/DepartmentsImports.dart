import 'package:base_flutter/customer/models/sub_category_model.dart';
import 'package:base_flutter/customer/screens/departments/widgets/DepartmentsWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/utils_functions/LoadingDialog.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../resources/customer_repository_imports.dart';
part 'Departments.dart';
part 'DepartmentsData.dart';