part of 'DepartmentsImports.dart';

class Departments extends StatefulWidget {
  final String title;
  final int catId;

  const Departments({required this.title,required this.catId});

  @override
  _DepartmentsState createState() => _DepartmentsState();
}

class _DepartmentsState extends State<Departments> {

  final DepartmentsData departmentsData = DepartmentsData();

  @override
  void initState() {
    departmentsData.fetchSubCategories(context,widget.catId ,refresh: false);
    departmentsData.fetchSubCategories(context,widget.catId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: MyColors.greyWhite,
        appBar: DefaultAppBar(title: widget.title),
        body: BlocBuilder<GenericBloc<List<SubCategoryModel>>, GenericState<List<SubCategoryModel>>>(
          bloc: departmentsData.subCategoriesCubit,
          builder: (context, state) {
            if(state is GenericUpdateState) {
              if(state.data.isNotEmpty) {
                return ListView.builder(
                  padding: const EdgeInsets.all(10),
                  itemBuilder: (_, index) {
                    return BuildDepartmentItem(subCategoryModel: state.data[index],);
                  }, itemCount: state.data.length,);
              }
              return Center(child: MyText(title: tr(context, "noData"),color: MyColors.primary,size: 12,),);
            }
            return LoadingDialog.showLoadingView();
          },
        )
    );
  }
}
