part of 'DepartmentsImports.dart';
class DepartmentsData{
  final GenericBloc<List<SubCategoryModel>> subCategoriesCubit = GenericBloc([]);

  fetchSubCategories (BuildContext context,int id,{bool refresh =true})async{
    var result = await CustomerRepository(context).getSubCategories(refresh,id);
    subCategoriesCubit.onUpdateData(result);
  }
}