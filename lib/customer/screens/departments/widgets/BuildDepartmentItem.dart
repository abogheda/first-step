part of 'DepartmentsWidgetsImports.dart';

class BuildDepartmentItem extends StatelessWidget {
final SubCategoryModel? subCategoryModel;

  const BuildDepartmentItem({Key? key, this.subCategoryModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      closedColor: Colors.transparent,
      openColor: Colors.transparent,
      closedElevation: 0,
      openElevation: 0,
      transitionDuration: Duration(milliseconds: 800),
      transitionType: ContainerTransitionType.fadeThrough,
      openBuilder: (_, action) => DepartmentsDetails(subCategoryId: subCategoryModel!.id,image: subCategoryModel?.image ?? "",
      title: subCategoryModel?.name,),
      closedBuilder: (_, action) => Container(
        margin: const EdgeInsets.symmetric(vertical: 5),
        child: CachedImage(
          url: subCategoryModel?.image ?? "",
          fit: BoxFit.fill,
          height: 160,
          alignment: Alignment.bottomCenter,
          borderRadius: BorderRadius.circular(10),
          child: MyText(
            title: "${subCategoryModel?.name}",
            color: MyColors.white,
            size: 15,
          ),
        ),
      ),
    );
  }
}
