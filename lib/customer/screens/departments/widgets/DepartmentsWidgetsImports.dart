import 'package:animations/animations.dart';
import 'package:base_flutter/customer/screens/department_details/DepartmentDetailsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../models/sub_category_model.dart';
part 'BuildDepartmentItem.dart';