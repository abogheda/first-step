part of 'EditProfileWidgetsImports.dart';

class BuildProfileImage extends StatelessWidget {
  final EditProfileData editProfileData;

  const BuildProfileImage({required this.editProfileData});

  @override
  Widget build(BuildContext context) {
    var user = context.watch<UserCubit>().state.model;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Stack(
          alignment: AlignmentDirectional.bottomCenter,
          children: [
            BlocBuilder<GenericBloc<File?>, GenericState<File?>>(
              bloc: editProfileData.imageCubit,
              builder: (context, state) {
                if (state.data != null) {
                  return Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                      color: MyColors.grey.withOpacity(0.7),
                      shape: BoxShape.circle,
                      border: Border.all(color: MyColors.grey),
                      image: DecorationImage(
                        image: FileImage(state.data!),
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                }
                return CachedImage(
                  url:user.image,
                  width: 100,
                  height: 100,
                  haveRadius: false,
                  boxShape: BoxShape.circle,
                  fit: BoxFit.cover,
                );
              },
            ),
            Positioned(
              right: 10,
              child: InkWell(
                onTap: () => editProfileData.setImage(),
                child: Container(
                  height: 30,
                  width: 30,
                  decoration: BoxDecoration(
                    color: MyColors.primary,
                    shape: BoxShape.circle,
                  ),
                  alignment: Alignment.center,
                  child: Icon(Icons.edit, color: MyColors.white, size: 15),
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 20),
      ],
    );
  }
}
