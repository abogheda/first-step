part of 'EditProfileWidgetsImports.dart';

class BuildProfileForm extends StatelessWidget {
  final EditProfileData editProfileData;

  const BuildProfileForm({required this.editProfileData});

  @override
  Widget build(BuildContext context) {
    return Form(
      key: editProfileData.formKey,
      child: Expanded(
        child: Container(
          margin: const EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: MyColors.white,
            borderRadius: BorderRadius.circular(5),
          ),
          child: ListView(
            padding: const EdgeInsets.all(20),
            children: [
              BuildProfileImage(editProfileData: editProfileData),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: InkWell(
                  onTap: () => showDialog(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      titlePadding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 8),
                      title: MyText(
                        title: tr(context,"wantDeleteAccount"),
                        color: MyColors.black,
                        size: 12,
                      ),
                      contentPadding:
                          const EdgeInsets.symmetric(horizontal: 10),
                      content: MyText(
                        title:
                            tr(context, "checkBeforeDelete"),
                        color: MyColors.blackOpacity,
                        size: 11,
                      ),
                      actionsPadding: const EdgeInsets.symmetric(horizontal: 5),
                      actions: [
                        TextButton(
                          onPressed: () => AutoRouter.of(context).pop(),
                          child: MyText(
                            title: tr(context,"cancel"),
                            color: MyColors.primary,
                            size: 12,
                          ),
                        ),
                        TextButton(
                          onPressed: () => editProfileData.deleteAcc(context),
                          child: MyText(
                            title: tr(context,"deleteAcc"),
                            color: MyColors.primary,
                            size: 12,
                          ),
                        ),
                      ],
                    ),
                  ),
                  child: Center(
                    child: MyText(
                      title: tr(context,"deleteAcc"),
                      color: MyColors.black,
                      size: 12,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
              ),
              GenericTextField(
                controller: editProfileData.phone,
                label: tr(context,"phone"),
                fieldTypes: FieldTypes.normal,
                type: TextInputType.phone,
                action: TextInputAction.next,
                margin: const EdgeInsets.symmetric(vertical: 10),
                validate: (value) => value!.validatePhone(context),
              ),
              GenericTextField(
                controller: editProfileData.name,
                label: tr(context,"name"),
                fieldTypes: FieldTypes.normal,
                type: TextInputType.text,
                margin: const EdgeInsets.symmetric(vertical: 10),
                action: TextInputAction.next,
                validate: (value) => value!.validateEmpty(context),
              ),
              GenericTextField(
                controller: editProfileData.email,
                label: tr(context,"mail"),
                fieldTypes: FieldTypes.normal,
                type: TextInputType.emailAddress,
                margin: const EdgeInsets.symmetric(vertical: 10),
                action: TextInputAction.next,
                validate: (value) => value!.validateEmail(context),
              ),
              BlocConsumer<LocationCubit, LocationState>(
                bloc: editProfileData.locationCubit,
                listener: (_, state) {
                  editProfileData.address.text = state.model?.address ?? "";
                },
                builder: (_, state) {
                  return GenericTextField(
                    label: tr(context,"address"),
                    controller: editProfileData.address,
                    onTab: () => editProfileData.onLocationClick(context),
                    fieldTypes: FieldTypes.clickable,
                    type: TextInputType.text,
                    action: TextInputAction.done,
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    validate: (value) => value!.validateEmpty(context),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
