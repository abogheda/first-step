part of 'EditProfileWidgetsImports.dart';

class BuildPasswordContent extends StatelessWidget {
  final EditProfileData editProfileData;

  const BuildPasswordContent({required this.editProfileData});

  @override
  Widget build(BuildContext context) {
    return Form(
      key: editProfileData.passwordFormKey,
      child: ListView(
        padding: const EdgeInsets.all(30),
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyText(
                  title: tr(context, "changeLang"), color: MyColors.black, size: 18),
              IconButton(
                onPressed: (){
                    editProfileData.oldPassword.clear();
                    editProfileData.newPassword.clear();
                    editProfileData.confirmPassword.clear();
                    AutoRouter.of(context).pop();
                },
                icon: Icon(MdiIcons.closeCircle, size: 30),
              )
            ],
          ),
          SizedBox(height: 10),
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
            bloc: editProfileData.passwordCubit,
            builder: (_, state) {
              return GenericTextField(
                label: tr(context, "oldPass"),
                fieldTypes:
                    state.data ? FieldTypes.normal : FieldTypes.password,
                type: TextInputType.text,
                action: TextInputAction.next,
                controller: editProfileData.oldPassword,
                margin: const EdgeInsets.symmetric(vertical: 10),
                validate: (value) => value!.validateEmpty(context),
                suffixIcon: IconButton(
                  onPressed: () =>
                      editProfileData.passwordCubit.onUpdateData(!state.data),
                  icon: Icon(
                    state.data ? Icons.visibility : Icons.visibility_off,
                  ),
                ),
              );
            },
          ),
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
            bloc: editProfileData.newPasswordCubit,
            builder: (_, state) {
              return GenericTextField(
                label: tr(context, "newPass"),
                fieldTypes:
                    state.data ? FieldTypes.normal : FieldTypes.password,
                type: TextInputType.text,
                action: TextInputAction.next,
                controller: editProfileData.newPassword,
                margin: const EdgeInsets.symmetric(vertical: 5),
                validate: (value) => value!.validateEmpty(context),
                suffixIcon: IconButton(
                  onPressed: () => editProfileData.newPasswordCubit
                      .onUpdateData(!state.data),
                  icon: Icon(
                    state.data ? Icons.visibility : Icons.visibility_off,
                  ),
                ),
              );
            },
          ),
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
            bloc: editProfileData.confirmPasswordCubit,
            builder: (_, state) {
              return GenericTextField(
                label: tr(context, "confirmPass"),
                fieldTypes:
                    state.data ? FieldTypes.normal : FieldTypes.password,
                type: TextInputType.text,
                action: TextInputAction.next,
                controller: editProfileData.confirmPassword,
                margin: const EdgeInsets.symmetric(vertical: 10),
                validate: (value) => value!.validatePasswordConfirm(context,pass: editProfileData.newPassword.text),
                suffixIcon: IconButton(
                  onPressed: () => editProfileData.confirmPasswordCubit
                      .onUpdateData(!state.data),
                  icon: Icon(
                    state.data ? Icons.visibility : Icons.visibility_off,
                  ),
                ),
              );
            },
          ),
          DefaultButton(title: "تأكيد", onTap: () {
            editProfileData.setChangePassword(context);
          })
        ],
      ),
    );
  }
}
