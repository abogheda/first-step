part of 'EditProfileWidgetsImports.dart';

class BuildProfileButtons extends StatelessWidget {
  final EditProfileData editProfileData;

  const BuildProfileButtons({required this.editProfileData});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: ()=>editProfileData.showPassword(context, editProfileData),
          child: MyText(
            title: tr(context,"changePassword"),
            color: MyColors.secondary,
            size: 15,
            decoration: TextDecoration.underline,
          ),
        ),
        DefaultButton(
          title: tr(context,"send"),
          onTap: () =>editProfileData.saveUserProfile(context),
          margin: const EdgeInsets.all(30),
        )
      ],
    );
  }
}
