part of 'EditProfileImports.dart';

class EditProfileData {
  final GlobalKey<FormState> formKey = GlobalKey();
  final GlobalKey<FormState> passwordFormKey = GlobalKey();

  final LocationCubit locationCubit = LocationCubit();
  final TextEditingController name = TextEditingController();
  final TextEditingController phone = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController address = TextEditingController();
  final TextEditingController password = TextEditingController();
  final TextEditingController oldPassword = TextEditingController();
  final TextEditingController newPassword = TextEditingController();
  final TextEditingController confirmPassword = TextEditingController();
  final GlobalKey<CustomButtonState> btnKey = GlobalKey<CustomButtonState>();
  final GenericBloc<File?> imageCubit = GenericBloc(null);
  final GenericBloc<bool> passwordCubit = GenericBloc(false);
  final GenericBloc<bool> newPasswordCubit = GenericBloc(false);
  final GenericBloc<bool> confirmPasswordCubit = GenericBloc(false);

  setImage() async {
    var image = await Utils.getImage();
    if (image != null) {
      imageCubit.onUpdateData(image);
    }
  }

  onLocationClick(BuildContext context) async {
    var loc = await Utils.getCurrentLocation(context);
    locationCubit.onLocationUpdated(
      LocationModel(
        lat: loc?.latitude ?? 24.774265,
        lng: loc?.longitude ?? 46.738586,
        address: "",
      ),
    );
    Navigator.of(context).push(
      CupertinoPageRoute(
        builder: (cxt) => BlocProvider.value(
          value: locationCubit,
          child: LocationAddress(),
        ),
      ),
    );
  }
  void initProfile(BuildContext context){
    UserModel user =  context.read<UserCubit>().state.model;
    name.text = user.name;
    email.text = user.email;
    phone.text = user.phone;
    address.text = user.address;
  }
  saveUserProfile(BuildContext context) async {
    FocusScope.of(context).requestFocus(FocusNode());
    // var user = context.read<UserCubit>().state.model;
    if (formKey.currentState!.validate()) {
      btnKey.currentState?.animateForward();
      ProfileModel model = ProfileModel(
        email: email.text,
        image: imageCubit.state.data,
        name: name.text,
        phone: phone.text,
        address: address.text,
        lat: locationCubit.state.model!.lat.toString(),
        lng: locationCubit.state.model!.lng.toString(),
      );
      var result = await CustomerRepository(context).updateUserProfile(model);
      if (result) {
        btnKey.currentState?.animateReverse();
        Navigator.of(context).pop();
      }
      btnKey.currentState?.animateReverse();
    }
  }

  void setChangePassword(BuildContext context) async {
    if (passwordFormKey.currentState!.validate()) {
      btnKey.currentState?.animateForward();
      var result = await CustomerRepository(context).updateUserPassword(
          newPassword.text,
          oldPassword.text,
          confirmPassword.text);
      if (result) {
        oldPassword.clear();
        newPassword.clear();
        confirmPassword.clear();
        Navigator.of(context).pop();
        btnKey.currentState?.animateReverse();
      }
      btnKey.currentState?.animateReverse();
    }
  }

  void showPassword(BuildContext context, EditProfileData editProfileData) {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return BuildPasswordContent(editProfileData: editProfileData);
      },
    );
  }

  deleteAcc(BuildContext context) async{
    var data = await CustomerRepository(context).deleteUser();
    if(data){
      CustomToast.showSimpleToast(msg: tr(context, "deleteDone"));
    }
  }
}
