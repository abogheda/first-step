
import 'dart:io';

import 'package:base_flutter/customer/screens/edit_porfile/widgets/EditProfileWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/models/LocationModel.dart';
import 'package:base_flutter/general/screens/location_address/LocationAddressImports.dart';
import 'package:base_flutter/general/screens/location_address/location_cubit/location_cubit.dart';
import 'package:base_flutter/general/utilities/utils_functions/UtilsImports.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:dio_helper/dio_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../../general/blocks/user_cubit/user_cubit.dart';
import '../../../general/models/UserModel.dart';
import '../../models/dots/profile_model.dart';
import '../../resources/customer_repository_imports.dart';
part 'EditProfile.dart';
part 'EditProfileData.dart';