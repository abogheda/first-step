part of 'EditProfileImports.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final EditProfileData editProfileData = EditProfileData();

  @override
  void initState() {
    editProfileData.initProfile(context);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.greyWhite,
      appBar: DefaultAppBar(title: tr(context, "profile")),
      body: Column(
        children: [
          BuildProfileForm(editProfileData: editProfileData),
          BuildProfileButtons(editProfileData: editProfileData),
        ],
      ),
    );
  }
}
