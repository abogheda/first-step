part of 'ConfirmPasswordImports.dart';

class ConfirmPasswordData {
  final GlobalKey<ScaffoldState> scaffold = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final TextEditingController code = new TextEditingController();
  final GenericBloc<bool> checkTimer = GenericBloc(false);

  final GenericBloc<Duration> durationCubit = GenericBloc(Duration());

  Duration countdownDuration = Duration(minutes: 0, seconds: 59);
  Timer? timer;

  bool countDown = true;

  void reset() {
    if (countDown) {
      durationCubit.onUpdateData(countdownDuration);
    } else {
      durationCubit.onUpdateData(Duration());
    }
  }

  void startTimer() {
    timer = Timer.periodic(Duration(seconds: 1), (_) => addTime());
  }

  void addTime() {
    final addSeconds = countDown ? -1 : 1;
    final seconds = durationCubit.state.data.inSeconds + addSeconds;
    if (seconds < 0) {
      timer?.cancel();
      checkTimer.onUpdateData(false);
    } else {
      durationCubit.onUpdateData(Duration(seconds: seconds));
    }
  }

  void onResendCode(BuildContext context, String userId) async {
    // await GeneralRepository(context).resendCode(userId);
    startTimer();
    checkTimer.onUpdateData(true);
    reset();
  }
}
