part of 'ConfirmPasswordImports.dart';

class ConfirmPassword extends StatefulWidget {
  @override
  _ConfirmPasswordState createState() => _ConfirmPasswordState();
}

class _ConfirmPasswordState extends State<ConfirmPassword> {
  final ConfirmPasswordData confirmPasswordData = new ConfirmPasswordData();

  @override
  void initState() {
    super.initState();
    confirmPasswordData.startTimer();
    confirmPasswordData.checkTimer.onUpdateData(true);
    confirmPasswordData.reset();
  }
  @override
  Widget build(BuildContext context) {
    return AuthScaffold(
      child: ListView(
        padding: const EdgeInsets.symmetric(vertical: 40,horizontal: 20),
        physics: BouncingScrollPhysics(
          parent: AlwaysScrollableScrollPhysics(),
        ),
        children: [
          BuildText(),
          BuildFormInputs(confirmPasswordData: confirmPasswordData),
          BuildButtonList(
            confirmPasswordData: confirmPasswordData,
            userId: "",
          ),
        ],
      ),
    );
  }
}
