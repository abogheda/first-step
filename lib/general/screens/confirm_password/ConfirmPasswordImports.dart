import 'dart:async';

import 'package:base_flutter/general/screens/confirm_password/widgets/ConfirmPasswordWidgetsImports.dart';
import 'package:base_flutter/general/widgets/AuthScaffold.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';

part 'ConfirmPassword.dart';

part 'ConfirmPasswordData.dart';
