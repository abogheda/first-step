part of 'ConfirmPasswordWidgetsImports.dart';
class BuildTimeCard extends StatelessWidget {
  final String time;

  const BuildTimeCard({required this.time});

  @override
  Widget build(BuildContext context) {
    return  MyText(
      title: time,
      color: MyColors.secondary,
      size: 13,
      fontWeight: FontWeight.w600,
    );
  }
}
