import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/screens/confirm_password/ConfirmPasswordImports.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:tf_validator/tf_validator.dart';

part 'BuildText.dart';

part 'BuildFormInputs.dart';

part 'BuildButtonList.dart';
part 'BuildTime.dart';
part 'BuildTimeCard.dart';
