part of 'ConfirmPasswordWidgetsImports.dart';

class BuildButtonList extends StatelessWidget {
  final ConfirmPasswordData confirmPasswordData;
  final String userId;

  const BuildButtonList(
      {required this.confirmPasswordData, required this.userId});

  @override
  Widget build(BuildContext context) {
    return    BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
      bloc: confirmPasswordData.checkTimer,
      builder: (_, state) {
        Widget checkWidget = Container();
        if (state.data == true) {
          checkWidget = BuildTime(confirmPasswordData: confirmPasswordData);
        } else {
          checkWidget = Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MyText(
                title: tr(context, "noReceiveCode"),
                size: 14,
                color: MyColors.grey,
              ),
              InkWell(
                onTap: () =>
                    confirmPasswordData.onResendCode(context, userId),
                child: MyText(
                  title: tr(context, "sendCode"),
                  size: 14,
                  color: MyColors.primary,
                  decoration: TextDecoration.underline,
                ),
              ),
            ],
          );
        }
        return Column(
          children: [
            checkWidget,
            DefaultButton(
              title: tr(context, "confirm"),
              onTap: () =>
                  AutoRouter.of(context).push(
                      ResetPasswordRoute(phone: '')),
              color: MyColors.primary,
              margin: const EdgeInsets.all(20),
            ),
          ],
        );
      },
    );
  }
}
