part of 'SelectUserWidgetsImports.dart';

class BuildUserText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.symmetric(vertical: 30),
      child: MyText(
        title:tr(context,"selectAccount"),
        size: 17,
        color: MyColors.black,
      ),
    );
  }
}
