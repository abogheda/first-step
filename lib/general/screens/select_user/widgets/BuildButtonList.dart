part of 'SelectUserWidgetsImports.dart';

class BuildButtonList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        DefaultButton(
          title: "مستخدم",
          onTap: () => AutoRouter.of(context).push(LoginRoute()),
          margin: const EdgeInsets.symmetric(vertical: 10),
          color: MyColors.primary,
        ),
        DefaultButton(
          title: "مقدم خدمة",
          onTap: () {
            AutoRouter.of(context).push(ProviderWebViewRoute());
          },
          margin: const EdgeInsets.symmetric(vertical: 10),
          color: MyColors.secondary,
          borderColor: MyColors.secondary,
          textColor: MyColors.white,
        ),
      ],
    );
  }
}
