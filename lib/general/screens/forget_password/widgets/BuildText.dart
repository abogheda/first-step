part of 'ForgetPasswordWidgetsImports.dart';

class BuildText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        MyText(
          title: tr(context,"forgetPassword"),
          size: 17,
          color: MyColors.black,
        ),
        MyText(
          title: tr(context,"insertPhone"),
          size: 14,
          color: MyColors.grey,
        ),
      ],
    );
  }
}
