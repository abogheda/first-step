import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/tf_validator.dart';

import '../../constants/MyColors.dart';
import '../../models/intro_model.dart';

import '../../resources/GeneralRepoImports.dart';
import '../../utilities/utils_functions/LoadingDialog.dart';

part 'onBoarding.dart';

part 'OnBoardingData.dart';