part of 'OnBoardingImports.dart';

class OnBoardingData {
  final GenericBloc<List<IntroModel>> onBoardingCubit =
      GenericBloc<List<IntroModel>>([]);

  //final GenericBloc<bool> buttonLoading = GenericBloc<bool>(false);

  Future<void> fetchData(BuildContext context) async {
    var data = await GeneralRepository(context).intros();
    onBoardingCubit.onUpdateData(data);
  }

  Future<bool> onBackPressed() async {
    SystemNavigator.pop();
    return true;
  }
}
