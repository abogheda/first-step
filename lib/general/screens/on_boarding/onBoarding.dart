part of 'OnBoardingImports.dart';

class OnBoarding extends StatefulWidget {
  const OnBoarding({Key? key}) : super(key: key);

  @override
  State<OnBoarding> createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {
  final OnBoardingData onBoardingData = OnBoardingData();
  final _controller = PageController();
  int sliderPage = 0;

  @override
  void initState() {
    onBoardingData.fetchData(context);
    super.initState();
    _controller.addListener(() {
      setState(() {
        sliderPage = _controller.page!.round();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<GenericBloc<List<IntroModel>>,
              GenericState<List<IntroModel>>>(
          bloc: onBoardingData.onBoardingCubit,
          builder: (context, state) {
            if (state is GenericUpdateState) {
              return PageView.builder(
                itemCount: state.data.length,
                controller: _controller,
                itemBuilder: (context, index) {
                  return Stack(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(
                              state.data[index].image ?? "",
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: state.data.map((url) {
                                int index = state.data.indexOf(url);
                                return Container(
                                  width: sliderPage == index ? 20 : 5,
                                  height: 7,
                                  margin: EdgeInsets.symmetric(
                                      vertical: 2.0, horizontal: 2.0),
                                  decoration: BoxDecoration(
                                    borderRadius: sliderPage == index
                                        ? BorderRadius.circular(20)
                                        : BorderRadius.circular(50),
                                    color: sliderPage == index
                                        ? MyColors.primary
                                        : MyColors.white,
                                  ),
                                );
                              }).toList(),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.7,
                              child: Center(
                                  child: Stack(
                                children: <Widget>[
                                  // Stroked text as border.
                                  Text(
                                    state.data[index].description!,
                                    style: TextStyle(
                                        fontSize: 16,
                                        foreground: Paint()
                                          ..style = PaintingStyle.stroke
                                          ..strokeWidth = 4
                                          ..color = MyColors.primary),
                                    textAlign: TextAlign.center,
                                  ),
                                  // Solid text as fill.
                                  Text(
                                    state.data[index].description!,
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: MyColors.white,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              )),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.7,
                              child: Center(
                                  child: Stack(
                                children: <Widget>[
                                  // Stroked text as border.
                                  Text(
                                    state.data[index].description!,
                                    style: TextStyle(
                                        fontSize: 16,
                                        foreground: Paint()
                                          ..style = PaintingStyle.stroke
                                          ..strokeWidth = 4
                                          ..color = MyColors.primary),
                                    textAlign: TextAlign.center,
                                  ),
                                  // Solid text as fill.
                                  Text(
                                    state.data[index].description!,
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: MyColors.white,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              )),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            InkWell(
                              onTap: ()=>AutoRouter.of(context).push(SelectUserRoute()),
                              child: Align(
                                  alignment: Alignment.bottomCenter,
                                  child: MyText(
                                      title: tr(context, "skip"),
                                      color: MyColors.primary,
                                      decoration: TextDecoration.underline,
                                      size: 12)),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.13,
                              child: DefaultButton(
                                margin: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 30),
                                title: index + 1 == state.data.length
                                    ? "${tr(context, 'start')}"
                                    : "${tr(context, 'next')}",
                                onTap: () async {
                                  if (index + 1 == state.data.length) {
                                    //  onBoardingData.buttonLoading.onUpdateData(true);
                                    SharedPreferences prefs =
                                        await SharedPreferences.getInstance();
                                    //FlutterAppBadger.removeBadge();
                                    //Utils.manipulateSplashData(context);
                                    AutoRouter.of(context).pushAndPopUntil(
                                        SelectUserRoute(),
                                        predicate: (value) => false);
                                    await prefs.setBool('firstTime', false);
                                  } else {
                                    _controller.nextPage(
                                        duration: Duration(milliseconds: 300),
                                        curve: Curves.ease);
                                  }
                                },
                                color: MyColors.primary,
                                textColor: MyColors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                },
              );
            }
            return LoadingDialog.showLoadingView();
          }),
    );
  }
}
