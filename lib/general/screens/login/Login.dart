part of 'LoginImports.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  LoginData loginData = new LoginData();

  @override
  Widget build(BuildContext context) {
    return AuthScaffold(
      child: ListView(
        padding: const EdgeInsets.all(20),
        physics: BouncingScrollPhysics(
          parent: AlwaysScrollableScrollPhysics(),
        ),
        children: [
          BuildText(),
          BuildFormInputs(loginData: loginData),
          BuildForgetText(),
          BuildLoginButton(loginData: loginData),
          BuildNewRegister(),
          BuildVisitor(),
        ],
      ),
    );
  }

  @override
  void dispose() {
    loginData.phone.dispose();
    loginData.password.dispose();
    super.dispose();
  }
}
