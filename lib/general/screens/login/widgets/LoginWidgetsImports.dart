import 'package:auto_route/auto_route.dart';
import 'package:dio_helper/utils/GlobalState.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:flutter/material.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/tf_validator.dart';
import '../LoginImports.dart';



part 'BuildFormInputs.dart';

part 'BuildForgetText.dart';

part 'BuildLoginButton.dart';

part 'BuildNewRegister.dart';

part 'BuildText.dart';

part 'BuildVisitor.dart';
