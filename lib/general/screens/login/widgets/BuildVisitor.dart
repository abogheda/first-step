part of 'LoginWidgetsImports.dart';

class BuildVisitor extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        AutoRouter.of(context).push((HomeRoute()));
        GlobalState.instance.set("visitor", true);
      },
      child: MyText(
        title:tr(context, "visitor"),
        size: 14,
        alien: TextAlign.center,
        color: MyColors.secondary,
        decoration: TextDecoration.underline,
      ),
    );
  }
}
