part of 'LoginWidgetsImports.dart';

class BuildNewRegister extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: InkWell(
        onTap: () => AutoRouter.of(context).push(RegisterRoute()),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MyText(
              title: tr(context, "don'tHaveAccount"),
              size: 14,
              color: MyColors.black,
            ),
            SizedBox(width: 5),
            MyText(
              title: "تسجيل جديد",
              size: 14,
              color: MyColors.secondary,
            ),
          ],
        ),
      ),
    );
  }
}
