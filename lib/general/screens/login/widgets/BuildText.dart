part of 'LoginWidgetsImports.dart';

class BuildText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        MyText(
          title: tr(context,"login"),
          size: 17,
          color: MyColors.black,
        ),
        MyText(
          title: "من فضلك قم بتسجيل الدخول",
          size: 14,
          color: MyColors.grey,
        ),
      ],
    );
  }
}
