part of 'LoginWidgetsImports.dart';

class BuildFormInputs extends StatelessWidget {
  final LoginData loginData;

  const BuildFormInputs({required this.loginData});

  @override
  Widget build(BuildContext context) {
    return Form(
      key: loginData.formKey,
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GenericTextField(
              fieldTypes: FieldTypes.normal,
              label: tr(context, "phone"),
              controller: loginData.phone,
              margin: const EdgeInsets.symmetric(vertical: 10),
              action: TextInputAction.next,
              type: TextInputType.phone,
              validate: (value) => value!.validateEmpty(context),
            ),
            BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
              bloc: loginData.showPassword,
              builder: (context, state) {
                return GenericTextField(
                  suffixIcon: IconButton(
                    icon: Icon(state.data ? Icons.visibility_off : Icons.visibility),
                    onPressed: () => loginData.showPassword.onUpdateData(!state.data),
                  ),
                  fieldTypes:
                      state.data ? FieldTypes.password : FieldTypes.normal,
                  label: tr(context, "password"),
                  controller: loginData.password,
                  validate: (value) => value!.validateEmpty(context),
                  type: TextInputType.text,
                  action: TextInputAction.done,
                  onSubmit: () => loginData.userLogin(context),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
