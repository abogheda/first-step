part of 'ActiveAccountWidgetsImports.dart';

class BuildText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        MyText(
          title:tr(context,"activeAccount"),
          size: 17,
          color: MyColors.black,
        ),
        MyText(
          title: tr(context,"codeSendToPhone"),
          size: 13,
          color: MyColors.grey,
        ),
      ],
    );
  }
}
