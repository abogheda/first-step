part of 'ActiveAccountWidgetsImports.dart';

class BuildButtonList extends StatelessWidget {
  final ActiveAccountData activeAccountData;
  final String phone;

  const BuildButtonList({required this.activeAccountData,required this.phone});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
      bloc: activeAccountData.checkTimer,
      builder: (_, state) {
        Widget checkWidget = Container();
        if (state.data == true) {
          checkWidget = BuildTime(activeAccountData: activeAccountData);
        } else {
          checkWidget = Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MyText(
                title: tr(context, "noReceiveCode"),
                size: 14,
                color: MyColors.grey,
              ),
              InkWell(
                onTap: () =>
                    activeAccountData.onResendCode(context, phone),
                child: MyText(
                  title: tr(context, "sendCode"),
                  size: 14,
                  color: MyColors.primary,
                  decoration: TextDecoration.underline,
                ),
              ),
            ],
          );
        }
        return Column(
          children: [
            checkWidget,
            DefaultButton(
              title: tr(context, "confirm"),
              onTap: ()=>activeAccountData.onActiveAccount(context,phone),
              color: MyColors.primary,
              margin: const EdgeInsets.all(20),
            ),
          ],
        );
      },
    );
  }
}
