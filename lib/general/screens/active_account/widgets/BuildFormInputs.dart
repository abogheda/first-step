part of 'ActiveAccountWidgetsImports.dart';

class BuildFormInputs extends StatelessWidget {
  final ActiveAccountData activeAccountData;

  const BuildFormInputs({required this.activeAccountData});

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: Form(
        key: activeAccountData.formKey,
        child: Padding(
          padding: const EdgeInsets.all(25),
          child: PinCodeTextField(
            length: 4,
            appContext: context,
            onChanged: (String value) {},
            backgroundColor: Colors.transparent,
            animationType: AnimationType.fade,
            pinTheme: PinTheme(
                shape: PinCodeFieldShape.box,
                // borderRadius: BorderRadius.circular(5),
                fieldHeight: 45,
                fieldWidth: 45,
                inactiveColor: MyColors.grey,
                activeColor: MyColors.primary,
                selectedFillColor: MyColors.white,
                selectedColor: MyColors.blackOpacity,
                inactiveFillColor: MyColors.white,
                activeFillColor: MyColors.white,
                disabledColor: MyColors.black),
            animationDuration: Duration(milliseconds: 300),
            enableActiveFill: true,
            controller: activeAccountData.code,
          ),
        ),
      ),
    );
  }
}
