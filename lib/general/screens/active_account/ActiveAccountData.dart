part of 'ActiveAccountImports.dart';

class ActiveAccountData {
  GlobalKey<ScaffoldState> scaffold = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<CustomButtonState> btnKey =
      new GlobalKey<CustomButtonState>();

  final TextEditingController code = new TextEditingController();
  final GenericBloc<bool> checkTimer = GenericBloc(false);

  final GenericBloc<Duration> durationCubit = GenericBloc(Duration());

  Duration countdownDuration = Duration(minutes: 0, seconds: 59);
  Timer? timer;

  bool countDown = true;

  void reset() {
    if (countDown) {
      durationCubit.onUpdateData(countdownDuration);
    } else {
      durationCubit.onUpdateData(Duration());
    }
  }

  void startTimer() {
    timer = Timer.periodic(Duration(seconds: 1), (_) => addTime());
  }

  void addTime() {
    final addSeconds = countDown ? -1 : 1;
    final seconds = durationCubit.state.data.inSeconds + addSeconds;
    if (seconds < 0) {
      timer?.cancel();
      checkTimer.onUpdateData(false);
    } else {
      durationCubit.onUpdateData(Duration(seconds: seconds));
    }
  }

  void onResendCode(BuildContext context, String phone) async {
    await GeneralRepository(context).resendCode(phone);
    startTimer();
    checkTimer.onUpdateData(true);
    reset();
  }

  void onActiveAccount(BuildContext context, String phone) async {
    if (formKey.currentState!.validate()) {
      btnKey.currentState?.animateForward();
      var result = await GeneralRepository(context).sendCode(code.text, phone);
      btnKey.currentState?.animateReverse();
      if (result) {
        AutoRouter.of(context).push(LoginRoute());
      }
    }
  }
}
