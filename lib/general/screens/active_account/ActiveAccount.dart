part of 'ActiveAccountImports.dart';

class ActiveAccount extends StatefulWidget {
  final String phone;

  const ActiveAccount({required this.phone});

  @override
  _ActiveAccountState createState() => _ActiveAccountState();
}

class _ActiveAccountState extends State<ActiveAccount> {
 final ActiveAccountData activeAccountData = new ActiveAccountData();

  @override
  void initState() {
    super.initState();
    activeAccountData.startTimer();
    activeAccountData.checkTimer.onUpdateData(true);
    activeAccountData.reset();
  }
  @override
  Widget build(BuildContext context) {
    return AuthScaffold(
      child: ListView(
        padding: const EdgeInsets.symmetric(vertical: 40,horizontal: 20),
        physics: BouncingScrollPhysics(
          parent: AlwaysScrollableScrollPhysics(),
        ),
        children: [
          BuildText(),
          BuildFormInputs(activeAccountData: activeAccountData),
          BuildButtonList(activeAccountData: activeAccountData,phone: widget.phone,),
        ],
      ),
    );
  }
}
