part of 'provider_webview_imports.dart';

class ProviderWebView extends StatefulWidget {
  const ProviderWebView({Key? key}) : super(key: key);

  @override
  State<ProviderWebView> createState() => _ProviderWebViewState();
}

class _ProviderWebViewState extends State<ProviderWebView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "مقدم الخدمة",
      ),
      body: WebView(
        initialUrl: "https://firststep1.net/",
      ),
    );
  }
}
