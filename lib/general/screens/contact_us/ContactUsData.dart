part of 'ContactUsImports.dart';

class ContactUsData{
  final GlobalKey<FormState> formKey = GlobalKey();
  final TextEditingController name = TextEditingController();
  final TextEditingController phone = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController subject = TextEditingController();

  sendComplaint (BuildContext context)async{
    FocusScope.of(context).requestFocus(FocusNode());
    if(formKey.currentState!.validate()){
      var result = await GeneralRepository(context).addComplaint(name.text,phone.text ,email.text, subject.text) ;
      if(result){

        email.clear();
        name.clear();
        phone.clear();
        subject.clear();
        Navigator.pop(context);
      }

    }

  }


}