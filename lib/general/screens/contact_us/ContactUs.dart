part of 'ContactUsImports.dart';

class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  final ContactUsData contactUsData = new ContactUsData();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        appBar: DefaultAppBar(
          title: tr(context, "contactUs"),
        ),
        body: BuildContactInputs(
          contactUsData: contactUsData,
        ),
        bottomNavigationBar: DefaultButton(
          onTap: () => contactUsData.sendComplaint(context),
          title: tr(context, "send"),
        ),
      ),
    );
  }
}
