part of 'ContactUsWidgetsImports.dart';


class BuildContactInputs extends StatelessWidget {
final ContactUsData contactUsData;

  const BuildContactInputs({Key? key,required this.contactUsData}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Form(
        key: contactUsData.formKey,
        child: ListView(
          children: [
            GenericTextField(
              margin: const EdgeInsets.symmetric(vertical: 10),
              label: tr(context, "name"),
              fieldTypes: FieldTypes.normal,
              type: TextInputType.text,
              controller: contactUsData.name,
              action: TextInputAction.next,
              validate: (value) => value!.validateEmpty(context),
            ),
            GenericTextField(
              label: tr(context, "phone"),
              fieldTypes: FieldTypes.normal,
              type: TextInputType.phone,
              margin: const EdgeInsets.symmetric(vertical: 10),
              controller: contactUsData.phone,
              action: TextInputAction.next,
              validate: (value) => value!.validatePhone(context),
            ),
            GenericTextField(
              margin: const EdgeInsets.symmetric(vertical: 10),
              label:tr(context, "mail"),
              fieldTypes: FieldTypes.normal,
              type: TextInputType.emailAddress,
              controller: contactUsData.email,
              action: TextInputAction.next,
              validate: (value) => value!.validateEmail(context),
            ),
            GenericTextField(
              margin: const EdgeInsets.symmetric(vertical: 10),
              max: 3,
              label:tr(context, "subject"),
              fieldTypes: FieldTypes.rich,
              type: TextInputType.text,
              controller: contactUsData.subject,
              action: TextInputAction.done,
              validate: (value) => value!.validateEmpty(context),
            ),
          ],
        )
      ),
    );
  }
}
