import 'package:base_flutter/general/screens/contact_us/widgets/ContactUsWidgetsImports.dart';

import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';

import 'package:tf_custom_widgets/widgets/DefaultButton.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../resources/GeneralRepoImports.dart';

part 'ContactUsData.dart';

part 'ContactUs.dart';
