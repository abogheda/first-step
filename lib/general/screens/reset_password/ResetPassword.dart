part of 'ResetPasswordImports.dart';

class ResetPassword extends StatefulWidget {
  final String phone;

  const ResetPassword({required this.phone});

  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  ResetPasswordData resetPasswordData = ResetPasswordData();

  @override
  Widget build(BuildContext context) {
    return AuthScaffold(
      child: ListView(
        padding: const EdgeInsets.all(20),
        physics: BouncingScrollPhysics(
          parent: AlwaysScrollableScrollPhysics(),
        ),
        children: [
          BuildText(),
          BuildFormInputs(
            resetPasswordData: resetPasswordData,
          ),
          BuildButton(
            resetPasswordData: resetPasswordData,
            phone: widget.phone,
          ),
        ],
      ),
    );
  }
}
