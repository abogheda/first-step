part of 'ResetPasswordWidgetsImports.dart';

class BuildFormInputs extends StatelessWidget {
  final ResetPasswordData resetPasswordData;

  const BuildFormInputs({required this.resetPasswordData});

  @override
  Widget build(BuildContext context) {
    return Form(
      key: resetPasswordData.formKey,
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GenericTextField(
              fieldTypes: FieldTypes.normal,
              label: tr(context,"code"),
              controller: resetPasswordData.code,
              action: TextInputAction.next,
              type: TextInputType.number,
              margin: const EdgeInsets.symmetric(vertical: 20),
              validate: (value) => value!.validateEmpty(context),
            ),
            BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
              bloc: resetPasswordData.showPassword,
              builder: (context, state) {
                return GenericTextField(
                  suffixIcon: IconButton(
                    icon: Icon(
                      state.data ? Icons.visibility_off : Icons.visibility,
                    ),
                    onPressed: () => resetPasswordData.showPassword
                        .onUpdateData(!state.data),
                  ),
                  fieldTypes:
                  state.data ? FieldTypes.password : FieldTypes.normal,
                  label: tr(context, "newPassword"),
                  controller: resetPasswordData.newPassword,
                  validate: (value) => value!.validatePassword(context),
                  type: TextInputType.text,
                  action: TextInputAction.next,
                );
              },
            ),
            BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
              bloc: resetPasswordData.showConfirmPassword,
              builder: (context, state) {
                return GenericTextField(
                  suffixIcon: IconButton(
                    icon: Icon(
                      state.data ? Icons.visibility_off : Icons.visibility,
                    ),
                    onPressed: () => resetPasswordData.showConfirmPassword
                        .onUpdateData(!state.data),
                  ),
                  fieldTypes:
                  state.data ? FieldTypes.password : FieldTypes.normal,
                  label:tr(context,"confirmPassword"),
                  margin: const EdgeInsets.symmetric(vertical: 10),
                  controller: resetPasswordData.confirmNewPassword,
                  validate: (value) => value!.validatePasswordConfirm(context,
                      pass: resetPasswordData.newPassword.text),
                  type: TextInputType.text,
                  action: TextInputAction.done,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
