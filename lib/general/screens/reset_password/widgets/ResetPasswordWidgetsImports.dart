
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:flutter/material.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/screens/reset_password/ResetPasswordImports.dart';
import 'package:tf_validator/tf_validator.dart';

import '../../active_account/widgets/ActiveAccountWidgetsImports.dart';

part 'BuildFormInputs.dart';

part 'BuildText.dart';

part 'BuildButton.dart';

part 'BuildResetTime.dart';