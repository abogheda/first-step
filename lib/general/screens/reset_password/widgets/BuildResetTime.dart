part of 'ResetPasswordWidgetsImports.dart';

class BuildResetTime extends StatelessWidget {
  final ResetPasswordData resetPasswordData;

  const BuildResetTime({required this.resetPasswordData});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<Duration>, GenericState<Duration>>(
      bloc: resetPasswordData.durationCubit,
      builder: (_, state) {
        String twoDigits(int n) => n.toString().padLeft(2, '0');
        final minutes = twoDigits(state.data.inMinutes.remainder(60));
        final seconds = twoDigits(state.data.inSeconds.remainder(60));
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MyText(
              title: "الوقت المتبقي : ",
              size: 14,
              color: MyColors.grey,
            ),
            BuildTimeCard(time: seconds),
            MyText(
              title: " : ",
              size: 14,
              color: MyColors.secondary,
            ),
            BuildTimeCard(time: minutes),
          ],
        );
      },
    );
  }
}
