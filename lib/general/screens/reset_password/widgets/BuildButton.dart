part of 'ResetPasswordWidgetsImports.dart';

class BuildButton extends StatelessWidget {
  final ResetPasswordData resetPasswordData;
  final String phone;

  const BuildButton({required this.resetPasswordData, required this.phone});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
      bloc: resetPasswordData.checkTimer,
      builder: (_, state) {
        Widget checkWidget = Container();
        if (state.data == true) {
          checkWidget = BuildResetTime(resetPasswordData: resetPasswordData);
        } else {
          checkWidget = Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MyText(
                title: tr(context, "noReceiveCode"),
                size: 14,
                color: MyColors.grey,
              ),
              InkWell(
                onTap: () =>
                    resetPasswordData.onResendCode(context, phone),
                child: MyText(
                  title: tr(context, "sendCode"),
                  size: 14,
                  color: MyColors.primary,
                  decoration: TextDecoration.underline,
                ),
              ),
            ],
          );
        }
        return Column(
          children: [
            checkWidget,
            DefaultButton(
              title: tr(context, "confirm"),
              onTap: ()=>resetPasswordData.onResetPassword(context,phone),
              color: MyColors.primary,
              margin: const EdgeInsets.all(20),
            ),
          ],
        );
      },
    );

  }
}
