part of 'ResetPasswordImports.dart';

class ResetPasswordData {
  GlobalKey<ScaffoldState> scaffold = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<CustomButtonState> btnKey = new GlobalKey<CustomButtonState>();
  final TextEditingController newPassword = new TextEditingController();
  final TextEditingController confirmNewPassword = new TextEditingController();
  final TextEditingController code = new TextEditingController();
  final GenericBloc<bool> showPassword = new GenericBloc(true);
  final GenericBloc<bool> showConfirmPassword = new GenericBloc(true);

  final GenericBloc<bool> checkTimer = GenericBloc(false);

  final GenericBloc<Duration> durationCubit = GenericBloc(Duration());
  Duration countdownDuration = Duration(minutes: 0, seconds: 59);
  Timer? timer;

  bool countDown = true;


  void startTimer() {
    timer = Timer.periodic(Duration(seconds: 1), (_) => addTime());
  }

  void addTime() {
    final addSeconds = countDown ? -1 : 1;
    final seconds = durationCubit.state.data.inSeconds + addSeconds;
    if (seconds < 0) {
      timer?.cancel();
      checkTimer.onUpdateData(false);
    } else {
      durationCubit.onUpdateData(Duration(seconds: seconds));
    }
  }

  void reset() {
    if (countDown) {
      durationCubit.onUpdateData(countdownDuration);
    } else {
      durationCubit.onUpdateData(Duration());
    }
  }

  void onResetPassword(BuildContext context, String phone) async {
    FocusScope.of(context).requestFocus(FocusNode());
    if (formKey.currentState!.validate()) {
      bool result = await GeneralRepository(context).resetUserPassword(phone, code.text, newPassword.text);
      if (result) {
        AutoRouter.of(context).push(LoginRoute());
      }
    }
  }
  void onResendCode(BuildContext context, String phone) async {
    await GeneralRepository(context).resendCode(phone);
    startTimer();
    checkTimer.onUpdateData(true);
    reset();
  }

}
