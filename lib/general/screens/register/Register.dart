part of 'RegisterImports.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final RegisterData registerData = RegisterData();

  @override
  Widget build(BuildContext context) {
    return AuthScaffold(
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: ListView(
          padding: const EdgeInsets.all(20),
          children: [
            BuildRegisterText(),
            BuildRegisterForm(registerData: registerData),
            BuildCheckTerms(registerData: registerData),
            BuildRegisterButtons(registerData:registerData),
          ],
        ),
      ),
    );
  }
}
