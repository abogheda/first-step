part of 'RegisterImports.dart';

class RegisterData {
  final GlobalKey<FormState> formKey = GlobalKey();
  final TextEditingController name = TextEditingController();
  final TextEditingController phone = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController address = TextEditingController();
  final TextEditingController password = TextEditingController();
  final TextEditingController confirmPassword = TextEditingController();
  final GenericBloc<bool> showPassCubit = GenericBloc(false);
  final GenericBloc<bool> showConfirmPassCubit = GenericBloc(false);
  final GenericBloc<bool> checkTermCubit = GenericBloc(false);
  final LocationCubit locationCubit = new LocationCubit();
  final GlobalKey<CustomButtonState> btnKey =
  new GlobalKey<CustomButtonState>();
  onLocationClick(BuildContext context) async {
    LoadingDialog.showLoadingDialog();
    var _loc = await Utils.getCurrentLocation(context);
    locationCubit.onLocationUpdated(LocationModel(
      lat: _loc?.latitude ?? 24.774265,
      lng: _loc?.longitude ?? 46.738586,
      address: "",
    ));
    EasyLoading.dismiss();

    Navigator.of(context).push(
      CupertinoPageRoute(
        builder: (cxt) => BlocProvider.value(
          value: locationCubit,
          child: LocationAddress(),
        ),
      ),
    );
  }
  register(BuildContext context) async {
    // FirebaseMessaging messaging = FirebaseMessaging.instance;

    if (formKey.currentState!.validate()) {
      if (phone.text.length < 9) {
        CustomToast.showSimpleToast(msg: tr(context, "checkPhoneNumber"));
        return;
      }
      if (!RegExp(r'\S+@\S+\.\S+').hasMatch(email.text)) {
        CustomToast.showSimpleToast(msg: tr(context, "checkEmail"));
        return;
      }
      if (checkTermCubit.state.data != true) {
        CustomToast.showSimpleToast(msg: tr(context, "checkTerms"));
        return;
      }
      RegisterModel model = new RegisterModel(
        name: name.text,
        email: email.text,
        phone: phone.text,
        password: password.text,
        mapDesc: locationCubit.state.model!.address,
        lat: locationCubit.state.model!.lat.toString(),
        lng: locationCubit.state.model!.lng.toString(),
        termsAgree: checkTermCubit.state.data,
      );
      await GeneralRepository(context).signUp(model);
      btnKey.currentState?.animateReverse();
    }
  }

}
