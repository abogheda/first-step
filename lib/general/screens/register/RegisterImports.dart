import 'package:base_flutter/general/models/Dots/register_model.dart';
import 'package:base_flutter/general/models/LocationModel.dart';
import 'package:base_flutter/general/screens/location_address/LocationAddressImports.dart';
import 'package:base_flutter/general/screens/location_address/location_cubit/location_cubit.dart';
import 'package:base_flutter/general/screens/register/widgets/RegisterWidgetsImports.dart';
import 'package:base_flutter/general/utilities/utils_functions/UtilsImports.dart';
import 'package:base_flutter/general/widgets/AuthScaffold.dart';
import 'package:dio_helper/modals/LoadingDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../resources/GeneralRepoImports.dart';
import '../../utilities/utils_functions/LoadingDialog.dart';
part 'Register.dart';
part 'RegisterData.dart';