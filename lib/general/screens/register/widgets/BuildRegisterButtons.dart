part of 'RegisterWidgetsImports.dart';

class BuildRegisterButtons extends StatelessWidget {
final RegisterData registerData;

  const BuildRegisterButtons({required this.registerData}) ;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        DefaultButton(
          title: "تسجيل",
          // onTap: (){},
          onTap: () =>registerData.register(context),
        ),
        InkWell(
          onTap: () => AutoRouter.of(context).pop(),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MyText(
                title: tr(context, "haveAccount"),
                size: 14,
                color: MyColors.black,
              ),
              SizedBox(width: 5),
              InkWell(
                onTap: ()=> AutoRouter.of(context).pop(),
                child: MyText(
                  title: "تسجيل دخول",
                  size: 14,
                  color: MyColors.secondary,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
