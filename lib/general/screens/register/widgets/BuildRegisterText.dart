part of 'RegisterWidgetsImports.dart';
class BuildRegisterText extends StatelessWidget {
  const BuildRegisterText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          MyText(title: "انشاء حساب", color: MyColors.black, size: 17,),
          MyText(title: "من فضلك قم بادخال البيانات التالية", color: MyColors.grey, size: 13,)

        ],
      ),
    );
  }
}
