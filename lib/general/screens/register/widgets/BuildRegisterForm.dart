part of 'RegisterWidgetsImports.dart';

class BuildRegisterForm extends StatelessWidget {
  final RegisterData registerData;

  const BuildRegisterForm({required this.registerData});

  @override
  Widget build(BuildContext context) {
    return Form(
      key: registerData.formKey,
      child: Column(
        children: [
          GenericTextField(
            label: tr(context, "name"),
            fieldTypes: FieldTypes.normal,
            type: TextInputType.text,
            controller: registerData.name,
            action: TextInputAction.next,
            validate: (value) => value!.validateEmpty(context),
          ),
          GenericTextField(
            label: tr(context, "phone"),
            fieldTypes: FieldTypes.normal,
            type: TextInputType.phone,
            margin: const EdgeInsets.symmetric(vertical: 10),
            controller: registerData.phone,
            action: TextInputAction.next,
            validate: (value) => value!.validatePhone(context),
          ),
          GenericTextField(
            label:tr(context, "mail"),
            fieldTypes: FieldTypes.normal,
            type: TextInputType.emailAddress,
            controller: registerData.email,
            action: TextInputAction.next,
            validate: (value) => value!.validateEmail(context),
          ),
          BlocConsumer<LocationCubit, LocationState>(
            bloc: registerData.locationCubit,
            listener: (context, state) {
              registerData.address.text = state.model?.address ?? "";
            },
            builder: (context, state) {
              return GenericTextField(
                fieldTypes: FieldTypes.clickable,
                label: tr(context, "address"),
                margin: EdgeInsets.symmetric(vertical: 10),
                controller: registerData.address,
                validate: (value) => value!.validateEmpty(context),
                type: TextInputType.text,
                action: TextInputAction.next,
                onTab: () => registerData.onLocationClick(context),
              );
            },
          ),
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
            bloc: registerData.showPassCubit,
            builder: (_, state) {
              return GenericTextField(
                fieldTypes: state.data == true
                    ? FieldTypes.normal
                    : FieldTypes.password,
                label: tr(context, "password"),
                controller: registerData.password,
                validate: (value) => value!.validatePassword(context),
                type: TextInputType.text,
                action: TextInputAction.next,
                suffixIcon: IconButton(
                  icon: Icon(
                    state.data ? Icons.visibility : Icons.visibility_off,
                  ),
                  onPressed: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    registerData.showPassCubit.onUpdateData(!state.data);
                  },
                ),
              );
            },
          ),
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
            bloc: registerData.showConfirmPassCubit,
            builder: (_, state) {
              return GenericTextField(
                fieldTypes: state.data == true
                    ? FieldTypes.normal
                    : FieldTypes.password,
                label: tr(context, "confirmPassword"),
                controller: registerData.confirmPassword,
                validate: (value) => value!.validatePasswordConfirm(context,
                    pass: registerData.password.text),
                type: TextInputType.text,
                margin: const EdgeInsets.symmetric(vertical: 10),

                action: TextInputAction.done,
                suffixIcon: IconButton(
                  icon: Icon(
                    state.data ? Icons.visibility : Icons.visibility_off,
                  ),
                  onPressed: () => registerData.showConfirmPassCubit
                      .onUpdateData(!state.data),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
