part of 'RegisterWidgetsImports.dart';
class BuildCheckTerms extends StatelessWidget {
  final RegisterData registerData;

  const BuildCheckTerms({required this.registerData});

  @override
  Widget build(BuildContext context) {
    return Row(
        children: [
        BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
          bloc: registerData.checkTermCubit,
          builder: (_, state) {
            return Checkbox(
              value: state.data,
              onChanged: (value) =>
                  registerData.checkTermCubit.onUpdateData(value!),
            );
          },
        ),
        Expanded(
          child: Wrap(
            children: [
              MyText(
                title:  "قرأت و وافقت علي ",
                color: MyColors.black,
                size: 13,
              ),
              InkWell(
                onTap: () => AutoRouter.of(context).push(TermsRoute()),
                child: MyText(
                  title: tr(context, "terms"),
                  color: MyColors.primary,
                  size: 13,
                  decoration: TextDecoration.underline,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
