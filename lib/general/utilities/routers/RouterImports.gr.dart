// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i34;
import 'package:base_flutter/customer/models/offer_model.dart' as _i37;
import 'package:base_flutter/customer/models/provider_model.dart' as _i38;
import 'package:base_flutter/customer/screens/bill_details/BillDetailsImports.dart'
    as _i18;
import 'package:base_flutter/customer/screens/bills/BillsImports.dart' as _i19;
import 'package:base_flutter/customer/screens/chat_room/CustomerChatRoomImports.dart'
    as _i32;
import 'package:base_flutter/customer/screens/coupons/CouponsImports.dart'
    as _i20;
import 'package:base_flutter/customer/screens/department_details/DepartmentDetailsImports.dart'
    as _i21;
import 'package:base_flutter/customer/screens/departments/DepartmentsImports.dart'
    as _i22;
import 'package:base_flutter/customer/screens/edit_porfile/EditProfileImports.dart'
    as _i23;
import 'package:base_flutter/customer/screens/favorites/FavoritesImports.dart'
    as _i24;
import 'package:base_flutter/customer/screens/home/HomeImports.dart' as _i16;
import 'package:base_flutter/customer/screens/languages/LanguagesImports.dart'
    as _i25;
import 'package:base_flutter/customer/screens/offer_details/OfferDetailsImports.dart'
    as _i26;
import 'package:base_flutter/customer/screens/payment/PaymentImports.dart'
    as _i17;
import 'package:base_flutter/customer/screens/providers_details/ProvidersDetailsImports.dart'
    as _i27;
import 'package:base_flutter/customer/screens/reservation_details/ReservationDetailsImports.dart'
    as _i28;
import 'package:base_flutter/customer/screens/reservations/ReservationsImports.dart'
    as _i29;
import 'package:base_flutter/customer/screens/services_details/ServicesDetailsImports.dart'
    as _i30;
import 'package:base_flutter/customer/screens/wallet/WalletImports.dart'
    as _i31;
import 'package:base_flutter/general/screens/about/AboutImports.dart' as _i8;
import 'package:base_flutter/general/screens/active_account/ActiveAccountImports.dart'
    as _i4;
import 'package:base_flutter/general/screens/change_password/ChangePasswordImports.dart'
    as _i12;
import 'package:base_flutter/general/screens/confirm_password/ConfirmPasswordImports.dart'
    as _i11;
import 'package:base_flutter/general/screens/contact_us/ContactUsImports.dart'
    as _i9;
import 'package:base_flutter/general/screens/forget_password/ForgetPasswordImports.dart'
    as _i3;
import 'package:base_flutter/general/screens/image_zoom/ImageZoom.dart' as _i13;
import 'package:base_flutter/general/screens/login/LoginImports.dart' as _i2;
import 'package:base_flutter/general/screens/on_boarding/OnBoardingImports.dart'
    as _i14;
import 'package:base_flutter/general/screens/provider_webview/provider_webview_imports.dart'
    as _i33;
import 'package:base_flutter/general/screens/register/RegisterImports.dart'
    as _i15;
import 'package:base_flutter/general/screens/reset_password/ResetPasswordImports.dart'
    as _i5;
import 'package:base_flutter/general/screens/select_lang/SelectLangImports.dart'
    as _i6;
import 'package:base_flutter/general/screens/select_user/SelectUserImports.dart'
    as _i10;
import 'package:base_flutter/general/screens/splash/SplashImports.dart' as _i1;
import 'package:base_flutter/general/screens/terms/TermsImports.dart' as _i7;
import 'package:flutter/cupertino.dart' as _i36;
import 'package:flutter/material.dart' as _i35;

class AppRouter extends _i34.RootStackRouter {
  AppRouter([_i35.GlobalKey<_i35.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i34.PageFactory> pagesMap = {
    SplashRoute.name: (routeData) {
      final args = routeData.argsAs<SplashRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i1.Splash(navigatorKey: args.navigatorKey));
    },
    LoginRoute.name: (routeData) {
      return _i34.CustomPage<dynamic>(
          routeData: routeData,
          child: _i2.Login(),
          opaque: true,
          barrierDismissible: false);
    },
    ForgetPasswordRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i3.ForgetPassword());
    },
    ActiveAccountRoute.name: (routeData) {
      final args = routeData.argsAs<ActiveAccountRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i4.ActiveAccount(phone: args.phone));
    },
    ResetPasswordRoute.name: (routeData) {
      final args = routeData.argsAs<ResetPasswordRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i5.ResetPassword(phone: args.phone));
    },
    SelectLangRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i6.SelectLang());
    },
    TermsRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i7.Terms());
    },
    AboutRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i8.About());
    },
    ContactUsRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i9.ContactUs());
    },
    SelectUserRoute.name: (routeData) {
      return _i34.CustomPage<dynamic>(
          routeData: routeData,
          child: _i10.SelectUser(),
          transitionsBuilder: _i34.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 1500,
          opaque: true,
          barrierDismissible: false);
    },
    ConfirmPasswordRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i11.ConfirmPassword());
    },
    ChangePasswordRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i12.ChangePassword());
    },
    ImageZoomRoute.name: (routeData) {
      final args = routeData.argsAs<ImageZoomRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i13.ImageZoom(images: args.images));
    },
    OnBoardingRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i14.OnBoarding());
    },
    RegisterRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i15.Register());
    },
    HomeRoute.name: (routeData) {
      final args =
          routeData.argsAs<HomeRouteArgs>(orElse: () => const HomeRouteArgs());
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i16.Home(index: args.index));
    },
    PaymentRoute.name: (routeData) {
      final args = routeData.argsAs<PaymentRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i17.Payment(
              key: args.key, id: args.id, totalPrice: args.totalPrice));
    },
    BillDetailsRoute.name: (routeData) {
      final args = routeData.argsAs<BillDetailsRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i18.BillDetails(key: args.key, id: args.id));
    },
    BillsRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i19.Bills());
    },
    CouponsRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i20.Coupons());
    },
    DepartmentsDetailsRoute.name: (routeData) {
      final args = routeData.argsAs<DepartmentsDetailsRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i21.DepartmentsDetails(
              key: args.key,
              subCategoryId: args.subCategoryId,
              image: args.image,
              title: args.title));
    },
    DepartmentsRoute.name: (routeData) {
      final args = routeData.argsAs<DepartmentsRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i22.Departments(title: args.title, catId: args.catId));
    },
    EditProfileRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i23.EditProfile());
    },
    FavoritesRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i24.Favorites());
    },
    LanguagesRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i25.Languages());
    },
    OfferDetailsRoute.name: (routeData) {
      final args = routeData.argsAs<OfferDetailsRouteArgs>(
          orElse: () => const OfferDetailsRouteArgs());
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i26.OfferDetails(key: args.key, model: args.model));
    },
    ProvidersDetailsRoute.name: (routeData) {
      final args = routeData.argsAs<ProvidersDetailsRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i27.ProvidersDetails(
              key: args.key,
              id: args.id,
              providerModel: args.providerModel,
              subCategoryId: args.subCategoryId,
              index: args.index));
    },
    ReservationDetailsRoute.name: (routeData) {
      final args = routeData.argsAs<ReservationDetailsRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i28.ReservationDetails(
              key: args.key, invoiceId: args.invoiceId, orderId: args.orderId));
    },
    ReservationsRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i29.Reservations());
    },
    ServicesDetailsRoute.name: (routeData) {
      final args = routeData.argsAs<ServicesDetailsRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i30.ServicesDetails(
              key: args.key, id: args.id, index: args.index));
    },
    WalletRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i31.Wallet());
    },
    CustomerChatRoomRoute.name: (routeData) {
      final args = routeData.argsAs<CustomerChatRoomRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i32.CustomerChatRoom(
              roomId: args.roomId, receiverName: args.receiverName));
    },
    ProviderWebViewRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i33.ProviderWebView());
    }
  };

  @override
  List<_i34.RouteConfig> get routes => [
        _i34.RouteConfig(SplashRoute.name, path: '/'),
        _i34.RouteConfig(LoginRoute.name, path: '/Login'),
        _i34.RouteConfig(ForgetPasswordRoute.name, path: '/forget-password'),
        _i34.RouteConfig(ActiveAccountRoute.name, path: '/active-account'),
        _i34.RouteConfig(ResetPasswordRoute.name, path: '/reset-password'),
        _i34.RouteConfig(SelectLangRoute.name, path: '/select-lang'),
        _i34.RouteConfig(TermsRoute.name, path: '/Terms'),
        _i34.RouteConfig(AboutRoute.name, path: '/About'),
        _i34.RouteConfig(ContactUsRoute.name, path: '/contact-us'),
        _i34.RouteConfig(SelectUserRoute.name, path: '/select-user'),
        _i34.RouteConfig(ConfirmPasswordRoute.name, path: '/confirm-password'),
        _i34.RouteConfig(ChangePasswordRoute.name, path: '/change-password'),
        _i34.RouteConfig(ImageZoomRoute.name, path: '/image-zoom'),
        _i34.RouteConfig(OnBoardingRoute.name, path: '/on-boarding'),
        _i34.RouteConfig(RegisterRoute.name, path: '/Register'),
        _i34.RouteConfig(HomeRoute.name, path: '/Home'),
        _i34.RouteConfig(PaymentRoute.name, path: '/Payment'),
        _i34.RouteConfig(BillDetailsRoute.name, path: '/bill-details'),
        _i34.RouteConfig(BillsRoute.name, path: '/Bills'),
        _i34.RouteConfig(CouponsRoute.name, path: '/Coupons'),
        _i34.RouteConfig(DepartmentsDetailsRoute.name,
            path: '/departments-details'),
        _i34.RouteConfig(DepartmentsRoute.name, path: '/Departments'),
        _i34.RouteConfig(EditProfileRoute.name, path: '/edit-profile'),
        _i34.RouteConfig(FavoritesRoute.name, path: '/Favorites'),
        _i34.RouteConfig(LanguagesRoute.name, path: '/Languages'),
        _i34.RouteConfig(OfferDetailsRoute.name, path: '/offer-details'),
        _i34.RouteConfig(ProvidersDetailsRoute.name,
            path: '/providers-details'),
        _i34.RouteConfig(ReservationDetailsRoute.name,
            path: '/reservation-details'),
        _i34.RouteConfig(ReservationsRoute.name, path: '/Reservations'),
        _i34.RouteConfig(ServicesDetailsRoute.name, path: '/services-details'),
        _i34.RouteConfig(WalletRoute.name, path: '/Wallet'),
        _i34.RouteConfig(CustomerChatRoomRoute.name,
            path: '/customer-chat-room'),
        _i34.RouteConfig(ProviderWebViewRoute.name, path: '/provider-web-view')
      ];
}

/// generated route for
/// [_i1.Splash]
class SplashRoute extends _i34.PageRouteInfo<SplashRouteArgs> {
  SplashRoute({required _i36.GlobalKey<_i36.NavigatorState> navigatorKey})
      : super(SplashRoute.name,
            path: '/', args: SplashRouteArgs(navigatorKey: navigatorKey));

  static const String name = 'SplashRoute';
}

class SplashRouteArgs {
  const SplashRouteArgs({required this.navigatorKey});

  final _i36.GlobalKey<_i36.NavigatorState> navigatorKey;

  @override
  String toString() {
    return 'SplashRouteArgs{navigatorKey: $navigatorKey}';
  }
}

/// generated route for
/// [_i2.Login]
class LoginRoute extends _i34.PageRouteInfo<void> {
  const LoginRoute() : super(LoginRoute.name, path: '/Login');

  static const String name = 'LoginRoute';
}

/// generated route for
/// [_i3.ForgetPassword]
class ForgetPasswordRoute extends _i34.PageRouteInfo<void> {
  const ForgetPasswordRoute()
      : super(ForgetPasswordRoute.name, path: '/forget-password');

  static const String name = 'ForgetPasswordRoute';
}

/// generated route for
/// [_i4.ActiveAccount]
class ActiveAccountRoute extends _i34.PageRouteInfo<ActiveAccountRouteArgs> {
  ActiveAccountRoute({required String phone})
      : super(ActiveAccountRoute.name,
            path: '/active-account',
            args: ActiveAccountRouteArgs(phone: phone));

  static const String name = 'ActiveAccountRoute';
}

class ActiveAccountRouteArgs {
  const ActiveAccountRouteArgs({required this.phone});

  final String phone;

  @override
  String toString() {
    return 'ActiveAccountRouteArgs{phone: $phone}';
  }
}

/// generated route for
/// [_i5.ResetPassword]
class ResetPasswordRoute extends _i34.PageRouteInfo<ResetPasswordRouteArgs> {
  ResetPasswordRoute({required String phone})
      : super(ResetPasswordRoute.name,
            path: '/reset-password',
            args: ResetPasswordRouteArgs(phone: phone));

  static const String name = 'ResetPasswordRoute';
}

class ResetPasswordRouteArgs {
  const ResetPasswordRouteArgs({required this.phone});

  final String phone;

  @override
  String toString() {
    return 'ResetPasswordRouteArgs{phone: $phone}';
  }
}

/// generated route for
/// [_i6.SelectLang]
class SelectLangRoute extends _i34.PageRouteInfo<void> {
  const SelectLangRoute() : super(SelectLangRoute.name, path: '/select-lang');

  static const String name = 'SelectLangRoute';
}

/// generated route for
/// [_i7.Terms]
class TermsRoute extends _i34.PageRouteInfo<void> {
  const TermsRoute() : super(TermsRoute.name, path: '/Terms');

  static const String name = 'TermsRoute';
}

/// generated route for
/// [_i8.About]
class AboutRoute extends _i34.PageRouteInfo<void> {
  const AboutRoute() : super(AboutRoute.name, path: '/About');

  static const String name = 'AboutRoute';
}

/// generated route for
/// [_i9.ContactUs]
class ContactUsRoute extends _i34.PageRouteInfo<void> {
  const ContactUsRoute() : super(ContactUsRoute.name, path: '/contact-us');

  static const String name = 'ContactUsRoute';
}

/// generated route for
/// [_i10.SelectUser]
class SelectUserRoute extends _i34.PageRouteInfo<void> {
  const SelectUserRoute() : super(SelectUserRoute.name, path: '/select-user');

  static const String name = 'SelectUserRoute';
}

/// generated route for
/// [_i11.ConfirmPassword]
class ConfirmPasswordRoute extends _i34.PageRouteInfo<void> {
  const ConfirmPasswordRoute()
      : super(ConfirmPasswordRoute.name, path: '/confirm-password');

  static const String name = 'ConfirmPasswordRoute';
}

/// generated route for
/// [_i12.ChangePassword]
class ChangePasswordRoute extends _i34.PageRouteInfo<void> {
  const ChangePasswordRoute()
      : super(ChangePasswordRoute.name, path: '/change-password');

  static const String name = 'ChangePasswordRoute';
}

/// generated route for
/// [_i13.ImageZoom]
class ImageZoomRoute extends _i34.PageRouteInfo<ImageZoomRouteArgs> {
  ImageZoomRoute({required List<dynamic> images})
      : super(ImageZoomRoute.name,
            path: '/image-zoom', args: ImageZoomRouteArgs(images: images));

  static const String name = 'ImageZoomRoute';
}

class ImageZoomRouteArgs {
  const ImageZoomRouteArgs({required this.images});

  final List<dynamic> images;

  @override
  String toString() {
    return 'ImageZoomRouteArgs{images: $images}';
  }
}

/// generated route for
/// [_i14.OnBoarding]
class OnBoardingRoute extends _i34.PageRouteInfo<void> {
  const OnBoardingRoute() : super(OnBoardingRoute.name, path: '/on-boarding');

  static const String name = 'OnBoardingRoute';
}

/// generated route for
/// [_i15.Register]
class RegisterRoute extends _i34.PageRouteInfo<void> {
  const RegisterRoute() : super(RegisterRoute.name, path: '/Register');

  static const String name = 'RegisterRoute';
}

/// generated route for
/// [_i16.Home]
class HomeRoute extends _i34.PageRouteInfo<HomeRouteArgs> {
  HomeRoute({int index = 0})
      : super(HomeRoute.name, path: '/Home', args: HomeRouteArgs(index: index));

  static const String name = 'HomeRoute';
}

class HomeRouteArgs {
  const HomeRouteArgs({this.index = 0});

  final int index;

  @override
  String toString() {
    return 'HomeRouteArgs{index: $index}';
  }
}

/// generated route for
/// [_i17.Payment]
class PaymentRoute extends _i34.PageRouteInfo<PaymentRouteArgs> {
  PaymentRoute({_i36.Key? key, required int id, required num totalPrice})
      : super(PaymentRoute.name,
            path: '/Payment',
            args: PaymentRouteArgs(key: key, id: id, totalPrice: totalPrice));

  static const String name = 'PaymentRoute';
}

class PaymentRouteArgs {
  const PaymentRouteArgs(
      {this.key, required this.id, required this.totalPrice});

  final _i36.Key? key;

  final int id;

  final num totalPrice;

  @override
  String toString() {
    return 'PaymentRouteArgs{key: $key, id: $id, totalPrice: $totalPrice}';
  }
}

/// generated route for
/// [_i18.BillDetails]
class BillDetailsRoute extends _i34.PageRouteInfo<BillDetailsRouteArgs> {
  BillDetailsRoute({_i36.Key? key, required int id})
      : super(BillDetailsRoute.name,
            path: '/bill-details',
            args: BillDetailsRouteArgs(key: key, id: id));

  static const String name = 'BillDetailsRoute';
}

class BillDetailsRouteArgs {
  const BillDetailsRouteArgs({this.key, required this.id});

  final _i36.Key? key;

  final int id;

  @override
  String toString() {
    return 'BillDetailsRouteArgs{key: $key, id: $id}';
  }
}

/// generated route for
/// [_i19.Bills]
class BillsRoute extends _i34.PageRouteInfo<void> {
  const BillsRoute() : super(BillsRoute.name, path: '/Bills');

  static const String name = 'BillsRoute';
}

/// generated route for
/// [_i20.Coupons]
class CouponsRoute extends _i34.PageRouteInfo<void> {
  const CouponsRoute() : super(CouponsRoute.name, path: '/Coupons');

  static const String name = 'CouponsRoute';
}

/// generated route for
/// [_i21.DepartmentsDetails]
class DepartmentsDetailsRoute
    extends _i34.PageRouteInfo<DepartmentsDetailsRouteArgs> {
  DepartmentsDetailsRoute(
      {_i36.Key? key, required int subCategoryId, String? image, String? title})
      : super(DepartmentsDetailsRoute.name,
            path: '/departments-details',
            args: DepartmentsDetailsRouteArgs(
                key: key,
                subCategoryId: subCategoryId,
                image: image,
                title: title));

  static const String name = 'DepartmentsDetailsRoute';
}

class DepartmentsDetailsRouteArgs {
  const DepartmentsDetailsRouteArgs(
      {this.key, required this.subCategoryId, this.image, this.title});

  final _i36.Key? key;

  final int subCategoryId;

  final String? image;

  final String? title;

  @override
  String toString() {
    return 'DepartmentsDetailsRouteArgs{key: $key, subCategoryId: $subCategoryId, image: $image, title: $title}';
  }
}

/// generated route for
/// [_i22.Departments]
class DepartmentsRoute extends _i34.PageRouteInfo<DepartmentsRouteArgs> {
  DepartmentsRoute({required String title, required int catId})
      : super(DepartmentsRoute.name,
            path: '/Departments',
            args: DepartmentsRouteArgs(title: title, catId: catId));

  static const String name = 'DepartmentsRoute';
}

class DepartmentsRouteArgs {
  const DepartmentsRouteArgs({required this.title, required this.catId});

  final String title;

  final int catId;

  @override
  String toString() {
    return 'DepartmentsRouteArgs{title: $title, catId: $catId}';
  }
}

/// generated route for
/// [_i23.EditProfile]
class EditProfileRoute extends _i34.PageRouteInfo<void> {
  const EditProfileRoute()
      : super(EditProfileRoute.name, path: '/edit-profile');

  static const String name = 'EditProfileRoute';
}

/// generated route for
/// [_i24.Favorites]
class FavoritesRoute extends _i34.PageRouteInfo<void> {
  const FavoritesRoute() : super(FavoritesRoute.name, path: '/Favorites');

  static const String name = 'FavoritesRoute';
}

/// generated route for
/// [_i25.Languages]
class LanguagesRoute extends _i34.PageRouteInfo<void> {
  const LanguagesRoute() : super(LanguagesRoute.name, path: '/Languages');

  static const String name = 'LanguagesRoute';
}

/// generated route for
/// [_i26.OfferDetails]
class OfferDetailsRoute extends _i34.PageRouteInfo<OfferDetailsRouteArgs> {
  OfferDetailsRoute({_i36.Key? key, _i37.OfferModel? model})
      : super(OfferDetailsRoute.name,
            path: '/offer-details',
            args: OfferDetailsRouteArgs(key: key, model: model));

  static const String name = 'OfferDetailsRoute';
}

class OfferDetailsRouteArgs {
  const OfferDetailsRouteArgs({this.key, this.model});

  final _i36.Key? key;

  final _i37.OfferModel? model;

  @override
  String toString() {
    return 'OfferDetailsRouteArgs{key: $key, model: $model}';
  }
}

/// generated route for
/// [_i27.ProvidersDetails]
class ProvidersDetailsRoute
    extends _i34.PageRouteInfo<ProvidersDetailsRouteArgs> {
  ProvidersDetailsRoute(
      {_i36.Key? key,
      required int id,
      _i38.ProviderModel? providerModel,
      int? subCategoryId,
      int? index})
      : super(ProvidersDetailsRoute.name,
            path: '/providers-details',
            args: ProvidersDetailsRouteArgs(
                key: key,
                id: id,
                providerModel: providerModel,
                subCategoryId: subCategoryId,
                index: index));

  static const String name = 'ProvidersDetailsRoute';
}

class ProvidersDetailsRouteArgs {
  const ProvidersDetailsRouteArgs(
      {this.key,
      required this.id,
      this.providerModel,
      this.subCategoryId,
      this.index});

  final _i36.Key? key;

  final int id;

  final _i38.ProviderModel? providerModel;

  final int? subCategoryId;

  final int? index;

  @override
  String toString() {
    return 'ProvidersDetailsRouteArgs{key: $key, id: $id, providerModel: $providerModel, subCategoryId: $subCategoryId, index: $index}';
  }
}

/// generated route for
/// [_i28.ReservationDetails]
class ReservationDetailsRoute
    extends _i34.PageRouteInfo<ReservationDetailsRouteArgs> {
  ReservationDetailsRoute(
      {_i36.Key? key, required int invoiceId, required int orderId})
      : super(ReservationDetailsRoute.name,
            path: '/reservation-details',
            args: ReservationDetailsRouteArgs(
                key: key, invoiceId: invoiceId, orderId: orderId));

  static const String name = 'ReservationDetailsRoute';
}

class ReservationDetailsRouteArgs {
  const ReservationDetailsRouteArgs(
      {this.key, required this.invoiceId, required this.orderId});

  final _i36.Key? key;

  final int invoiceId;

  final int orderId;

  @override
  String toString() {
    return 'ReservationDetailsRouteArgs{key: $key, invoiceId: $invoiceId, orderId: $orderId}';
  }
}

/// generated route for
/// [_i29.Reservations]
class ReservationsRoute extends _i34.PageRouteInfo<void> {
  const ReservationsRoute()
      : super(ReservationsRoute.name, path: '/Reservations');

  static const String name = 'ReservationsRoute';
}

/// generated route for
/// [_i30.ServicesDetails]
class ServicesDetailsRoute
    extends _i34.PageRouteInfo<ServicesDetailsRouteArgs> {
  ServicesDetailsRoute({_i36.Key? key, required int id, int? index})
      : super(ServicesDetailsRoute.name,
            path: '/services-details',
            args: ServicesDetailsRouteArgs(key: key, id: id, index: index));

  static const String name = 'ServicesDetailsRoute';
}

class ServicesDetailsRouteArgs {
  const ServicesDetailsRouteArgs({this.key, required this.id, this.index});

  final _i36.Key? key;

  final int id;

  final int? index;

  @override
  String toString() {
    return 'ServicesDetailsRouteArgs{key: $key, id: $id, index: $index}';
  }
}

/// generated route for
/// [_i31.Wallet]
class WalletRoute extends _i34.PageRouteInfo<void> {
  const WalletRoute() : super(WalletRoute.name, path: '/Wallet');

  static const String name = 'WalletRoute';
}

/// generated route for
/// [_i32.CustomerChatRoom]
class CustomerChatRoomRoute
    extends _i34.PageRouteInfo<CustomerChatRoomRouteArgs> {
  CustomerChatRoomRoute({required int roomId, String receiverName = ""})
      : super(CustomerChatRoomRoute.name,
            path: '/customer-chat-room',
            args: CustomerChatRoomRouteArgs(
                roomId: roomId, receiverName: receiverName));

  static const String name = 'CustomerChatRoomRoute';
}

class CustomerChatRoomRouteArgs {
  const CustomerChatRoomRouteArgs(
      {required this.roomId, this.receiverName = ""});

  final int roomId;

  final String receiverName;

  @override
  String toString() {
    return 'CustomerChatRoomRouteArgs{roomId: $roomId, receiverName: $receiverName}';
  }
}

/// generated route for
/// [_i33.ProviderWebView]
class ProviderWebViewRoute extends _i34.PageRouteInfo<void> {
  const ProviderWebViewRoute()
      : super(ProviderWebViewRoute.name, path: '/provider-web-view');

  static const String name = 'ProviderWebViewRoute';
}
