import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/bill_details/BillDetailsImports.dart';
import 'package:base_flutter/customer/screens/bills/BillsImports.dart';
import 'package:base_flutter/customer/screens/chat_room/CustomerChatRoomImports.dart';
import 'package:base_flutter/customer/screens/coupons/CouponsImports.dart';
import 'package:base_flutter/customer/screens/department_details/DepartmentDetailsImports.dart';
import 'package:base_flutter/customer/screens/departments/DepartmentsImports.dart';
import 'package:base_flutter/customer/screens/edit_porfile/EditProfileImports.dart';
import 'package:base_flutter/customer/screens/favorites/FavoritesImports.dart';
import 'package:base_flutter/customer/screens/home/HomeImports.dart';
import 'package:base_flutter/customer/screens/languages/LanguagesImports.dart';
import 'package:base_flutter/customer/screens/offer_details/OfferDetailsImports.dart';
import 'package:base_flutter/customer/screens/payment/PaymentImports.dart';
import 'package:base_flutter/customer/screens/providers_details/ProvidersDetailsImports.dart';
import 'package:base_flutter/customer/screens/reservation_details/ReservationDetailsImports.dart';
import 'package:base_flutter/customer/screens/reservations/ReservationsImports.dart';
import 'package:base_flutter/customer/screens/services_details/ServicesDetailsImports.dart';
import 'package:base_flutter/customer/screens/wallet/WalletImports.dart';
import 'package:base_flutter/general/screens/about/AboutImports.dart';
import 'package:base_flutter/general/screens/active_account/ActiveAccountImports.dart';
import 'package:base_flutter/general/screens/change_password/ChangePasswordImports.dart';
import 'package:base_flutter/general/screens/confirm_password/ConfirmPasswordImports.dart';
import 'package:base_flutter/general/screens/contact_us/ContactUsImports.dart';
import 'package:base_flutter/general/screens/forget_password/ForgetPasswordImports.dart';
import 'package:base_flutter/general/screens/image_zoom/ImageZoom.dart';
import 'package:base_flutter/general/screens/login/LoginImports.dart';
import 'package:base_flutter/general/screens/on_boarding/OnBoardingImports.dart';
import 'package:base_flutter/general/screens/register/RegisterImports.dart';
import 'package:base_flutter/general/screens/reset_password/ResetPasswordImports.dart';
import 'package:base_flutter/general/screens/select_lang/SelectLangImports.dart';
import 'package:base_flutter/general/screens/select_user/SelectUserImports.dart';
import 'package:base_flutter/general/screens/splash/SplashImports.dart';
import 'package:base_flutter/general/screens/terms/TermsImports.dart';

import '../../screens/provider_webview/provider_webview_imports.dart';


part 'Router.dart';