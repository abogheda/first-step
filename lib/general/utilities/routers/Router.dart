part of 'RouterImports.dart';

@AdaptiveAutoRouter(
  routes: <AutoRoute>[
    //general routes
    AdaptiveRoute(
      page: Splash,
      initial: true,
    ),
    CustomRoute(
      page: Login,
    ),
    AdaptiveRoute(page: ForgetPassword),
    AdaptiveRoute(page: ActiveAccount),
    AdaptiveRoute(page: ResetPassword),
    AdaptiveRoute(page: SelectLang),
    AdaptiveRoute(page: Terms),
    AdaptiveRoute(page: About),
    AdaptiveRoute(page: ContactUs),
    CustomRoute(
        page: SelectUser,
        transitionsBuilder: TransitionsBuilders.fadeIn,
        durationInMilliseconds: 1500),
    AdaptiveRoute(page: ConfirmPassword),
    AdaptiveRoute(page: ChangePassword),
    AdaptiveRoute(page: ImageZoom),
    AdaptiveRoute(page: OnBoarding),
    AdaptiveRoute(page: Register),

    //customer
    AdaptiveRoute(page: Home),
    AdaptiveRoute(page: Payment),
    AdaptiveRoute(page: BillDetails),
    AdaptiveRoute(page: Bills),
    AdaptiveRoute(page: Coupons),
    AdaptiveRoute(page: DepartmentsDetails),
    AdaptiveRoute(page: Departments),
    AdaptiveRoute(page: EditProfile),
    AdaptiveRoute(page: Favorites),

    AdaptiveRoute(page: Languages),
    AdaptiveRoute(page: OfferDetails),
    AdaptiveRoute(page: ProvidersDetails),
    AdaptiveRoute(page: ReservationDetails),
    AdaptiveRoute(page: Reservations),
    AdaptiveRoute(page: ServicesDetails),
    AdaptiveRoute(page: Wallet),
    AdaptiveRoute(page: CustomerChatRoom),
    AdaptiveRoute(page: ProviderWebView),

  ],
)
class $AppRouter {}
