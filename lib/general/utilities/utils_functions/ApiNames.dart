class ApiNames{

  static const String baseUrl = "https://firststep1.net/api/";
  static const String branch = "1";

  static const String aboutApp = "about";
  static const String terms = "terms";
  static const String login = "sign-in";
  static const String sendCode = "activate";
  static const String resendCode = "resend-code";
  static const String repeatedQuestions = "FrequentlyAskedQuestions";
  static const String switchNotify = "switch-notify";
  static const String forgetPassword = "forget-password-send-code";
  static const String resetPassword = "reset-password";
  static const String contactUs = "new-complaint";
  static const String intros = "intros";


  // New
  static const String signUp = "sign-up";
  static const String signOut = "sign-out";
  static const String updateProfile = "update-profile";
  static const String updatePassword = "update-passward";
  static const String changeLang = "change-lang";
  static const String notifications = "notifications";
  static const String deleteUser = "delete-user";
  static const String deleteNotify = "delete-notifications";
  static const String home = "user-home";
  static const String coupons = "get-copouns";
  static const String subCategories = "categories";
  static const String providers = "all-providers";
  static const String Offers = "all-offers";
  static const String addToCart = "add-to-cart";
  static const String getCart = "get-cart";
  static const String checkCoupon = "check-coupon";
  static const String removeFromCart = "remove-from-cart";
  static const String getProviderOffers = "get-provider-offers";
  static const String getProviderServices = "get-provider-services";
  static const String getServiceDetails = "get-service-details";
  static const String getProviderRates = "get-provider-rates";
  static const String getProviderFiles = "get-provider-files";
  static const String getFavoriteProviders = "get-favorite-providers";
  static const String getFavoriteServices = "get-favorite-services";
  static const String getMyNewOrders = "get-my-new-orders";
  static const String getMyFinishedOrders = "get-my-finished-orders";
  static const String createOrder = "create-order";
  static const String getOrderDetails = "get-order-details";
  static const String getMyInvoices = "get-my-invoices";
  static const String getInvoicesDetails = "get-invoice-details";
  static const String getRoomMessages = "get-room-messages";
  static const String sendMessage = "send-message/";
  static const String rateProvider = "post-provider-rate";
  static const String rateService = "post-service-rate";
  static const String getUserBalance = "get-user-balance";
  static const String switchUserFavorite = "switch-user-favorite";
  static const String switchServiceFavorite = "switch-service-favorite";
  static const String getProviderTime = "get-provider-Timings";
  static const String completePayment = "complete-payment";

}