part of 'notification_count_cubit.dart';

abstract class NotificationCountState extends Equatable {
  final int count;

  const NotificationCountState({required this.count});
}

class NotificationCountInitial extends NotificationCountState {
  NotificationCountInitial() : super(count: 0);

  @override
  List<Object> get props => [count];
}

class NotificationCountUpdateState extends NotificationCountState {
  NotificationCountUpdateState(int count) : super(count: count);


  @override
  List<Object> get props => [count];
}
