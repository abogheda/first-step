import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'notification_count_state.dart';

class NotificationCountCubit extends Cubit<NotificationCountState> {
  NotificationCountCubit() : super(NotificationCountInitial());

  onUpdateUserData(int count) {
    emit(NotificationCountUpdateState(count));
  }
}
