import 'dart:developer';
import 'dart:io';
import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/general/models/Dots/register_model.dart';
import 'package:base_flutter/general/models/QuestionModel.dart';
import 'package:base_flutter/general/models/intro_model.dart';
import 'package:base_flutter/general/resources/handleHttpData.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/general/utilities/utils_functions/ApiNames.dart';
import 'package:base_flutter/general/utilities/utils_functions/UtilsImports.dart';
import 'package:dio_helper/ModaLs/LoadingDialog.dart';
import 'package:dio_helper/http/GenericHttp.dart';
import 'package:dio_helper/utils/DioUtils.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

part 'GeneralRepository.dart';
part 'GeneralHttpMethods.dart';