import 'dart:developer';

import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/general/blocks/user_cubit/user_cubit.dart';
import 'package:base_flutter/general/models/UserModel.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';

import 'package:base_flutter/general/utilities/utils_functions/UtilsImports.dart';
import 'package:dio_helper/ModaLs/LoadingDialog.dart';
import 'package:dio_helper/utils/DioUtils.dart';
import 'package:dio_helper/utils/GlobalState.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HandleData {
  HandleData._();

  static final HandleData instance = HandleData._();

  failure(dynamic data, BuildContext context) {
    switch (data["key"]) {
      case 'fail':
        DioUtils.dismissDialog();
        return CustomToast.showSimpleToast(msg: data['msg']);
      case 'exception':
        DioUtils.dismissDialog();
        return CustomToast.showSimpleToast(msg: data['msg']);
      case 'blocked':
        DioUtils.dismissDialog();
        Utils.unAuthenticatedFunc(context: context);
        return CustomToast.showSimpleToast(msg: data['msg']);
      case 'unauthenticated':
        DioUtils.dismissDialog();
       return Utils.unAuthenticatedFunc(context: context);
        // return CustomToast.showSimpleToast(msg: data['msg']);
      case 'needActive':
        DioUtils.dismissDialog();
        return CustomToast.showSimpleToast(msg: data['msg']);
    }
  }

  /// ********************* post Request ************************
  handlePostData(dynamic data, BuildContext context, {bool fullData = true}) {
    if (data["key"] == "success") {
      DioUtils.dismissDialog();
      CustomToast.showSimpleToast(msg: data['msg']);
      log("++++++++++++++++++++++++++++++++ data ${data.toString()}");
      if (fullData) {
        return data;
      }
      return data["data"];
    }
    return failure(data, context);
  }

  /// ********************* get Request ************************
  handleGetData(dynamic data, BuildContext context, {bool fullData = false}) {
    if (data["key"] == "success") {
      if (fullData) {
        return data;
      }
      return data["data"];
    }
    return failure(data, context);
  }

  /// ********************* customize Requests ************************

  Future<bool> manipulateUpdateProfileData(
      dynamic data, BuildContext context) async {
    if (data["key"] == "success") {
      DioUtils.dismissDialog();
      CustomToast.showSimpleToast(msg: data['msg']);
      log("++++++++++++++++++++++++++++++++ data ${data.toString()}");
      if (data["data"] != null) {
        log("<>>>>>>>>>>>> data1 ${data.toString()}");
        UserModel user = UserModel.fromJson(data["data"]);
        user.token = GlobalState.instance.get("token");
        await Utils.saveUserData(user);
        context.read<UserCubit>().onUpdateUserData(user);
        log("<>>>>>>>>>>>> data2 ${data.toString()}");
      }
      return true;
    } else {
      failure(data, context);
      return false;
    }
  }

  Future<bool> handleRegister(dynamic data, BuildContext context,
      String phone) async {
    if (data != null) {
      CustomToast.showSimpleToast(msg: data['msg']);
      await AutoRouter.of(context).push(ActiveAccountRoute(phone: phone));
      return true;
    } else {
      failure(data, context);
      return false;
    }
  }
}
