part of 'GeneralRepoImports.dart';


class GeneralRepository {
  late BuildContext _context;
  late GeneralHttpMethods _generalHttpMethods;
  GeneralRepository(BuildContext context) {
    _context = context;
    _generalHttpMethods = new GeneralHttpMethods(_context);
  }

  Future<bool> setUserLogin(String phone, String pass)=> _generalHttpMethods.userLogin(phone, pass);

  Future<bool> sendCode(String code,  String phone,)  =>
      _generalHttpMethods.sendCode(code,phone);

  Future<bool> resendCode(String phone) =>
      _generalHttpMethods.resendCode(phone);

  Future<String?> aboutApp() => _generalHttpMethods.aboutApp();

  Future<String?> terms() => _generalHttpMethods.terms();

  Future<bool> switchNotify() => _generalHttpMethods.switchNotify();

  Future<bool> forgetPassword(String phone) =>
      _generalHttpMethods.forgetPassword(phone);

  Future<bool> resetUserPassword(String phone, String code, String pass) =>
      _generalHttpMethods.resetUserPassword(phone, code, pass);

  Future<List<QuestionModel>> frequentQuestions() =>
      _generalHttpMethods.frequentQuestions();

  Future<bool> addComplaint(
      String userName, String phone,String email ,String complaint)=>
  _generalHttpMethods.addComplaint(userName, phone, email, complaint);

  Future<bool> signUp(RegisterModel model)=>_generalHttpMethods.signUp(model);

  Future<List<IntroModel>> intros()=>_generalHttpMethods.intros();

}
