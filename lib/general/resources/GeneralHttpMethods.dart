part of 'GeneralRepoImports.dart';

class GeneralHttpMethods {
  final BuildContext context;

  FirebaseMessaging messaging = FirebaseMessaging.instance;

  GeneralHttpMethods(this.context);

  Future<bool> userLogin(String phone, String pass) async {
    String param = "?count_notifications";
    String? _token = await messaging.getToken();
    Map<String, dynamic> body = {
      "phone": "$phone",
      "password": "$pass",
      "device_id": "$_token",
      "device_type": Platform.isIOS ? "ios" : "android",
    };

    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.login +param,
      json: body,
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      returnDataFun: (data){
        log(">>>>>>>>>>>>>>${data.toString()}");
        if (data["key"] == "needActive") {
          DioUtils.dismissDialog();
          AutoRouter.of(context).push(ActiveAccountRoute(phone: phone));
          return CustomToast.showSimpleToast(msg: data['msg']);
        } else {
          return HandleData.instance.handlePostData(data, context,fullData: false);
        }
      },
      // toJsonFunc: (json)=>UserModel.fromJson(json),
      showLoader: false,

    );

    return Utils.manipulateLoginData(context, result, _token ?? "",phone);
  }

  Future<bool> signUp(RegisterModel model) async {
    dynamic data = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.signUp,
      json: model.toJson(),
      returnType: ReturnType.Type,
      returnDataFun: (data) =>
          HandleData.instance.handleRegister(data, context,model.phone ??''),
      methodType: MethodType.Post,
      showLoader: false,
    );
    return HandleData.instance
        .handleRegister(data, context, model.phone!);
  }

  Future<List<QuestionModel>> frequentQuestions() async {
    return await GenericHttp<QuestionModel>(context).callApi(
        name: ApiNames.repeatedQuestions,
        returnType: ReturnType.List,
        showLoader: false,
        methodType: MethodType.Get,
        returnDataFun: (data)=> data["data"],
        toJsonFunc: (json)=> QuestionModel.fromJson(json)
    ) as List<QuestionModel>;
  }

  Future<bool> sendCode(String code, String phone) async {
    String? deviceId = await messaging.getToken();
    Map<String, dynamic> body = {
      "code": code,
      "country_code": "00966",
      "phone": phone,
      "device_id": deviceId,
      "device_type": Platform.isIOS ? "ios" : "android",
    };
    dynamic data = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.sendCode+ "?_method=patch",
      json: body,
      returnType: ReturnType.Type,
      showLoader: false,
      methodType: MethodType.Post,
      returnDataFun: (data) =>
          HandleData.instance.handlePostData(data, context),
    );
    return (data != null);
  }

  Future<bool> resendCode(String phone) async {
    String params = "?phone=$phone" ;
    dynamic data = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.resendCode + params,
      returnType: ReturnType.Type,
      methodType: MethodType.Get,
      returnDataFun: (data) {
        CustomToast.showSimpleToast(msg: data['msg']);
      },

    );
    return (data != null);
  }



  Future<String?> aboutApp() async {
    String? result = await GenericHttp<String>(context).callApi(
      name: ApiNames.aboutApp,
      returnType: ReturnType.Type,
      refresh: true,
      showLoader: false,
      returnDataFun: (data) {
        if(data["key"]=="success"){
          return data["data"]["about"] ;
        }else{
          return HandleData.instance.failure(data, context);
        }
      },
      methodType: MethodType.Get,
    );
    return result ;
  }

  Future<String?> terms() async {
    return await GenericHttp<String>(context).callApi(
      name: ApiNames.terms,
      returnType: ReturnType.Type,
      showLoader: false,
      returnDataFun: (data) {
        if(data["key"]=="success"){
          return data["data"]["terms"] ;
        }else{
          return HandleData.instance.failure(data, context);
        }
      },
      methodType: MethodType.Get,
    );
  }
  Future<List<IntroModel>> intros() async {
    return await GenericHttp<IntroModel>(context).callApi(
      name: ApiNames.intros,
      returnType: ReturnType.List,
      showLoader: false,
      returnDataFun: (data) {
        if(data["key"]=="success"){
          return data["data"]["intros"] ;
        }else{
          return HandleData.instance.failure(data, context);
        }
      },
      toJsonFunc: (json) => IntroModel.fromJson(json),

      methodType: MethodType.Get,
    );
  }



  Future<bool> switchNotify() async {
    dynamic data = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.switchNotify,
      returnType: ReturnType.Type,
      showLoader: false,
      methodType: MethodType.Post,
    );
    return (data != null);
  }

  Future<bool> forgetPassword(String phone) async {
    Map<String, dynamic> body = {
      "phone": "$phone",
    };
    dynamic data = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.forgetPassword,
      returnType: ReturnType.Type,
      json: body,
      showLoader: false,
      methodType: MethodType.Post,
      returnDataFun: (data) {
        print(data);
        return HandleData.instance.handlePostData(data, context);
      },
    );
    return (data != null);
  }

  Future<bool> resetUserPassword(String phone, String code, String pass) async {
    Map<String, dynamic> body = {
      "phone": "$phone",
      "code": "$code",
      "password": "$pass",
    };
    dynamic data = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.resetPassword,
      returnType: ReturnType.Type,
      json: body,
      showLoader: false,
      methodType: MethodType.Post,
      returnDataFun: (data) =>
          HandleData.instance.handlePostData(data, context),
    );
    return (data != null);
  }

  Future<bool> addComplaint(
      String userName, String phone,String email ,String complaint) async {
    Map<String, dynamic> body = {
      "user_name": "$userName",
      "phone": "$phone",
      "email": "$email",
      "complaint": "$complaint",
    };
    var result = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.contactUs,
      json: body,
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      toJsonFunc: (data) => data,
      showLoader: false,
    );
    if (result != null) {
      HandleData.instance.handlePostData(result, context);
      return true;
    }
    return false;
  }

}
