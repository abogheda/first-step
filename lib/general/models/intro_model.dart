import 'package:json_annotation/json_annotation.dart'; 

part 'intro_model.g.dart'; 

@JsonSerializable(ignoreUnannotated: false)
class IntroModel {
  @JsonKey(name: 'id')
  int? id;
  @JsonKey(name: 'description')
  String? description;
  @JsonKey(name: 'image')
  String? image;

  IntroModel({this.id, this.description, this.image});

   factory IntroModel.fromJson(Map<String, dynamic> json) => _$IntroModelFromJson(json);

   Map<String, dynamic> toJson() => _$IntroModelToJson(this);
}

