import 'package:json_annotation/json_annotation.dart'; 

part 'register_model.g.dart'; 

@JsonSerializable( ignoreUnannotated: false)
class RegisterModel {
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'phone')
  String? phone;
  @JsonKey(name: 'email')
  String? email;
  @JsonKey(name: 'password')
  String? password;
  @JsonKey(name: 'lat')
  String? lat;
  @JsonKey(name: 'lng')
  String? lng;
  @JsonKey(name: 'map_desc')
  String? mapDesc;
  @JsonKey(name: 'terms_agree')
  bool? termsAgree;

  RegisterModel({this.name, this.phone, this.email, this.password, this.lat, this.lng, this.mapDesc, this.termsAgree});

   factory RegisterModel.fromJson(Map<String, dynamic> json) => _$RegisterModelFromJson(json);

   Map<String, dynamic> toJson() => _$RegisterModelToJson(this);
}

