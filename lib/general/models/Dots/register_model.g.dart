// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterModel _$RegisterModelFromJson(Map<String, dynamic> json) =>
    RegisterModel(
      name: json['name'] as String?,
      phone: json['phone'] as String?,
      email: json['email'] as String?,
      password: json['password'] as String?,
      lat: json['lat'] as String?,
      lng: json['lng'] as String?,
      mapDesc: json['map_desc'] as String?,
      termsAgree: json['terms_agree'] as bool?,
    );

Map<String, dynamic> _$RegisterModelToJson(RegisterModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'phone': instance.phone,
      'email': instance.email,
      'password': instance.password,
      'lat': instance.lat,
      'lng': instance.lng,
      'map_desc': instance.mapDesc,
      'terms_agree': instance.termsAgree,
    };
