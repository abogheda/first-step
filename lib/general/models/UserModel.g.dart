// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'UserModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) => UserModel(
      name: json['name'] as String? ?? '',
      id: json['id'] as int,
      token: json['token'] as String,
      lang: json['lang'] as String,
      type: json['type'] as String,
      phone: json['phone'] as String,
      lng: json['lng'] as String,
      address: json['map_desc'] as String,
      lat: json['lat'] as String,
      countryCode: json['country_code'] as String,
      fullPhone: json['full_phone'] as String,
      email: json['email'] as String,
      image: json['image'] as String,
      isNotify: json['is_notify'] as bool,
    );

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'token': instance.token,
      'lang': instance.lang,
      'type': instance.type,
      'country_code': instance.countryCode,
      'phone': instance.phone,
      'full_phone': instance.fullPhone,
      'image': instance.image,
      'email': instance.email,
      'lat': instance.lat,
      'lng': instance.lng,
      'map_desc': instance.address,
      'is_notify': instance.isNotify,
    };
