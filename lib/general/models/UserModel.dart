import 'package:json_annotation/json_annotation.dart';

part 'UserModel.g.dart';

@JsonSerializable()
class UserModel {
  @JsonKey(name: "name")
  String name;
  @JsonKey(name: "id")
  int id;
  @JsonKey(name: "token")
  String token;
  @JsonKey(name: "lang")
  String lang;
  @JsonKey(name: "type")
  String type;
  @JsonKey(name: "country_code")
  String countryCode;
  @JsonKey(name: "phone")
  String phone;
  @JsonKey(name: "full_phone")
  String fullPhone;
  @JsonKey(name: "image")
  String image;
  @JsonKey(name: "email")
  String email;
  @JsonKey(name: "lat")
  String lat;
  @JsonKey(name: "lng")
  String lng;
  @JsonKey(name: "map_desc")
  String address;
  @JsonKey(name: "is_notify")
  bool isNotify;

  UserModel(
      {this.name ='',
      required this.id,
      required this.token,
      required this.lang,
      required this.type,
      required this.phone,
      required this.lng,
      required this.address,
      required this.lat,
      required this.countryCode,
      required this.fullPhone,
      required this.email,
      required this.image,
      required this.isNotify,
      });

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}
