// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'intro_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IntroModel _$IntroModelFromJson(Map<String, dynamic> json) => IntroModel(
      id: json['id'] as int?,
      description: json['description'] as String?,
      image: json['image'] as String?,
    );

Map<String, dynamic> _$IntroModelToJson(IntroModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'description': instance.description,
      'image': instance.image,
    };
