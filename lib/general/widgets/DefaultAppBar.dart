import 'package:flutter/material.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class DefaultAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final Widget? leading;
  final List<Widget> actions;
  final double? size ;
  final bool? needLeading;

  DefaultAppBar({
    required this.title,
    this.actions = const [],
    this.leading,
    this.needLeading,
    this.size
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: MyText(
        title: "$title",
        size: 17,
        color: MyColors.white,
      ),
      centerTitle: false,
      automaticallyImplyLeading: false,
      backgroundColor: MyColors.primary,
      elevation: 0,
      leading: leading ??
          IconButton(
            icon:Icon(Icons.adaptive.arrow_back,color: MyColors.white,size: 25,),
            onPressed: () => Navigator.of(context).pop(),
          ),
      actions: actions,
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(size??65);
}
