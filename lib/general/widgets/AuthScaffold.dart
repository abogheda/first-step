import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';

class AuthScaffold extends StatelessWidget {
  final Widget child;

  AuthScaffold({
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()=> FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: MyColors.greyWhite,
          child: Stack(
            children: [
              Container(
                height: 270,
                decoration: BoxDecoration(
                  color: MyColors.primary,
                  borderRadius:
                      BorderRadius.vertical(bottom: Radius.circular(40)),
                  image: DecorationImage(
                    scale: 9,
                    image: AssetImage(Res.logoWhite),
                  ),
                ),
              ),
              Positioned(
                top: 220,
                left: 30,
                right: 30,
                bottom: 30,
                child: Container(
                  alignment: Alignment.bottomCenter,
                  decoration: BoxDecoration(
                      color: MyColors.white,
                      borderRadius: BorderRadius.circular(10)),
                  child: child,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
